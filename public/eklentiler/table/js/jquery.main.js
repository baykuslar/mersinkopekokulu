$(document).ready(function() {

	"use strict";

	initbackTop();
	// backtop init
    function initbackTop() {
        "use strict";

        var jQuerybackToTop = jQuery("#back-top");
        jQuery(window).on('scroll', function() {
            if (jQuery(this).scrollTop() > 100) {
                jQuerybackToTop.addClass('active');
            } else {
                jQuerybackToTop.removeClass('active');
            }
        });
        jQuerybackToTop.on('click', function(e) {
            jQuery("html, body").animate({scrollTop: 0}, 900);
        });
    }

});

 
jQuery( window ).on( "load" , function() {
	"use strict";

	jQuery( "#loader" ).delay( 600 ).fadeOut( 300 );

}); 
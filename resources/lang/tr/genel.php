<?php

return [
    'logo' => 'logo_tr.png',
    'flogo' => 'footer-logo_tr.png',
    'dil' => 'Türkçe',
    'admin' => 'GİRİŞ',
    'turkce' => 'TÜRKÇE',
    'ingilizce' => 'İNGİLİZCE',
    'ucretsizsahip' => 'ÜCRETSİZ SAHİPLENDİRME',
    'admingiris' => 'Admin GİRİŞ',
    'bizeulasin' => 'BİZE ULAŞIN',
    'aramayap' => 'Arama Yap',
    'aramayapiniz' => 'Arama Yapınız...',

    'egitimuzman' => 'Eğitim Uzmanı',
    'egitimuzmanacikla' => 'Uzman Köpek Eğiticisi Rasim TEKİN uzmanlığında Eğitim...',
    'kopekoteli' => 'Köpek Oteli',
    'kopekoteliacikla' => 'Ayrı ayrı köpeklere ayrılmış külübeler halinde otel alanı...',
    'pea' => 'Profesyonel Eğitim Alanı',
    'peaacikla' => '3500 metrekare alana yerleştirilmiş eğitim merkezi...',
    'dogalyasam' => 'Doğal Yaşam',
    'dogalyasamacikla' => 'Yaşam alanından mahrum edilmemiş profesyonel çiftlik...',

    'marsk9galeri' => 'MARS K9 GALERİ',
    'facetakipedin' => 'Facebook’da takip edin',
    'mersinilk' => 'Mersin\'de İlk ve Tek!',
    'merkeztarim' => 'Merkezimiz Tarım ve Köy İşleri Bakanlığı, Mersin Büyükşehir Belediyesi\'nden ruhsatlıdır. Mersin\'de kanunen köpek eğitim hizmeti ve köpek oteli hizmeti sunan tek işletme Mars K9 Köpek Okulu\'dur.',
    'ozelyetistirme' => 'ÖZEL OLARAK YETİŞTİRDİĞİMİZ KÖPEK IRKLARI',
    'kisalink' => 'KISA LİNKLER',
    'akdeniztek' => 'Akdeniz bölgesin de Tek, ',
    'akdeniztekaciklama' => '5199 Hayvanları Koruma Kanunu Yönetmeliğine uygun, T.C Tarım bakanlığından Ruhsatlı, Köpek Eğitim Merkezi ve Köpek Oteli dir.',

    'iletisimformu' => 'İLETİŞİM FORMU',
    'iletisimbilgileri' => 'İLETİŞİM BİLGİLERİ',
    'adiniz' => 'Adınız ve Soyadınız',
    'adres' => 'Adres',
    'telefon' => 'Telefon',
    'email' => 'E-Posta',
    'konu' => 'Konu',
    'mesaj' => 'Mesajınız',
    'frdogru' => 'Form Doğrulama',
    'calismasaatleri' => 'Çalışma Saatleri',
    'hartaici' => 'Pzt. - Cuma',
    'cumartesi' => 'Pzt. - Cuma',
    'pazar' => 'Pazar',
    'ile' => 'ile',
    'kapali' => 'Kapalı',
    'bizeaboneol' => 'Bize Abone Olunuz!',
    'bizeaboneolyazi' => 'Etkinlikler, Satışlar ve Teklifler hakkındaki en son bilgileri edinin. Bülten için bugün kaydolun.',
    'mailgirin' => 'E-posta adresinizi giriniz.',
    'gonder' => 'Gönder',
    'rota' => 'Rota Oluştur',
    'iletigonder' => 'Mars K9 Köpek Okulu iletişim bilgileri mevcuttur.',
    'copyright' => 'Tüm Hakları Saklıdır.  Designer By',

    'anasayfa'  => 'ANASAYFA',
    'kurumsal'  => 'KURUMSAL',
    'hakkimizda'  => 'Hakkımızda',
    'ekibimiz'  => 'Ekibimiz',
    'insankaynaklari'  => 'İnsan Kaynakları',
    'galeri' => 'GALERİ',
    'resimgaleri' => 'RESİM GALERİSİ',
    'videogaleri' => 'VİDEO GALERİSİ',
    'videosayfaaciklama' => '<ul>
											<li>Mars İle Aşağıdaki Yarışma Ve Gösterilere İştirak Edilerek Dereceler Alınmıştır.</li>
											<li>Dünya Da İlk Garsonluk Yapan K-9 Köpeği</li>
											<li>Alman Çoban Köpeği Derneği Mersin Irk Standartları Yarışmasında Birincilik</li>
											<li>Alman Çoban Köpeği Derneğinin Türkiye Çapında Düzenlediği Bh İtaat Ve Refaket Sınavında Başarı</li>
											<li>Hayvan Hakları Derneği-Resmi Ve Özel Kurumların Talepleri Doğrultusunda Çok Sayıda Gösteriler</li>
											<li>Show Tv Yetenek Sizsiniz Programında 3 Kez İştirak - Tnt Çarkı Felek Programında Gösteri</li>
											<li>Mars Dışındaki Diğer Eğitimli Köpeklerimiz İlede Çok Sayıda Profesyonel Köpek Eğitim Gösterileri Yapılmıştır.</li>
										</ul>',
    'tumu' => 'TÜMÜ',
    'videolar' => 'VİDEOLAR',
    'sss' => 'SSS',
    'sssorular'    => 'Sıkça Sorulan Sorular',

    'blog' => 'BLOG',
    'yorumlar' => 'Yorumlar',
    'digeryazi' => 'DİĞER YAZILARIMIZ',
    'cevapbirak' => 'CEVAP BIRAKIN',
    'epostagizli' => 'E-posta hesabınız yayımlanmayacak. Gerekli alanlar işaretlendi. ',

    'referanslar' => 'REFERANSLARIMIZ',
    'sahip' => 'Sahip',
    'devaminioku' => 'Devamını oku...',
    'referans' => 'REFERANS',
    'guncelleme' => 'Güncellenme',
    'guruntulemesayisi' => 'Görüntüleme Sayısı',
    'digerreferans' => 'DİĞER REFERANSLARIMIZ',
    'etiketler' => 'ETİKETLER',
    'ilgiligorsel' => 'İLGİLİ GÖRSELLER',

    'iletisim' => 'İLETİŞİM',
    'egitimlerimiz' => 'EĞİTİMLERİMİZ',
    'whatsapsohbet' => 'WhatsApp\'ta bizimle sohbet ediniz...',
    'sssorular' => 'SIKÇA SORULAN SORULAR',
    'sssaciklama' => 'Bc Tasarım Ahşap, atölyemiz ile ilgili sıkça sorulan sorular yanıtlanmıştır.',
    'chatbaslik' => 'MARS K9 KÖPEK OKULU',
    'whatsapsohbet' => 'WhatsApp\'ta bizimle sohbet ediniz...',


];

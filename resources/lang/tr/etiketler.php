<?php
return [
    'kopekokulu' => 'Köpek Okulu',
    'kopekotel' => 'Köpek Oteli',
    'marsk9' => 'Mars K9',
    'mersinegitim' => 'Mersin Eğitim',
    'kopekegi' => 'Köpek Eğitimi',
    'uzmanke' => ' Uzman Köpek Eğitimi',
    'kopek' => 'Köpek',
    'okul' => 'Okul',
    'otel' => 'Otel',
    'egitim' => 'Eğitim',
    'uzman' => 'Uzman',
    'baribak' => 'Barınak',
    'egitimmerkezi' => 'Eğitim Merkezi',
    'rasim' => 'Rasim',
    'tekin' => 'Tekin',
    'mars' => 'Mars',
    'k9' => 'K9',

];
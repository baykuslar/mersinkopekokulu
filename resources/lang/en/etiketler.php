<?php
return [
    'kopekokulu' => 'Dog School',
    'kopekotel' => 'Dog Hotel',
    'marsk9' => 'Mars K9',
    'mersinegitim' => 'Mersin Education',
    'kopekegi' => 'Dog Training',
    'uzmanke' => ' Expert Dog Training',
    'kopek' => 'Dog',
    'okul' => 'School',
    'otel' => 'Otel',
    'egitim' => 'Education',
    'uzman' => 'Expert',
    'baribak' => 'Shelter',
    'egitimmerkezi' => 'education Center',
    'rasim' => 'Rasim',
    'tekin' => 'Tekin',
    'mars' => 'Mars',
    'k9' => 'K9',

];
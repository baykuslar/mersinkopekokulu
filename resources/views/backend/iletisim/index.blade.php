@extends('backend.master')
@section('title') İletisim Formu @endsection
@section('govde')
@section('page_level_scripts_css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="/backend/assets/pages/css/portfolio.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('page_css')
    <link rel="stylesheet" href="/backend/css/custom.css">
@endsection
<style>
    .od-gecmis{
        font-size: 14px !important;
        color: white !important;
        animation:biranimasyon 3s linear infinite;
    }
    @keyframes biranimasyon {
        0% {background: #520000
        }
        20% {background: #520000}
        40% {background: #520000}
        60% {background: red}
        80% {background: #2c323c}
        100% {background: red}
    }
    @-moz-keyframes biranimasyon {
        0% {color: #520000}
        20% {color: #520000}
        40% {color: #520000}
        60% {color: red}
        80% {color: #2c323c}
        100% {color: red}
    }
    @-webkit-keyframes biranimasyon {
        0% {color: #520000}
        20% {color: #520000}
        40% {color: #520000}
        60% {color: red}
        80% {color: #2c323c}
        100% {color: red}
    }
    @-o-keyframes biranimasyon {
        0% {color: #520000}
        20% {color: #520000}
        40% {color: #520000}
        60% {color: red}
        80% {color: #2c323c}
        100% {color: red}
    }
    .cev{
        color: rgba(128, 128, 128, 0.34) !important;
        cursor: not-allowed;
    }

</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">

        <div class="row">
            <!--Sağ Alan Bilgileri-->
            <div class="col-lg-12 col-md-12 cl-sm-12 col-xs-12">
                <div class="tab-content vertical">

                    <div class="tab-pane fade in active" id="is-basvulari" role="tabpanel">
                        <div class="panel panel-default">
                            <div class="panel-heading" style="padding: 0px 10px;margin-top:19px;">
                                <div class="row">
                                    <a href="#" class="list-group-item no-border active"><i class="fa fa-download m-r-5"></i> Gelen Forum Mailleri
                                        <b>
                                            {{$basvurutopla}}
                                        </b>
                                    </a>
                                </div>
                            </div>
                            <div class="panel-body">
                                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                                    <thead>
                                    <tr>
                                        <th class="text-center"> İşlemler</th>
                                        <th class="text-center"> Adı Soyadı</th>
                                        <th class="text-center"> Telefon</th>
                                        <th class="text-center"> E-mail</th>
                                        <th class="text-center"> Konu</th>
                                        <th class="text-center"> Mesaj</th>
                                        <th class="text-center"> Gönderim Tarihi</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {{ csrf_field() }}
                                    <?php $no=1 ?>
                                    @foreach($isbasvurukayitlar as $basvuru)
                                        @if($basvuru->aktif_pasif=='off')
                                            <tr class="item{{$basvuru->id}}">
                                                <td class="text-center cev" style="max-width: 200px;min-width: 110px;">
                                                    <button  title="Güncelle"  class="td-pa btn btn-xs waves-effect waves-light">
                                                        <i class="fa fa-file-text-o" aria-hidden="true"></i> Kapatıldı
                                                    </button>
                                                    <button title="Sil" class="cev td-pa btn btn-xs  waves-effect waves-light delete-modal"
                                                            data-toggle="tooltip" data-placement="right" title="Sil"
                                                            data-id="{{$basvuru->id}}" data-title="{{$basvuru->adi_soyadi}}"><i class="fa fa-trash"></i>
                                                    </button>
                                                </td>
                                                <td class="text-center cev">{{$basvuru->adi_soyadi}}</td>
                                                <td class="text-center cev">{{$basvuru->telefon}}</td>
                                                <td class="text-center cev">{{$basvuru->email}}</td>
                                                <td class="text-center cev">{{$basvuru->konu}}</td>
                                                <td class="text-center cev">{{$basvuru->mesaj}}</td>
                                                <td class="text-center cev" style="max-width: 200px">{{\Carbon\Carbon::parse($basvuru->created_at)->format('d/m/Y H:i:s')}}</td>
                                            </tr>

                                        @else
                                            <tr class="item{{$basvuru->id}}">
                                                <td class="text-center" style="max-width: 200px;min-width: 110px;">
                                                    <button type="button" class="td-pa btn btn-xs btn-success btn-custom waves-effect waves-light edit-modal"
                                                            data-aktif_pasif="off"
                                                            data-id="{{$basvuru->id}}"
                                                            style="margin-right: 0px;">
                                                        <i class="fa fa-check" aria-hidden="true"></i> Kapat
                                                    </button>
                                                    <button title="Sil" class="td-pa btn btn-danger btn-xs btn-custom waves-effect waves-light delete-modal"
                                                            data-toggle="tooltip" data-placement="right" title="Sil"
                                                            data-id="{{$basvuru->id}}" data-title="{{$basvuru->adi_soyadi}}"><i class="fa fa-trash"></i>
                                                    </button>
                                                </td>
                                                <td class="text-center">{{$basvuru->adi_soyadi}}</td>
                                                <td class="text-center">{{$basvuru->telefon}}</td>
                                                <td class="text-center">{{$basvuru->email}}</td>
                                                <td class="text-center">{{$basvuru->konu}}</td>
                                                <td class="text-center">{{$basvuru->mesaj}}</td>
                                                <td class="text-center" style="max-width: 200px">{{\Carbon\Carbon::parse($basvuru->created_at)->format('d/m/Y H:i:s')}}</td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div> <!-- end Col-9 -->
        </div> <!-- end Col-9 -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

<!-- Delete Item Modal -->
<div class="sweet-alert showSweetAlert" id="delete-modal" style="border-radius: 24px !important;">
    <div class="modal-header">
        <div class="waricon">
            <i style="color:red;font-size: 60px;line-height: 60px;" class="fa fa-exclamation-circle" aria-hidden="true"></i>
        </div>
        <span class="hidden id"></span>
        <h2 style="color:orangered" class="title"> </h2>
        <h3>Bilgisi silinecektir?</h3>
        <p>Bilgiyi tamamen silmek İstediğinizden emin misiniz!</p>
    </div>
    <div class="modal-footer" style="text-align:center !important">
        <button style="margin: 0px 15px;padding: 7px 20px;float: left" type="button" class="btn btn-circle red btn-sm  actionBtn" data-dismiss="modal">
            <i class="fa fa-trash fa-lg" aria-hidden="true"></i> Delete
        </button>
        <button style="margin: 0px 15px;padding: 7px 20px;" type="button" class="btn btn-circle green btn-sm"  data-dismiss="modal"
                onclick="$.Notification.autoHideNotify('error', 'top right', 'İşlem Başarısız','Kayıt Silinmedi!...')">
            <i class="fa fa-times fa-lg" aria-hidden="true"></i> Vazgeç
        </button>
    </div>
</div>

<!-- //İş Başvurusunu Kapatma Modalı -->
<div class="modal fade" id="myModal" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog" style="width: 265px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                </button>
                <h4 class="modal-title custom_align" id="Heading"> Kayıt Güncelle</h4>
            </div>
            <div class="modal-body">
                <p>@include('backend.iletisim.edit')</p>
            </div>
        </div>
    </div>
</div>
@endsection
@section('page_level_scripts_js')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="/backend/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js" type="text/javascript"></script>
@endsection
@section('page_level_scripts_js_end')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="/backend/assets/pages/scripts/portfolio-1.min.js" type="text/javascript"></script>
@endsection
@section('dahil_edilecek_js')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#panelPrecios [data-toggle="tooltip"]').tooltip({
                animated: 'fade',
                placement: 'bottom',
                html: true
            });

        });
        //delete
        $(document).on('click', '.delete-modal', function() {
            $('.actionBtn').addClass('delete');
            $('.id').text($(this).data('id'));
            $('.deleteContent').show();
            $('.title').html($(this).data('title'));
            $('#delete-modal').modal('show');
        });
        $('.modal-footer').on('click', '.delete', function() {
            $.ajax({
                url: '/admin/deleteFormmail',
                type : 'POST',
                data: {
                    '_token': $('input[name=_token]').val(),
                    'id': $('.id').text()
                },
                success : function(cevap){
                    $('.item' + $('.id').text()).remove();
                    console.log(cevap);
                    if( cevap.giris !== false ){
                        toastr['success']('Kayıtlı Bilgi Silindi!', 'İşlem Başarılı!');
                        window.location.reload(true);
                    }else{
                        toastr['error']('Kayıtlı Bilgi Silinmedi!', 'İşlem Başarısız!');
                    }
                }
            });

        });
    </script>
@endsection



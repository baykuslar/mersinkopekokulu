@section('page_level_scripts_css')
    @parent
    <link href="/eklentiler/css/custom.css" rel="stylesheet" type="text/css">
    <link href="/eklentiler/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="/eklentiler/jquery-confirm-master/dist/jquery-confirm.min.css">
    <style type="text/css">
        .sweet-alert {
            top: 19%;
        }
    </style>
@endsection
<!-- //Sorumluyu Aktif Pasif Yapma Modalı -->
<div class="row">
    <div class="col-md-12 cl-sm-6 col-xs-12" >

        <div class="alert alert-success alert-dismissable">
            <span>Kaydı kapatmak istediğinizden emin misiniz?</span>
        </div>

        <div class="">
            <form class="m-top form-horizontal" name="guncelle_formu" id="guncelle_formu">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="panel-body" style="padding: 0px;">
                            <input type="hidden" id="aktif_pasif" name="aktif_pasif" value=""/>
                            <div class="modal-footer b-n">
                                <div class="col-sm-4">
                                    <button type="submit" class="btn btn-success waves-effect waves-light m-b-5" id="btn-kaydet">Evet</button>
                                </div>
                                <div class="col-sm-8">
                                    <button type="button" data-dismiss="modal" class="btn red" id="kayir">Hayır</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>

    </div> <!-- col -->
</div> <!-- End row -->
@section('page_level_scripts_js')
    <script src="/eklentiler/sweetalert/dist/sweetalert.min.js"></script>
    <script src="/eklentiler/sweetalert/dist/jquery.sweet-alert.init.js"></script>
    <script src="/eklentiler/jquery-confirm-master/dist/jquery-confirm.min.js"></script>
@endsection
@section('page_level_scripts_js_end')
    @parent
    <script type="text/javascript">
        $(document).ready(function() {
            $('#btn-kaydet').on('click', function (e) {
                e.preventDefault(); //post edilmesi durdur ve aşağıdaki funksiyonu çağır
                formuGonder();
            })

        });
        function formuGonder() {
            $.ajax({
                method: 'POST',
                url: '/admin/iletisim/' + window.kullaniciId + '/edit', data: $(document.guncelle_formu).serialize()
            }).done(function() {
                        window.location.reload(true);
                    }).fail(function(e) {
            });
        };

        $(document).on('click', '.edit-modal', function() {
            window.kullaniciId = $(this).data('id');
            $('.form-horizontal').show();
            $('#aktif_pasif').val($(this).data('aktif_pasif'));
            $('#myModal').modal('show');
        });
    </script>
@endsection
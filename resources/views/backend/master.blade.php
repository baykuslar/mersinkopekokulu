<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
    <title>{{$viewAyarlar->{'site_baslik_'.session('dil')} }} - @yield('title')</title>
    <meta charset="UTF-8">
    <link rel="apple-touch-icon" sizes="57x57" href="/backend/assets/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/backend/assets/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/backend/assets/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/backend/assets/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/backend/assets/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/backend/assets/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/backend/assets/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/backend/assets/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/backend/assets/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/backend/assets/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/backend/assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/backend/assets/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/backend/assets/favicon/favicon-16x16.png">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="Preview page of Metronic Admin Theme #1 for statistics, charts, recent events and reports" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="/backend/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="/backend/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="/backend/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/backend/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="/backend/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="/backend/assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <link href="/backend/assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
    <link href="/backend/assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" />

    <link href="/backend/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
    <link href="/backend/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />

    <link href="/eklentiler/notification/css/notification.css" rel="stylesheet">
    <link href="/eklentiler/alertconfirm/jquery-confirm-master/dist/jquery-confirm.min.css" rel="stylesheet">
    <link href="/eklentiler/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">

    <link href="/backend/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="/backend/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/backend/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
    <link href="/backend/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet" type="text/css" />
    <link href="/backend/assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet" type="text/css" />

    <link href="/backend/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="/backend/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
    <link href="/backend/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="/backend/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="/backend/assets/global/plugins/clockface/css/clockface.css" rel="stylesheet" type="text/css" />

    <link href="/backend/assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet" type="text/css" />
    <link href="/backend/assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.css" rel="stylesheet" type="text/css" />
    <link href="/backend/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />

    <link href="/backend/assets/global/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css" />
    <link href="/backend/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
    <link href="/backend/css/custom.css" rel="stylesheet" type="text/css" />
    <link href="/css/custom.css" rel="stylesheet" type="text/css" />
@yield('page_level_scripts_css')
<!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="/backend/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="/backend/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />

    <link href="/eklentiler/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css"/>
    <link href="/backend/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="/backend/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
    <link href="/backend/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
    <link href="/backend/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />


    @yield('page_css')

    @yield('style')
    <style type="text/css">
        .fa-ikon{
            color: #aab8cc !important;
        }
    </style>
</head>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
<div class="page-wrapper">
    <!--Üst MENÜ Bar Start -->
@include('backend.moduller.ust-bar')
<!-- Gövde start -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        @include('backend.moduller.sol-menu')
        @yield('govde')
    </div>

</div>
<!-- Footer start -->
@include('backend.moduller.footer')
{{--@include('backend.moduller.quick-nav')--}}
<script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>
<!--[if lt IE 9]>
<script src="/backend/assets/global/plugins/respond.min.js"></script>
<script src="/backend/assets/global/plugins/excanvas.min.js"></script>
<script src="/backend/assets/global/plugins/ie8.fix.min.js"></script>
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="/backend/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->

<script src="/backend/assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/jquery.input-ip-address-control-1.0.min.js" type="text/javascript"></script>
<script src="/backend/assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>

<script src="/backend/assets/global/plugins/moment.min.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/horizontal-timeline/horizontal-timeline.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>

<!--wizard initialization-->
{{--<script src="/eklentiler/notification/js/jquery.wizard-init.js" type="text/javascript"></script>--}}
<script src="/eklentiler/notification/js/notify.min.js"></script>
<script src="/eklentiler/notification/js/notify-metro.js"></script>
<script src="/eklentiler/notification/js/notifications.js"></script>

<script src="/eklentiler/alertconfirm/jquery-confirm-master/dist/jquery-confirm.min.js"></script>
<script src="/eklentiler/sweetalert/dist/sweetalert.min.js"></script>
<script src="/eklentiler/sweetalert/dist/jquery.sweet-alert.init.js"></script>

<script src="/backend/assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>

<script src="/backend/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/clockface/js/clockface.js" type="text/javascript"></script>

<script src="/backend/assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>

<script src="/backend/assets/global/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>

<script src="/backend/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>

<script src="/backend/assets/global/plugins/typeahead/handlebars.min.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/typeahead/typeahead.bundle.min.js" type="text/javascript"></script>
@yield('page_level_scripts_js')
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="/backend/assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="/backend/assets/pages/scripts/components-typeahead.min.js" type="text/javascript"></script>
<script src="/backend/assets/pages/scripts/components-select2.min.js" type="text/javascript"></script>
<script src="/backend/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>
<script src="/backend/assets/pages/scripts/dashboard.min.js" type="text/javascript"></script>
<script src="/backend/assets/pages/scripts/form-wizard.min.js" type="text/javascript"></script>
<script src="/backend/assets/pages/scripts/form-validation.min.js" type="text/javascript"></script>
<script src="/backend/assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<script src="/backend/assets/pages/scripts/components-bootstrap-tagsinput.min.js" type="text/javascript"></script>
<script src="/backend/assets/pages/scripts/components-bootstrap-maxlength.min.js" type="text/javascript"></script>

<script src="/backend/assets/pages/scripts/ui-toastr.min.js" type="text/javascript"></script>
<script src="/eklentiler/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>
<script src="/backend/assets/pages/scripts/form-input-mask.min.js" type="text/javascript"></script>

<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="/backend/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
<script src="/backend/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
<script src="/backend/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
<script src="/backend/assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>

@yield('page_level_scripts_js_end')
{{--<script src="/backend/assets/pages/scripts/profile.min.js" type="text/javascript"></script>--}}
{{--<script src="/backend/assets/pages/scripts/timeline.min.js" type="text/javascript"></script>--}}
<!-- END THEME LAYOUT SCRIPTS -->
<script type="text/javascript" src="/eklentiler/icheck/icheck.min.js"></script>

<!--Resim 15kb maxsimum olsun script START -->
<script src="/eklentiler/sweetalert2/sweetalert2.min.js" type="text/javascript"></script>
<link href="/eklentiler/sweetalert2/sweetalert2.css" rel="stylesheet" type="text/css">

<link href="/eklentiler/resim-uploads/bootstrap-imageupload.css" rel="stylesheet">
<script src="/eklentiler/resim-uploads/bootstrap-imageupload.js"></script>
<script type="text/javascript">
    var $imageupload = $('.imageupload');
    $imageupload.imageupload();

    $('#imageupload-disable').on('click', function() {
        $imageupload.imageupload('disable');
        $(this).blur();
    })

    $('#imageupload-enable').on('click', function() {
        $imageupload.imageupload('enable');
        $(this).blur();
    })

    $('#imageupload-reset').on('click', function() {
        $imageupload.imageupload('reset');
        $(this).blur();
    });
</script>
<!--Resim 15kb maxsimum olsun script END -->

@yield('dahil_edilecek_js')<!--// Dahil Edilecek yollar -->
<script type="text/javascript">
    @if(Session::has('notification'))
    alert("{{ Session::get('notification.alert-type') }}");
    var type = "{{ Session::get('notification.alert-type', 'info') }}";
    switch(type){
        case 'info':
            toastr.info("{{ Session::get('notification.message') }}");
            break;

        case 'warning':
            toastr.warning("{{ Session::get('notification.message') }}");
            break;
        case 'success':
            toastr.success("{{ Session::get('notification.message') }}");
            break;
        case 'error':
            toastr.error("{{ Session::get('notification.message') }}");
            break;
    }
    @endif
    <!--   İi-İlçe-Mahalle Script  START-->
    $('#sehir').change(function(){
        var sehirID = $(this).val();
        if(sehirID){
            $.ajax({
                type:"GET",
                url:"{{url('/admin/api/get-ilce-list')}}?ilce_sehirkey="+sehirID,
                success:function(res){
                    if(res){
                        $("#ilce").empty();
                        $("#ilce").append('<option>Seçiniz</option>');
                        $.each(res,function(key,value){
                            $("#ilce").append('<option value="'+key+'">'+value+'</option>');
                        });

                    }else{
                        $("#ilce").empty();
                    }
                    that.trigger('sehirlerYuklendi');
                }
            });
        }else{
            $("#ilce").empty();
            $("#mahalle").empty();
        }
    });
    $('#ilce').on('change',function(){
        var ilceID = $(this).val();
        if(ilceID){
            $.ajax({
                type:"GET",
                url:"{{url('/admin/api/get-mahalle-list')}}?mahalle_ilcekey="+ilceID,
                success:function(res){
                    if(res){
                        $("#mahalle").empty();
                        $("#mahalle").append('<option>Seçiniz</option>');
                        $.each(res,function(key,value){
                            $("#mahalle").append('<option value="'+key+'">'+value+'</option>');
                        });

                    }else{
                        $("#mahalle").empty();
                    }
                    that.trigger('ilcelerYuklendi');
                }
            });
        }else{
            $("#ilce").empty();
            $("#mahalle").empty();
        }

    });

    var sehirBilgisi = null;
    var ilceBilgisi = null;
    $('#myModal [name=sehir]').on('sehirlerYuklendi', function() {
        $('#myModal [name=ilce]').val(sehirBilgisi).change();
    });
    $('#myModal [name=ilce]').on('ilcelerYuklendi', function() {
        $('#myModal [name=mahalle]').val(ilceBilgisi);
    });
    var sehirKutulari = $('#myModal [name=sehir]');
    if (sehirKutulari.length) {
        $('#myModal [name=sehir]')[0].setDeger = function(_ulke, _il, _ilce) {
            sehirBilgisi = _il;
            ilceBilgisi = _ilce;
            $('#myModal [name=sehir]').val(_ulke).change();
        }
    }
    <!--   İi-İlçe-Mahalle Script  END-->

    $(document).ready(function() {
        CKEDITOR.replace( 'aciklamasi' );
        $('#btnGecmis').on('click', function () {
            $('#islemGecmisi').fadeIn('slow');
        });
        // Maskeli inputların maskesini uygula
        $(":input").inputmask();

        tarih = new  Date();
        yil = tarih.getFullYear();
        ay = tarih.getMonth() + 1 ;

        $('#talep_tarihi').datepicker({
            format: 'dd-mm-yyyy'
        });

    });

    @if( !empty( session('onayMesaji') ) )
        toastr['success']('{{session('onayMesaji')}}', 'İşlem Başarılı');
    <?php \Session::put('onayMesaji', '' );?>
            @endif

            @if( !empty( session('hataMesaji') ) )
        toastr['error']('{{session('hataMesaji')}}', 'İşlem Başarısız');
    <?php \Session::put('hataMesaji', '' );?>
    @endif
    //Tooltip code
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });
    <!-- Sadece Rakam Yızılı imput alanına START "autofocus maxlength="11" required="" onKeyPress="return numbersonly(this, event)""-->
    function numbersonly(myfield, e, dec)
    {
        var key;
        var keychar;

        if (window.event)
            key = window.event.keyCode;
        else if (e)
            key = e.which;
        else
            return true;
        keychar = String.fromCharCode(key);

        // control keys
        if ((key==null) || (key==0) || (key==8) ||
            (key==9) || (key==13) || (key==27) )
            return true;

        // numbers
        else if ((("0123456789").indexOf(keychar) > -1))
            return true;

        // decimal point jump
        else if (dec && (keychar == "."))
        {
            myfield.form.elements[dec].focus();
            return false;
        }
        else
            return false;
    }
    <!-- Sadece Rakam Yızılı imput alanına END -->
</script>
<script>
    $(document).ready(function() {
        // initiate layout and plugins
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout
        UIToastr.init();
        ComponentsPickers.init();
        language: 'tr';
    });
    CKEDITOR.editorConfig = function( config ) {
        // Define changes to default configuration here. For example:
        // config.language = 'fr';
        // config.uiColor = '#AADC6E';
        config.extraPlugins = 'base64image';
        config.skin = 'office2013';
        config.removeButtons = 'Flash';
        config.removePlugins = 'forms';
        config.toolbar =    [
            ['Cut','Copy','Paste','PasteText','PasteFromWord','-','Print', 'SpellChecker', 'Scayt'],
            ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
            ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField'],
            ['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
            '/',
            ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
            ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
            ['Link','Unlink','Anchor'],
            ['Image', 'Table','HorizontalRule','Smiley','SpecialChar'],
            ['TextColor','BGColor'],
            ['Maximize', 'ShowBlocks', 'base64image']
        ];
    };
</script>
</body>
</html>

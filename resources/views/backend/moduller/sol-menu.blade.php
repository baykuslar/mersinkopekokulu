<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            <li class="sidebar-toggler-wrapper hide">
                <div class="sidebar-toggler">
                    <span></span>
                </div>
            </li>
            <li class="nav-item start active open">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-user"></i>
                    <span class="titlee ">{{\Auth::user()->name }}</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item  ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-building fa-ikon"></i>
                    <span class="titlee">Kurumsal</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  ">
                        <a href="/admin/hakkimizda" class="nav-link ">
                            <span class="titlee">Hakkımızda</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="/admin/backend/kurumsal/ekibimiz" class="nav-link ">
                            <span class="title">Ekibimiz</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="/admin/insan-kaynaklari" class="nav-link ">
                            <span class="titlee">İnsan Kaynakları</span>
                            @if($viewIsbasvurusu >= '1')
                                <span class="badge badge-success">
                                {{$viewIsbasvurusu}}
                                </span>
                            @else
                            @endif
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item  ">
                <a href="/admin/galeri" class="nav-link nav-toggle">
                    <i class="fa fa-picture-o fa-ikon"></i>
                    <span class="titlee">Galeri</span>
                </a>
            </li>
            <li class="nav-item  ">
                <a href="/admin/video" class="nav-link nav-toggle">
                    <i class="fa fa-video-camera fa-ikon"></i>
                    <span class="titlee">Video</span>
                </a>
            </li>
            <li class="nav-item  ">
                <a href="/admin/sssorular" class="nav-link nav-toggle">
                    <i class="fa fa-building fa-ikon"></i>
                    <span class="titlee">Sss</span>
                </a>
            </li>
            <li class="nav-item  ">
                <a href="/admin/blog" class="nav-link nav-toggle">
                    <i class="fa fa-pencil-square fa-ikon"></i>
                    <span class="titlee">Blog</span>
                </a>
            </li>
            <li class="nav-item  ">
                <a href="/admin/egitimlerimiz" class="nav-link nav-toggle">
                    <i class="fa fa-stack-overflow fa-ikon"></i>
                    <span class="titlee">Eğitimlerimiz</span>
                </a>
            </li>
            <li class="nav-item  ">
                <a href="/admin/kopek-okulu" class="nav-link nav-toggle">
                    <i class="fa fa-stack-overflow fa-ikon"></i>
                    <span class="titlee">Eğitim Merkezi</span>
                </a>
            </li>
            <li class="nav-item  ">
                <a href="/admin/otel-pansiyon" class="nav-link nav-toggle">
                    <i class="fa fa-picture-o fa-ikon"></i>
                    <span class="titlee">Otel / Pansiyon</span>
                </a>
            </li>
            <li class="nav-item  ">
                <a href="/admin/backend/kopek-temini" class="nav-link nav-toggle">
                    <i class="fa fa-check-square fa-ikon"></i>
                    <span class="titlee">Köpeklerimiz</span>
                </a>
            </li>
            <li class="nav-item  ">
                <a href="/admin/referanslar" class="nav-link nav-toggle">
                    <i class="fa fa-user fa-ikon"></i>
                    <span class="titlee">Referanslar</span>
                </a>
            </li>
            <li class="nav-item  ">
                <a href="/admin/iletisim" class="nav-link nav-toggle">
                    <i class="fa fa-envelope fa-ikon"></i>
                    <span class="titlee">İletişim Formu</span>
                    @if($viewForummail >= '1')
                    <span class="badge badge-success">
                        {{$viewForummail}}
                    </span>
                    @else
                    @endif
                </a>
            </li>
            <li class="nav-item  ">
                <a href="/admin/backend/yoneticiler" class="nav-link nav-toggle">
                    <i class="fa fa-users fa-ikon"></i>
                    <span class="titlee">Yöneticiler</span>
                </a>
            </li>
            <li class="nav-item  ">
                <a href="/admin/giris-modal" class="nav-link nav-toggle">
                    <i class="fa fa-clock-o fa-ikon"></i>
                    <span class="titlee">Özel Duyuru! (Modal)</span>
                </a>
            </li>
            <li class="nav-item  ">
                <a href="/admin/ayarlar" class="nav-link nav-toggle">
                    <i class="fa fa-cogs fa-ikon"></i>
                    <span class="titlee">Ayarlar</span>
                </a>
            </li>
            <li class="nav-item  ">
                <a href="/admin/islem-gecmisi" class="nav-link nav-toggle">
                    <i class="fa fa-lastfm fa-ikon"></i>
                    <span class="titlee">İşlem Geçmişi</span>
                    @if($viewGunisciislemler >= '1')
                        <span class="badge badge-danger">
                        {{$viewGunisciislemler}}
                        </span>
                    @else
                    @endif
                </a>
            </li>

        </ul>
    </div>
    <!-- END SIDEBAR -->
</div>

<style>

    element.style {
    }
    #footer-bottom {
        background: #343b44;
        padding: 60px 0 50px 0;
    }
    .clearfix {
    }
    #site-container, .ht-container {
        max-width: 1170px;
    }
    #site-container, .ht-container {
        margin: 0 auto;
        max-width: 1000px;
        padding: 0 3%;
    }
    .row, .row-fixed, .row-adaptive, .row-delaybreak {
        width: 100%;
        margin: 0 auto;
        zoom: 1;
    }
    #footer-bottom .column {
        text-align: center;
        margin: 0;
    }
    .row .col-full, .row-fixed .col-full, .row-adaptive .col-full {
        width: 100%;
    }
    .column:first-child {
        margin-left: 0;
    }
    .column {
        margin-left: 4.4%;
        float: left;
        min-height: 1px;
        position: relative;
        margin-bottom: 1.5em;
        -moz-box-sizing: border-box;
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
    }
    #footer-bottom i {
        color: #647183;
        font-size: 22px;
    }
    #footer-bottom i:hover {
        color: #32a1cf;
    }
    #footer-bottom #copyright {
        font-size: 15px;
        color: #647183;
        float: none;
    }
    #footer-bottom #copyright {
        line-height: 100%;
    }
    #footer-bottom .column {
        text-align: center;
        margin: 0;
    }
    #footer-bottom {
        background: #343b44;
        padding: 60px 0 50px 0;
    }
</style>
<!-- BEGIN FOOTER -->
<footer id="footer" class="clearfix" style="bottom: 0px;padding: 0px !important;margin: 0px !important;">
    <div id="footer-bottom" class="clearfix">
        <div class="ht-container">

            <div class="row stacked">
                <div class="column col-full">
                    <a title="Keenthemes" href="https://keenthemes.com">
                        <img alt="Keenthemes" src="/backend/assets/layouts/layout/img/logo-big.png">
                    </a>
                    <div class="clearfix"></div>
                    <p>
                        <a href="#" target="_blank" class="c-icon-link"><i class="icon-social-twitter"></i></a>&nbsp;&nbsp;
                        <a href="#" target="_blank" class="c-icon-link"><i class="icon-social-dribbble"></i></a>&nbsp;&nbsp;
                        <a href="#" target="_blank" class="c-icon-link"><i class="icon-social-facebook"></i></a>&nbsp;&nbsp;
                        <a href="#" target="_blank" class="c-icon-link"><i class="icon-handbag"></i></a>
                    </p>
                    <div class="clearfix"></div>
                    <small id="copyright" role="contentinfo">Copyright © BaykusCreative.</small>
                </div>
            </div>

        </div>
    </div>

</footer>
<!-- End Our Newsletter ______________________________ -->
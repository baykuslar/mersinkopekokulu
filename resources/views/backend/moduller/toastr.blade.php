@if ($errors->any())
    <script type="text/javascript">
        toasts.error('İşlem bir nedenden dolayı tamamlanamadı!!', 'İşlem Başarısız!', {timeOut: 5000})
    </script>
@endif

@if ($mesaj = Session::get('success'))
    <script type="text/javascript">
        toasts.success('{{$mesaj}}', 'İşlem Başarılı!', {timeOut: 5000})
    </script>
    {{Session::forget('success')}}
@endif
@if ($mesaj = Session::get('error'))
    <script type="text/javascript">
        toasts.error('{{$mesaj}}', 'İşlem Başarısız!', {timeOut: 5000})
    </script>
    {{Session::forget('error')}}
@endif

@if ($mesaj = Session::get('warning'))
    <script type="text/javascript">
        toasts.warning('{{$mesaj}}', 'Dikkat!!', {timeOut: 5000})
    </script>
    {{Session::forget('warning')}}
@endif

@if ($mesaj = Session::get('info'))
    <script type="text/javascript">
        toasts.info('{{$mesaj}}', 'Bilgi!!', {timeOut: 5000})
    </script>
    {{Session::forget('info')}}
@endif
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="/admin/index">Anasayfa</a>
                        </li>
                    </ul>
                    <div class="page-toolbar">
                        <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                            <i class="icon-calendar"></i>&nbsp;
                            <span class="thin uppercase hidden-xs"></span>&nbsp;
                            <i class="fa fa-angle-down"></i>
                        </div>
                    </div>
                </div>
                <!-- BEGIN PAGE TITLE-->
                <h1 class="page-title"> Yönetici Gösterge Tablosu
                    <small>istatistikleri, son değişiklikler ve raporlar</small>
                </h1>
                <!-- END PAGE TITLE-->
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <a class="dashboard-stat dashboard-stat-v2 blue" href="/admin/backend/yoneticiler">
                            <div class="visual">
                                <i class="fa fa-comments"></i>
                            </div>
                            <div class="details">
                                <div class="number">
                                    <span >{{ \App\User::all()->count()-1 }}</span> <span style="font-size: 20px">Kişi</span>
                                </div>
                                <div class="desc"> Sistem kullanıcı Sayısı </div>
                            </div>
                        </a>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <a class="dashboard-stat dashboard-stat-v2 purple" href="#">
                            <div class="visual">
                                <i class="fa fa-globe"></i>
                            </div>
                            <div class="details">
                                <div class="number">
                                    <span >00000</span> <span style="font-size: 20px"> Adet</span>
                                </div>
                                <div class="desc"> Sahipsiz Köpeklerimiz</div>
                            </div>
                        </a>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <a class="dashboard-stat dashboard-stat-v2 grey" href="#">
                            <div class="visual">
                                <i class="fa fa-comments"></i>
                            </div>
                            <div class="details">
                                <div class="number">
                                    <span >0000</span> <span style="font-size: 20px"> Adet</span>
                                </div>
                                <div class="desc"> Sahipli Köpeklerimiz</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <a class="dashboard-stat dashboard-stat-v2 yellow" href="/admin/backend/kurumsal/ekibimiz">
                            <div class="visual">
                                <i class="fa fa-bar-chart-o"></i>
                            </div>
                            <div class="details">
                                <div class="number">
                                    <span >{{ $ekibimiz->count() }}</span><span style="font-size: 20px"> Kişi</span></div>
                                <div class="desc"> Çalışan Personel Sayısı </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <a class="dashboard-stat dashboard-stat-v2 green-meadow" href="/admin/backend/islem-gecmisi">
                            <div class="visual">
                                <i class="fa fa-shopping-cart"></i>
                            </div>
                            <div class="details">
                                <div class="number">
                                    <span >{{ $viewGunisciislemler }}</span> <span style="font-size: 20px"> İşlem</span>
                                </div>
                                <div class="desc">Gün İçi işlemler </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <a class="dashboard-stat dashboard-stat-v2 red" href="#">
                            <div class="visual">
                                <i class="fa fa-globe"></i>
                            </div>
                            <div class="details">
                                <div class="number">
                                    <span >00000</span><span style="font-size: 20px"> Adet</span></div>
                                <div class="desc"> Toplam Köpeklerimiz</div>
                            </div>
                        </a>
                    </div>

                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 red-haze" style="background-color: #ffae00c7;" href="/admin/insan-kaynaklari">
                                <div class="visual">
                                    <i class="fa fa-comments"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span >{{$viewIsbasvurusu}}</span> <span style="font-size: 20px"> Adet</span>
                                    </div>
                                    <div class="desc"> Yeni İş Başvurusu Var!</div>
                                </div>
                            </a>
                        </div>


                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <a class="dashboard-stat dashboard-stat-v2 green" href="/admin/backend/islem-gecmisi">
                            <div class="visual">
                                <i class="fa fa-users"></i>
                            </div>
                            <div class="details">
                                <div class="number">
                                        <span>
                                        {{Visitor::count() }}
                                        </span> <span style="font-size: 20px"> Kişi</span>
                                </div>
                                <div class="desc">Site Ziyaret Sayısı</div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
@extends('backend.master')
@section('title') Video Galerisi @endsection
@section('govde')
@section('page_level_scripts_css')
    <link href="/backend/assets/global/plugins/cubeportfolio/css/cubeportfolio.css" rel="stylesheet" type="text/css" />
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="/backend/assets/pages/css/portfolio.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('page_css')
    <link rel="stylesheet" href="/backend/css/custom.css">
@endsection

    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="admin/index">Anasayfa</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Video Galerisi</span>
                    </li>
                </ul>
            </div>
            <div class="m-heading-1 border-green m-bordered">
                <button id="sample_editable_1_new" class="btn sbold green btnCreate b-r"> Yeni Kayıt Ekle
                    <i class="fa fa-plus"></i>
                </button>
            </div>

            <div class="portfolio-content portfolio-1">
                <div id="js-grid-juicy-projects" class="cbp">
                    <?php $no=1 ?>
                        @foreach($videokayitlari as $kayit)
                        <div class="cbp-item item{{$kayit->id}}">
                            <div class="cbp-caption">
                                <div class="cbp-caption-defaultWrap">
                                    @if($kayit->url_adresi == null)
                                        <img src="/uploads/video/180x135.jpg" alt="" class="img-responsive">
                                    @else
                                        <img src="/uploads/video/180x135.jpg" alt="" class="img-responsive">
                                    @endif
                                </div>
                                <div class="cbp-caption-activeWrap">
                                    <div class="cbp-l-caption-alignCenter">
                                        <div class="cbp-l-caption-body">
                                            <a class="cbp-singlePage" rel="nofollow">
                                                <button type="button" class="btn btn-circle green btn-sm guncelle-btn "
                                                        data-id="{{$kayit->id}}"
                                                        data-sayfa_aciklamasi="{{$kayit->sayfa_aciklamasi}}"
                                                        data-adi="{{$kayit->adi}}"
                                                        data-video_aciklamasi="{{$kayit->video_aciklamasi}}"
                                                        data-url_adresi="{{$kayit->url_adresi}}">
                                                    <i class="fa fa-refresh" aria-hidden="true"></i>
                                                    <span>GÜNCELLE</span>
                                                </button>
                                            </a>
                                            <a class="cbp-lightbox">
                                                <button title="Sil" class="btn btn-circle red btn-sm delete-modal"
                                                        data-toggle="tooltip" data-placement="top" title="Sil"
                                                        data-id="{{$kayit->id}}" data-title="{{$kayit->adi}}"><i class="fa fa-trash"></i> SİL
                                                </button>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cbp-l-grid-projects-title text-center" style="font-weight: 100">
                                @if($kayit->video_aciklamasi==null)
                                    {{$kayit->id}}
                                @else
                                    {{$kayit->video_aciklamasi}}
                                @endif
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="clearfix"> </div>

    </div>
        <!-- END CONTENT BODY -->
</div>
        <!-- Delete Item Modal -->
        <div class="sweet-alert showSweetAlert" id="delete-modal" style="border-radius: 24px !important;">
            <div class="modal-header">
                <div class="waricon">
                    <i style="color:red;font-size: 60px;line-height: 60px;" class="fa fa-exclamation-circle" aria-hidden="true"></i>
                </div>
                <span class="hidden id"></span>
                <h2 style="color:orangered" class="title"> </h2>
                <h3>Bilgisi silinecektir?</h3>
                <p>Bilgiyi tamamen silmek İstediğinizden emin misiniz!</p>
            </div>
            <div class="modal-footer" style="text-align:center !important">
                <button style="margin: 0px 15px;padding: 7px 20px;float: left" type="button" class="btn btn-circle red btn-sm  actionBtn" data-dismiss="modal">
                    <i class="fa fa-trash fa-lg" aria-hidden="true"></i> Delete
                </button>
                <button style="margin: 0px 15px;padding: 7px 20px;" type="button" class="btn btn-circle green btn-sm"  data-dismiss="modal"
                        onclick="$.Notification.autoHideNotify('error', 'top right', 'İşlem Başarısız','Kayıt Silinmedi!...')">
                    <i class="fa fa-times fa-lg" aria-hidden="true"></i> Vazgeç
                </button>
            </div>
        </div>

<!-- //Edit Modal -->
<div class="modal fade" id="myModal" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                </button>
                <h4 class="modal-title custom_align" id="Heading"> Kayıt Ekle/Güncelle</h4>
            </div>
            <div class="modal-body">
                @include('backend.video.create')
            </div>
        </div>
    </div>
</div>
@endsection
@section('page_level_scripts_js')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="/backend/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js" type="text/javascript"></script>
    <script src="/js/irfan.js" type="text/javascript"></script>
@endsection
@section('page_level_scripts_js_end')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="/backend/assets/pages/scripts/portfolio-1.min.js" type="text/javascript"></script>
@endsection
@section('dahil_edilecek_js')
    <script type="text/javascript">
        $(document).ready(function() {
            $(":input").inputmask();

            $('.btnCreate').on('click', function () {
                $('#myModal').modal('show');
                forumBosalt();
            });

            $(document).on('click', '.guncelle-btn', function () {
                $('#myModal').modal('show');
                forumDoldur($(this));
            });

            $(document).on('click', '.delete-modal', function() {
                $('.actionBtn').addClass('delete');
                $('.id').text($(this).data('id'));
                $('.deleteContent').show();
                $('.title').html($(this).data('title'));
                $('#delete-modal').modal('show');
            });

            $('.modal-footer').on('click', '.delete', function() {
                $.ajax({
                    url: '/admin/deleteVideo',
                    type : 'POST',
                    data: {
                        '_token': $('input[name=_token]').val(),
                        'id': $('.id').text()
                    },
                    success : function(cevap){
                        $('.item' + $('.id').text()).remove();
                        console.log(cevap);
                        window.location.reload(true);
                    }
                });

            });

        });

       function forumDoldur(btnn) {
           $('#gmid').val(btnn.attr('data-id'));
           $('#sayfa_aciklamasi').val(btnn.attr('data-sayfa_aciklamasi'));
           $('#adi').val(btnn.attr('data-adi'));
           $('#video_aciklamasi').val(btnn.attr('data-video_aciklamasi'));
           $('#url_adresi').val(btnn.attr('data-url_adresi'));
       }

       function forumBosalt() {
           $('#gmid').val(0);
           $('#sayfa_aciklamasi').val('');
           $('#adi').val('');
           $('#video_aciklamasi').val('');
           $('#url_adresi').val('');
       }

    </script>
@endsection
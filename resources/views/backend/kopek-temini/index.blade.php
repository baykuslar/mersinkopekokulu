@extends('backend.master')
@section('title') Anasayfa @endsection
@section('govde')
@section('page_level_scripts_css')
    <link href="/backend/assets/global/plugins/cubeportfolio/css/cubeportfolio.css" rel="stylesheet" type="text/css" />
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="/backend/assets/pages/css/portfolio.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('page_css')
    <link rel="stylesheet" href="/backend/css/custom.css">
@endsection

    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="admin/index">Anasayfa</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Köpeklerimiz</span>
                    </li>
                </ul>
            </div>
            <div class="m-heading-1 border-green m-bordered">
                <button id="sample_editable_1_new" class="btn sbold green btnCreate b-r"> Yeni Kayıt Ekle
                    <i class="fa fa-plus"></i>
                </button>
            </div>
            <div class="portfolio-content portfolio-1">
                <div class="row">
                    <?php $no=1 ?>
                    @foreach($kopeklerimiz as $kopek)
                        <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12 item{{$kopek->id}}">
                            <div class=" card card-1" style="border-radius: 20px !important;">
                                <div class="cbp-item {{$kopek->id}}" style="padding: 15px;">
                                    <div class="cbp-caption">
                                        <div class="cbp-caption-defaultWrap">
                                            @if($kopek->resim == null)
                                                <img src="/frontend/images/kopek-temini/1.jpg" alt="" class="img-responsive">
                                            @else
                                                <img src="/frontend/images/kopek-temini/{{$kopek->resim}}" alt="" class="img-responsive">
                                            @endif
                                        </div>
                                    </div>
                                    <div class="cbp-l-grid-projects-title text-center">
                                        @if($kopek->kopek_kategori=='Satılık')
                                            <b style="color: green;">{{$kopek->kopek_kategori}}</b>
                                        @elseif($kopek->kopek_kategori=='Sahipsiz')
                                                <b style="color: #f6ae19;">{{$kopek->kopek_kategori}}</b>
                                        @elseif($kopek->kopek_kategori=='Sahipli')
                                            <b style="color: #0810f6;">{{$kopek->kopek_kategori}}</b>
                                        @else
                                            <b style="color: red;">{{$kopek->kopek_kategori}}</b>
                                        @endif
                                    </div>
                                    <div class="cbp-l-grid-projects-title text-center">{{$kopek->adi}}</div>
                                    <div class="cbp-l-grid-projects-desc text-center s-k-130">
                                        @if($kopek->aciklamasi==null)
                                            <b>{{$kopek->id}}</b>
                                        @else
                                            <b>{{$kopek->kopek_kategori}}</b>
                                        @endif
                                        {{$kopek->aciklamasi}}
                                    </div>
                                    <div class="profile-userbuttons">
                                        <button type="button" class="btn btn-circle green btn-sm guncelle-btn "
                                                data-id="{{$kopek->id}}"
                                                data-resim="/frontend/images/kopek-temini/{{$kopek->resim}}"
                                                data-adi="{{$kopek->adi}}"
                                                data-yasi="{{$kopek->yasi}}"
                                                data-cinsi="{{$kopek->cinsi}}"
                                                data-aciklamasi="{{$kopek->aciklamasi}}"
                                                data-link="{{$kopek->link}}"
                                                data-fiyat="{{$kopek->fiyat}}"
                                                data-kopek_kategori="{{$kopek->kopek_kategori}}"
                                                data-kopek_turu="{{$kopek->kopek_turu}}"
                                                data-baba_adi="{{$kopek->baba_adi}}"
                                                data-resim_babasi="/frontend/images/kopek-temini/{{$kopek->resim_babasi}}"
                                                data-anne_adi="{{$kopek->anne_adi}}"
                                                data-resim_annesi="/frontend/images/kopek-temini/{{$kopek->resim_annesi}}" >
                                            <i class="fa fa-refresh" aria-hidden="true"></i>
                                            <span class="guncelle">GÜNCELLE</span>
                                        </button>
                                        <button title="Sil" class="btn btn-circle red btn-sm delete-modal"
                                                data-toggle="tooltip" data-placement="top" title="Sil"
                                                data-id="{{$kopek->id}}" data-title="{{$kopek->adi}}"><i class="fa fa-trash"></i> SİL
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="clearfix"> </div>

    </div>
        <!-- END CONTENT BODY -->
</div>
        <!-- Delete Item Modal -->
        <div class="sweet-alert showSweetAlert" id="delete-modal" style="border-radius: 24px !important;">
            <div class="modal-header">
                <div class="waricon">
                    <i style="color:red;font-size: 60px;line-height: 60px;" class="fa fa-exclamation-circle" aria-hidden="true"></i>
                </div>
                <span class="hidden id"></span>
                <h2 style="color:orangered" class="title"> </h2>
                <h3>Bilgisi silinecektir?</h3>
                <p>Bilgiyi tamamen silmek İstediğinizden emin misiniz!</p>
            </div>
            <div class="modal-footer" style="text-align:center !important">
                <button style="margin: 0px 15px;padding: 7px 20px;float: left" type="button" class="btn btn-circle red btn-sm  actionBtn" data-dismiss="modal">
                    <i class="fa fa-trash fa-lg" aria-hidden="true"></i> Delete
                </button>
                <button style="margin: 0px 15px;padding: 7px 20px;" type="button" class="btn btn-circle green btn-sm"  data-dismiss="modal"
                        onclick="$.Notification.autoHideNotify('error', 'top right', 'İşlem Başarısız','Kayıt Silinmedi!...')">
                    <i class="fa fa-times fa-lg" aria-hidden="true"></i> Vazgeç
                </button>
            </div>
        </div>

<!-- //Edit Modal -->
<div class="modal fade" id="myModal" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                </button>
                <h4 class="modal-title custom_align" id="Heading"> Kayıt Ekle/Güncelle</h4>
            </div>
            <div class="modal-body">
                @include('backend.kopek-temini.create')
            </div>
        </div>
    </div>
</div>
@endsection
@section('page_level_scripts_js')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="/backend/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js" type="text/javascript"></script>
    <script src="/js/irfan.js" type="text/javascript"></script>
@endsection
@section('page_level_scripts_js_end')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="/backend/assets/pages/scripts/portfolio-1.min.js" type="text/javascript"></script>
@endsection
@section('dahil_edilecek_js')
    <script type="text/javascript">
        $(document).ready(function() {
            $(":input").inputmask();

            $('.btnCreate').on('click', function () {
                $('#myModal').modal('show');
                forumBosalt();
            });

            $(document).on('click', '.guncelle-btn', function () {
                $('#myModal').modal('show');
                forumDoldur($(this));
            });


            /*Kiralık-Satılık Ekle Çıkar*/
            $('#kopek_kategori').on('change', function() {
                var html=[];
                html.push('<option value="_" style="font-style: italic;">-- Seçiniz --</option>');
                    html.push('<option value="Erkek Yavrular">Erkek Yavrular</option>');
                    html.push('<option value="Dişi Yavrular">Dişi Yavrular</option>');
                    html.push('<option value="İleri İtaat Eğitimli Dişi Köpekler">İleri İtaat Eğitimli Dişi Köpekler</option>');
                    html.push('<option value="İleri İtaat Eğitimli Erkek Köpekler">İleri İtaat Eğitimli Erkek Köpekler</option>');
                    html.push('<option value="Temel İtaat Eğitimli Dişi Köpekler">Temel İtaat Eğitimli Dişi Köpekler</option>');
                    html.push('<option value="Temel İtaat Eğitimli Erkek Köpekler">Temel İtaat Eğitimli Erkek Köpekler</option>');
                    html.push('<option value="Bodyguard Eğitimli Dişi Köpekler">Bodyguard Eğitimli Dişi Köpekler</option>');
                    html.push('<option value="Bodyguard Eğitimli Erkek Köpekler">Bodyguard Eğitimli Erkek Köpekler</option>');
                    html.push('<option value="Diğer Irklar">Diğer Irklar</option>');

                $('#kopek_turu').html(html.join(''));
            });

            $(document).on('click', '.delete-modal', function() {
                $('.actionBtn').addClass('delete');
                $('.id').text($(this).data('id'));
                $('.deleteContent').show();
                $('.title').html($(this).data('title'));
                $('#delete-modal').modal('show');
            });

            $('.modal-footer').on('click', '.delete', function() {
                $.ajax({
                    url: '/admin/deleteKopektemini',
                    type : 'POST',
                    data: {
                        '_token': $('input[name=_token]').val(),
                        'id': $('.id').text()
                    },
                    success : function(cevap){
                        $('.item' + $('.id').text()).remove();
                        console.log(cevap);
                        if( cevap.giris !== false ){
                            toastr['success']('Kayıtlı Bilgi Silindi!', 'İşlem Başarılı!');
                            window.location.reload(true);
                        }else{
                            toastr['error']('Kayıtlı Bilgi Silinmedi!', 'İşlem Başarısız!');
                        }
                    }
                });

            });

        });

       function forumDoldur(btnn) {
           $('#gmid').val(btnn.attr('data-id'));
           $('#resim').attr('src',btnn.attr('data-resim'));
           $('#adi').val(btnn.attr('data-adi'));
           $('#yasi').val(btnn.attr('data-yasi'));
           $('#cinsi').val(btnn.attr('data-cinsi'));
           $('#aciklamasi').val(btnn.attr('data-aciklamasi'));
           $('#link').val(btnn.attr('data-link'));
           $('#fiyat').val(btnn.attr('data-fiyat'));
           $('#baba_adi').val(btnn.attr('data-baba_adi'));
           $('#resim_babasi').attr('src',btnn.attr('data-resim_babasi'));
           $('#anne_adi').val(btnn.attr('data-anne_adi'));
           $('#resim_annesi').attr('src',btnn.attr('data-resim_annesi'));
           $('#kopek_kategori').val(btnn.attr('data-kopek_kategori'));
           $('#kopek_kategori').trigger('change');
           $('#kopek_turu').val(btnn.attr('data-kopek_turu'));
       }

       function forumBosalt() {
           $('#gmid').val(0);
           $('#resim').attr('src','');
           $('#adi').val('');
           $('#yasi').val('');
           $('#cinsi').val('');
           $('#aciklamasi').val('');
           $('#link').val('');
           $('#fiyat').val('');
           $('#baba_adi').val('');
           $('#resim_babasi').attr('src','');
           $('#anne_adi').val('');
           $('#resim_annesi').attr('src','');
           $('#kopek_kategori').val('');
           $('#kopek_kategori').trigger('change');
           $('#kopek_turu').val('');
       }

    </script>
@endsection

    <form id="frm-gayrimenkul" class="form-horizontal" method="post" action="/admin/backend/kopek-temini" enctype="multipart/form-data">
            {{ csrf_field() }}
                <div class="form-body">
                    <div class="alert alert-danger display-hide">
                        <button class="close" data-close="alert"></button> Bazı form hatalarınız var. Lütfen kontrol ediniz. </div>
                    <div class="alert alert-success display-hide">
                        <button class="close" data-close="alert"></button> Form doğrulamanız başarılı!
                    </div>
                    <div class="form-group last">
                        <label class="control-label col-md-3">Resim</label>
                        <div class="col-md-9">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 120px; height: 90px;">
                                    <img src="" alt="" id="resim" />
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 120px; max-height: 90px;"> </div>
                                <div>
                                    <span class="btn green btn-file btn-xs">
                                        <span class="fileinput-new"> Resim Seçiniz </span>
                                        <span class="fileinput-exists"> Değiştir </span>
                                        <input type="file" name="resim" id="resim">
                                    </span>
                                    <a href="javascript:;" class="btn red fileinput-exists btn-xs" data-dismiss="fileinput"> Sil </a>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="form-group  margin-top-20">
                        <label class="control-label col-md-3">Köpek Adı
                            <span class="required"> * </span>
                        </label>
                        <div class="col-md-9">
                            <div class="input-icon right">
                                <i class="fa"></i>
                                <input type="text" class="form-control" name="adi" id="adi" required>
                            </div>
                        </div>
                    </div>

                    <div class="form-group  margin-top-20">
                        <label class="control-label col-md-3">Köpek Yaşı
                            <span class="required"> * </span>
                        </label>
                        <div class="col-md-9">
                            <div class="input-icon right">
                                <i class="fa"></i>
                                <input type="text" class="form-control" name="yasi" id="yasi" required>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3"> Köpek Cinsi
                            <span class="required"> * </span>
                        </label>
                        <div class="col-md-9">
                            <div class="input-icon right">
                                <i class="fa"></i>
                                <select name="cinsi" id="cinsi" class="form-control" required>
                                    <option value="" >Seçiniz</option>
                                    <option value="Erkek">Erkek</option>
                                    <option value="Dişi">Dişi</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group  margin-top-20">
                        <label class="control-label col-md-3">Açıklama (Özellikleri)
                            <span class=""> * </span>
                        </label>
                        <div class="col-md-9">
                            <div class="input-icon right">
                                <i class="fa"></i>
                                <textarea type="text" class="form-control" name="aciklamasi"  id="aciklamasi"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="form-group  margin-top-20">
                        <label class="control-label col-md-3">Link ver!
                            <span class=""> * </span>
                        </label>
                        <div class="col-md-9">
                            <div class="input-icon right">
                                <i class="fa"></i>
                                <input type="text" class="form-control" name="link"  id="link">
                            </div>
                        </div>
                    </div>

                    <div class="form-group  margin-top-20">
                        <label class="control-label col-md-3">Baba Adı
                            <span class="required"> * </span>
                        </label>
                        <div class="col-md-9">
                            <div class="input-icon right">
                                <i class="fa"></i>
                                <input type="text" class="form-control" name="baba_adi" id="baba_adi" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group last">
                        <label class="control-label col-md-3">Babası Resim</label>
                        <div class="col-md-9">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 120px; height: 90px;">
                                    <img src="" alt="" id="resim_babasi" />
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 120px; max-height: 90px;"> </div>
                                <div>
                                    <span class="btn green btn-file btn-xs">
                                        <span class="fileinput-new"> Resim Seçiniz </span>
                                        <span class="fileinput-exists"> Değiştir </span>
                                        <input type="file" name="resim_babasi" id="resim_babasi">
                                    </span>
                                    <a href="javascript:;" class="btn red fileinput-exists btn-xs" data-dismiss="fileinput"> Sil </a>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="form-group  margin-top-20">
                        <label class="control-label col-md-3">Anne Adı
                            <span class="required"> * </span>
                        </label>
                        <div class="col-md-9">
                            <div class="input-icon right">
                                <i class="fa"></i>
                                <input type="text" class="form-control" name="anne_adi" id="anne_adi" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group last">
                        <label class="control-label col-md-3">Anne Resim</label>
                        <div class="col-md-9">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 120px; height: 90px;">
                                    <img src="" alt="" id="resim_annesi" />
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 120px; max-height: 90px;"> </div>
                                <div>
                                    <span class="btn green btn-file btn-xs">
                                        <span class="fileinput-new"> Resim Seçiniz </span>
                                        <span class="fileinput-exists"> Değiştir </span>
                                        <input type="file" name="resim_annesi" id="resim_annesi">
                                    </span>
                                    <a href="javascript:;" class="btn red fileinput-exists btn-xs" data-dismiss="fileinput"> Sil </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3"> Köpek Kategori
                            <span class="required"> * </span>
                        </label>
                        <div class="col-md-9">
                            <div class="input-icon right">
                                <i class="fa"></i>
                                <select name="kopek_kategori" id="kopek_kategori" class="form-control" required>
                                    <option value="" >Seçiniz</option>
                                    <option value="Satıldı">Satıldı</option>
                                    <option value="Satılık">Satılık</option>
                                    <option value="Sahipli">Sahipli</option>
                                    <option value="Sahipsiz">Sahipsiz</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3"> Köpek Türü
                            <span class="required"> * </span>
                        </label>
                        <div class="col-md-9">
                            <div class="input-icon right">
                                <i class="fa"></i>
                                <select id="kopek_turu" name="kopek_turu" class="form-control select2" required>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">Fiyat <span class="required"> * </span></label>
                        <div class="col-md-9">
                            <div class="input-icon">
                                <i class="fa tl">₺</i>
                                <input type="text" class="form-control" name="fiyat" id="fiyat" onKeyPress="return numbersonly(this, event)" required>
                            </div>
                        </div>
                    </div>





                </div>

                <input type="hidden" name="gmid" id="gmid" value="0"/>

                <div class="modal-footer">
                    <button type="submit" class="btn green btn-circle add">
                        <span class='glyphicon glyphicon-check'></span> Kaydet
                    </button>
                    <button type="button" class="btn red btn-circle" data-dismiss="modal">
                        <span class='glyphicon glyphicon-remove'></span> Vazgeç
                    </button>
                </div>

            </form>

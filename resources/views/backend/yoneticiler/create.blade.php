<div class="row">
    <div class="col-md-12 cl-sm-6 col-xs-12">
        <div class="modal-body">
            <form id="frm" class="form-horizontal m-t-20" name="frm" role="form" method="POST" action="/admin/backend/yoneticiler" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" class="form-control" name="aktif_pasif" id="aktif_pasif" value="on"/>
                @if(\Auth::user()->kullanici_turu=='5')
                <input type="hidden" class="form-control" name="kullanici_turu" id="kullanici_turu" value="4"/>
                @elseif(\Auth::user()->kullanici_turu=='4')
                <input type="hidden" class="form-control" name="kullanici_turu" id="kullanici_turu" value="3"/>
                @endif
                <div class="form-group last">
                    <label class="control-label col-md-3">Profil Resmi</label>
                    <div class="col-md-9">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: 120px; height: 90px;">
                                <img src="" alt="" id="avatar" />
                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 120px; max-height: 90px;"> </div>
                            <div>
                                    <span class="btn green btn-file btn-xs">
                                        <span class="fileinput-new"> Resim Seçiniz </span>
                                        <span class="fileinput-exists"> Değiştir </span>
                                        <input type="file" name="avatar" id="avatar">
                                    </span>
                                <a href="javascript:;" class="btn red fileinput-exists btn-xs" data-dismiss="fileinput"> Sil </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group hidden">
                    <label class="col-sm-3 control-label" style="color: #ffb100">Kurum ID <span class="error">*</span></label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="kurum_id" name="kurum_id" value="{{\Auth::user()->kurum_id}}" readonly />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Ad Soyad</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="name" id="name" required autofocus/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">E-mail</label>
                    <div class="col-sm-9">
                        <input type="email" class="form-control" name="email" id="email" required autofocus/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Telefon</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="phone" id="phone" data-inputmask="'mask': '9 (999) 999 99 99'" required autofocus/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Kullanıcı Adı</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="username" id="username" required autofocus/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Şifre</label>
                    <div class="col-sm-9">
                        <input type="password" class="form-control" name="password" required autofocus>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Şifre Tekrarı</label>
                    <div class="col-sm-9">
                        <input type="password" class="form-control" name="password_confirmation">
                    </div>
                </div>

                <input type="hidden" name="gmid" id="gmid" value="0"/>

                <div class="modal-footer">
                    <button type="submit" class="btn green btn-circle add"  onclick="return val();">
                        <span class='glyphicon glyphicon-check'></span> Kaydet
                    </button>
                    <button type="button" class="btn red btn-circle" data-dismiss="modal">
                        <span class='glyphicon glyphicon-remove'></span> Vazgeç
                    </button>
                </div>
            </form>
        </div> <!-- panel-body -->
    </div> <!-- col -->
</div> <!-- End row -->
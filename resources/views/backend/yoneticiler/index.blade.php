@extends('backend.master')
@section('title') Yöneticiler @endsection
@section('govde')
    <style type="text/css">
        .thumb-lg {
            height: 88px;
            width: 88px;
        }
        .box-s{
            box-shadow: 0 1px 3px rgba(0, 0, 0, 0.1), 0 1px 2px rgba(0, 0, 0, 0.18) !important;
            border-radius: .25em !important;
        }
    </style>
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <!-- BEGIN PAGE BAR -->
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="index.html">Anasayfa</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Yöneticiler</span>
                        </li>
                    </ul>
                </div>
                <!-- END PAGE BAR -->
                <!-- BEGIN PAGE TITLE-->
                <h1 class="page-title"> Yöneticiler
                    <small>sayfası</small>
                </h1>
                <!-- END PAGE TITLE-->
                <!-- END PAGE HEADER-->
                <div class="m-heading-1 border-green m-bordered">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="btn-group">
                                <button id="sample_editable_1_new" class="btn sbold green btnCreate b-r"> Yeni Kayıt Ekle
                                    <i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="btn-group pull-right">
                                <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Araçlar
                                    <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a href="javascript:;">
                                            <i class="fa fa-print"></i> Print </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <i class="fa fa-file-excel-o"></i> Export to Excel </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="portlet light bordered">
                            <div class="portlet-body">
                                <div class="table-toolbar">

                                </div>
                                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                                    <thead>
                                    <tr>
                                        <th>
                                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                                                <span></span>
                                            </label>
                                        </th>
                                        <th class="text-center"> Profil Resmi </th>
                                        <th class="text-center"> Adı Soyadı </th>
                                        <th class="text-center"> Ünvan </th>
                                        <th class="text-center"> Kullanıcı Adı </th>
                                        <th class="text-center"> E-mail </th>
                                        <th class="text-center"> Telefon </th>
                                        <th class="text-center"> Kayıt Tarihi </th>
                                        <th class="text-center"> İşlemler</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {{ csrf_field() }}
                                    <?php $no=1 ?>
                                    @foreach($users as $user)
                                    <tr class="odd gradeX item{{$user->id}}">
                                        <td class="text-center">
                                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                <input type="checkbox" class="checkboxes" value="1" />
                                                <span></span>
                                            </label>
                                        </td>
                                        <td class="text-center">
                                            <a href="/admin/index" class="logo">
                                                @if($user->avatar==null)
                                                    <img src="/uploads/avatars/default.png" alt="user-img" class="thumb-lg img-thumbnail img-circle">
                                                @else
                                                    <img src="/uploads/avatars/{{$user->avatar}}" alt="user-img" class="thumb-lg img-thumbnail img-circle">
                                                @endif
                                            </a>
                                        </td>
                                        <td class="text-center">{{$user->name}}</td>
                                        <td class="text-center">
                                            <span class="label label-sm label-success"> Grafik Tasarım </span>
                                        </td>
                                        <td class="text-center"> {{$user->username}} </td>
                                        <td class="text-center">
                                            <a href="mailto:shuxer@gmail.com"> {{$user->email}} </a>
                                        </td>
                                        <td class="text-center"> {{$user->phone}} </td>
                                        <td class="text-center"> 12/01/2018 </td>
                                        <td class="text-center">

                                            <button type="button" class="btn btn-xs green guncelle-btn box-s"
                                                    data-id="{{$user->id}}"
                                                    data-avatar="/uploads/avatars/{{$user->avatar}}"
                                                    data-kullanici_turu="{{$user->kullanici_turu}}"
                                                    data-kurum_id="{{$user->kurum_id}}"
                                                    data-name="{{$user->name}}"
                                                    data-email="{{$user->email}}"
                                                    data-phone="{{$user->phone}}"
                                                    data-username="{{$user->username}}">

                                            <i class="fa fa-refresh" aria-hidden="true"></i>
                                            <span class="guncelle">Güncelle</span>
                                            </button>

                                            @if($user->id!= $admin->id)
                                            <button title="Sil" class="btn btn-xs red delete-modal box-s"
                                                    data-toggle="tooltip" data-placement="right" title="Sil"
                                                    data-id="{{$user->id}}" data-title="{{$user->name}}"><i class="fa fa-trash"></i>
                                            </button>
                                            @else
                                            <a href="javascript:;">
                                                <i class="icon-user"></i> Admin
                                            </a>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
                </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
    <!-- Delete Item Modal -->
    <div class="sweet-alert showSweetAlert" id="delete-modal" style="border-radius: 24px !important;">
        <div class="modal-header">
            <div class="waricon">
                <i style="color:red;font-size: 60px;line-height: 60px;" class="fa fa-exclamation-circle" aria-hidden="true"></i>
            </div>
            <span class="hidden id"></span>
            <h2 style="color:orangered" class="title"> </h2>
            <h3>Bilgisi silinecektir?</h3>
            <p>Bilgiyi tamamen silmek İstediğinizden emin misiniz!</p>
        </div>
        <div class="modal-footer" style="text-align:center !important">
            <button style="margin: 0px 15px;padding: 7px 20px;float: left" type="button" class="btn btn-circle red btn-sm  actionBtn" data-dismiss="modal">
                <i class="fa fa-trash fa-lg" aria-hidden="true"></i> Delete
            </button>
            <button style="margin: 0px 15px;padding: 7px 20px;" type="button" class="btn btn-circle green btn-sm"  data-dismiss="modal"
                    onclick="$.Notification.autoHideNotify('error', 'top right', 'İşlem Başarısız','Kayıt Silinmedi!...')">
                <i class="fa fa-times fa-lg" aria-hidden="true"></i> Vazgeç
            </button>
        </div>
    </div>


        <!-- //Edit Modal -->
        <div class="modal fade" id="myModal" role="dialog" aria-labelledby="edit" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                        </button>
                        <h4 class="modal-title custom_align" id="Heading"> Kayıt Ekle/Güncelle</h4>
                    </div>
                    <div class="modal-body">
                        @include('backend.yoneticiler.create')
                    </div>
                </div>
            </div>
        </div>

@endsection

@section('dahil_edilecek_js')
    <script type="text/javascript">
        $(document).ready(function() {

            $(":input").inputmask();

            $('.btnCreate').on('click', function () {
                $('#myModal').modal('show');
                forumBosalt();
            });

            $(document).on('click', '.guncelle-btn', function () {
                $('#myModal').modal('show');
                forumDoldur($(this));
            });

        });
        //şifre eşleşme ve onay function
        function val(){
            if(frm.password.value == "")
            {
                toastr['error']('Parolayı giriniz.', 'Eksik Bilgi!');
                frm.password.focus();
                return false;
            }
            if((frm.password.value).length < 6)
            {
                toastr['error']('Parola en az 6 karakter olmalıdır.', 'Eksik Bilgi!');
                frm.password.focus();
                return false;
            }

            if(frm.password_confirmation.value == "")
            {
                toastr['error']('Onay Parolasını giriniz.', 'Eksik Bilgi!');
                return false;
            }
            if(frm.password_confirmation.value != frm.password.value)
            {
                toastr['error']('Şifre onayı eşleşmiyor.', 'Eksik Bilgi!');
                return false;
            }

            return true;
        }
        $(document).on('click', '.delete-modal', function() {
            $('.actionBtn').addClass('delete');
            $('.id').text($(this).data('id'));
            $('.kurum_id').text($(this).data('kurum_id'));
            $('.deleteContent').show();
            $('.title').html($(this).data('title'));
            $('#delete-modal').modal('show');
        });
        $('.modal-footer').on('click', '.delete', function() {
            $.ajax({
                url: '/admin/deleteYonetici',
                type : 'POST',
                data: {
                    '_token': $('input[name=_token]').val(),
                    'id': $('.id').text()
                },
                success : function(cevap){
                    $('.item' + $('.id').text()).remove();
                    console.log(cevap);
                    if( cevap.giris !== false ){
                        toastr['success']('Kayıtlı Bilgi Silindi!', 'İşlem Başarılı!');
                    }else{
                        toastr['error']('Kayıtlı Bilgi Silinmedi!', 'İşlem Başarısız!');
                    }
                }
            });

        });

        // Edit/Add Modal..............
        function forumDoldur(btnn) {
            $('#gmid').val(btnn.attr('data-id'));

            $('#avatar').attr('src',btnn.attr('data-avatar'));
            $('#kurum_id').val(btnn.attr('data-kurum_id'));
            $('#kullanici_turu').val(btnn.attr('data-kullanici_turu'));
            $('#name').val(btnn.attr('data-name'));
            $('#email').val(btnn.attr('data-email'));
            $('#phone').val(btnn.attr('data-phone'));
            $('#username').val(btnn.attr('data-username'));
            $('#password').val(btnn.attr('data-password'));
        }
        function forumBosalt() {
            $('#gmid').val(0);

            $('#avatar').attr('');
            $('#name').val('');
            $('#email').val('');
            $('#phone').val('');
            $('#username').val('');
            $('#password').val('');
        }
    </script>
@endsection
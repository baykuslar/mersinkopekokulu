@extends('backend.master')
@section('title') Giriş Modal @endsection
@section('govde')
@section('page_level_scripts_css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="/backend/assets/pages/css/portfolio.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('page_css')
    <link rel="stylesheet" href="/backend/css/custom.css">
@endsection
<style>
    .od-gecmis{
        font-size: 14px !important;
        color: white !important;
        animation:biranimasyon 3s linear infinite;
    }
    @keyframes biranimasyon {
        0% {background: #520000
        }
        20% {background: #520000}
        40% {background: #520000}
        60% {background: red}
        80% {background: #2c323c}
        100% {background: red}
    }
    @-moz-keyframes biranimasyon {
        0% {color: #520000}
        20% {color: #520000}
        40% {color: #520000}
        60% {color: red}
        80% {color: #2c323c}
        100% {color: red}
    }
    @-webkit-keyframes biranimasyon {
        0% {color: #520000}
        20% {color: #520000}
        40% {color: #520000}
        60% {color: red}
        80% {color: #2c323c}
        100% {color: red}
    }
    @-o-keyframes biranimasyon {
        0% {color: #520000}
        20% {color: #520000}
        40% {color: #520000}
        60% {color: red}
        80% {color: #2c323c}
        100% {color: red}
    }
</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="index.html">Anasayfa</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Giriş Modal</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-9">
                <div class="portlet light bordered" id="form_wizard_1">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class=" icon-layers font-red"></i>
                            <span class="caption-subject font-red bold"> Giriş Modal -
                                <span class="step-title"> Sayfası Düzenle... </span>
                            </span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <form name="submit_form" class="form-horizontal" id="submit_form" role="form" method="post" action="/admin/giris-modal/{{$viewGirismodal->id}}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            {{method_field('PUT')}}
                            <div class="form-wizard">
                                <div class="form-body">
                                    <ul class="nav nav-pills nav-justified steps">
                                        <li>
                                            <a href="#tab1" data-toggle="tab" class="step">
                                                <span class="number"> 1 </span>
                                                <span class="desc">
                                                    <i class="fa fa-check"></i> Resim Yükleme Alanı</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#tab2" data-toggle="tab" class="step">
                                                <span class="number"> 2 </span>
                                                <span class="desc">
                                                <i class="fa fa-check"></i> Başlık Bilgi Alanı </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#tab3" data-toggle="tab" class="step active">
                                                <span class="number"> 3 </span>
                                                <span class="desc">
                                                    <i class="fa fa-check"></i> Açıklama Alanı </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#tab4" data-toggle="tab" class="step">
                                                <span class="number"> 4 </span>
                                                <span class="desc">
                                                    <i class="fa fa-check"></i> Görünüm </span>
                                            </a>
                                        </li>
                                    </ul>
                                    <div id="bar" class="progress progress-striped" role="progressbar">
                                        <div class="progress-bar progress-bar-success"> </div>
                                    </div>
                                    <div class="tab-content">
                                        <div class="alert alert-danger display-none">
                                            <button class="close" data-dismiss="alert"></button> Bazı form hatalarınız var. Lütfen kontrol edin. </div>
                                        <div class="alert alert-success display-none">
                                            <button class="close" data-dismiss="alert"></button> Form doğrulamanız başarılı! </div>
                                        <div class="tab-pane active" id="tab1">
                                            <h3 class="block">Gerekli alanları doldurunuz.</h3>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div style="padding: 80px">
                                                        <div class="form-group last">
                                                        <input type="file" name="resim" id="resim"><br>
                                                    </div>
                                                </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="tab-pane" id="tab2">
                                            <h3 class="block">Başlık Alanı</h3>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Başlık
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" name="baslik" value="{{$viewGirismodal->baslik}}"/>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Link ver!
                                                    <span class=""> * </span>
                                                </label>
                                                <div class="col-md-7">
                                                    <input type="text" class="form-control" name="link"  value="{{$viewGirismodal->link}}" placeholder=""/>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="tab-pane" id="tab3">
                                            <h3 class="block">Açıklama Alanı</h3>
                                            <div class="row">
                                                <div class="col-md-12">

                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Açıklaması
                                                            <span class="required"> * </span>
                                                        </label>
                                                        <div class="col-md-7">
                                                            <input type="text" class="form-control" name="aciklamasi"  value="{{$viewGirismodal->aciklamasi}}"/>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="tab4">
                                            <h3 class="block">Görünüm</h3>

                                            <div class="form-group">
                                                <div class="col-md-7">
                                                    <p class="form-control-static" data-display="baslik"> </p>
                                                </div>
                                                <div class="col-md-7">
                                                    <p class="form-control-static" data-display="aciklamasi"> </p>
                                                </div>
                                                <div class="col-md-7">
                                                    <p class="form-control-static" data-display="link"> </p>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <a href="javascript:;" class="btn default button-previous">
                                                <i class="fa fa-angle-left"></i> Geri </a>
                                            <a href="javascript:;" class="btn btn-outline green button-next"> İleri
                                                <i class="fa fa-angle-right"></i>
                                            </a>
                                            {{--<a href="#"  class="btn green button-submit"> Tamamlandı--}}
                                            {{--<i class="fa fa-check"></i>--}}
                                            {{--</a>--}}
                                            <button type="submit" class="btn blue button-submit"><i class="fa fa-check"></i> Gönder</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-3"> <h3> İlan Durumu!!! </h3>
                <p>
                <span style="font-size: 18px">İlan şuan</span>
                    @if($viewGirismodal->aktif_pasif=='on')
                        <b>AKTİF</b> durumdadır! PASİF Etmek İçin buraya tıklayınız.
                @else
                       <b>PASİF</b> durumdadır! AKTİF Etmek İçin buraya tıklayınız.
                    @endif
                </p>
                    @if($viewGirismodal->aktif_pasif=='on')
                        <button type="button" class="btn btn-sm btn-danger edit-modal" title="Aktif Etmek İçin Tıklayınız"
                                data-aktif_pasif="off"
                                data-id="{{$viewGirismodal->id}}"
                                style="margin-right: 0px;">
                            <i class="fa fa-check" aria-hidden="true"></i> <b class="yz-gizle">PASİF YAP</b>
                        </button>
                    @elseif($viewGirismodal->aktif_pasif=='off')
                        <button type="button" class="btn btn-sm btn-success edit-modal" title="Pasif Etmek İçin Tıklayınız"
                                data-aktif_pasif="on"
                                data-id="{{$viewGirismodal->id}}"
                                style="margin-right: 0px;">
                            <i class="fa fa-check" aria-hidden="true"></i> <b class="yz-gizle">AKTİF YAP</b>
                        </button>
                    @endif

                <div class="modal-content" style="margin-top: 15px">
                    <div class="modal-header">
                        <h4 style="color: #8f9ea6;font-size: 15px;text-align: center;color: #0a9c00;" class="modal-title">{{$viewAyarlar->site_basligi}}</h4>
                    </div>
                    <div class="modal-body">
                        <h3 class="text-center" style="color: orangered;font-size: 15px"> {{$viewGirismodal->baslik}}</h3>
                        <p>
                            <img src="/uploads/giris-modal/{{$viewGirismodal->resim}}" alt="" class="img-responsive">
                        </p>
                        <p class="text-center" style="font-size: 14px">{{$viewGirismodal->aciklamasi}}</p>
                    </div>
                    <div class="modal-footer">
                        <h4 class="text-center" style="color: #8f9ea6;text-transform:lowercase !important;font-size: 16px" class="modal-title">{{$viewAyarlar->site_adresi}}
                        </h4>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

<!-- //Dili Aktif Pasif Yapma Modalı -->
<div class="modal fade" id="myModal" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog" style="width: 265px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                </button>
                <h4 class="modal-title custom_align" id="Heading"> Kayıt Güncelle</h4>
            </div>
            <div class="modal-body">
                <p>@include('backend.giris-modal.edit')</p>
            </div>
        </div>
    </div>
</div>
@endsection
@section('page_level_scripts_js')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="/backend/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js" type="text/javascript"></script>
@endsection
@section('page_level_scripts_js_end')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="/backend/assets/pages/scripts/portfolio-1.min.js" type="text/javascript"></script>
@endsection
@section('dahil_edilecek_js')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#panelPrecios [data-toggle="tooltip"]').tooltip({
                animated: 'fade',
                placement: 'bottom',
                html: true
            });

        });
    </script>
    <style>
        .swal-overlay {
            z-index: 10053;
        }
    </style>
    <script>
        $(document).ready(function(){
            $(document).on('change', '#resim', function(){
                var name = document.getElementById("resim").files[0].name;
                var ext = name.split('.').pop().toLowerCase();
                if(jQuery.inArray(ext, ['gif','png','jpg','jpeg']) == -1)
                {
                    $('#resim').val('');
                    sweetAlert("UYARI!", "Dosya Formatı Desteklemiyor", "error");
                    return false;
                }
                var oFReader = new FileReader();
                oFReader.readAsDataURL(document.getElementById("resim").files[0]);
                var f = document.getElementById("resim").files[0];
                var fsize = f.size||f.fileSize;
                if(fsize > 150000) {
                    $('#resim').val('');
                    sweetAlert("UYARI!", "Dosya boyutunuz 150 kb'tan küçük olmalı!", "error");
                }
            });
        });
    </script>
@endsection



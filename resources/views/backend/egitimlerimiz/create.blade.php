
<form id="frm-galeri" class="form-horizontal" method="post" action="/admin/egitimlerimiz" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="form-body">
        <div class="form-group last">
            <div class="col-md-9">
                <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="fileinput-new thumbnail" style="width: 120px; height: 90px;">
                        <img src="" alt="" id="resim" />
                    </div>
                    {{--<label>Seçilen Resim:  <input type="button" id="resim_adi"></label>--}}
                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 120px; max-height: 90px;"> </div>
                    <div>
                                    <span class="btn green btn-file btn-xs">
                                        <span class="fileinput-new"> Resim Seçiniz </span>
                                        <span class="fileinput-exists"> Değiştir </span>
                                        <input type="file" name="resim" id="resim">
                                    </span>
                        <a href="javascript:;" class="btn red fileinput-exists btn-xs" data-dismiss="fileinput"> Sil </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group  margin-top-20">
            <div class="col-md-12">
                <div class="input-icon right">
                    <i class="fa"></i>
                    <input type="text" class="form-control" name="baslik" id="baslik" autocomplete="off">
                </div>
            </div>
        </div>
        <div class="form-group  margin-top-20">
            <div class="col-md-12">
                <div class="input-icon right">
                    <i class="fa"></i>
                    <textarea type="text" class="form-control" rows="12" name="aciklamasi" id="aciklamasi"></textarea>
                </div>
            </div>
        </div>

        <div class="form-group  margin-top-20">
            <div class="col-md-12">
                <div class="input-icon right">
                    <i class="fa"></i>
                    <select multiple id="meta_keywords" name="meta_keywords[]" data-role="tagsinput"></select>
                </div>
            </div>
        </div>
        <div class="form-group  margin-top-20">
            <div class="col-md-12">
                <div class="input-icon right">
                    <i class="fa"></i>
                    <textarea type="text" class="form-control" rows="3" name="meta_description" id="meta_description"></textarea>
                </div>
            </div>
        </div>

        <input type="hidden" name="gmid" id="gmid" value="0"/>

        <div class="modal-footer">
            <button type="submit" class="btn green btn-circle add">
                <span class='glyphicon glyphicon-check'></span> Kaydet
            </button>
            <button type="button" class="btn red btn-circle" data-dismiss="modal">
                <span class='glyphicon glyphicon-remove'></span> Vazgeç
            </button>
        </div>
    </div>
</form>

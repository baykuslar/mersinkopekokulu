
                <form id="frm-ekibimiz" class="form-horizontal" method="post" action="/admin/backend/kurumsal/ekibimiz" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-body">
                        <div class="alert alert-danger display-hide">
                            <button class="close" data-close="alert"></button> Bazı form hatalarınız var. Lütfen kontrol ediniz. </div>
                        <div class="alert alert-success display-hide">
                            <button class="close" data-close="alert"></button> Form doğrulamanız başarılı!
                        </div>
                        <div class="form-group last">
                            <label class="control-label col-md-3">Profil Resmi</label>
                            <div class="col-md-9">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail" style="width: 130px; height: 130px;">
                                        <img src="" alt="" id="profil_resim" />
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                    <div>
                                <span class="btn default btn-file">
                                    <span class="fileinput-new"> Resim Seçiniz </span>
                                    <span class="fileinput-exists"> Değiştir </span>
                                    <input type="file" name="profil_resim" id="profil_resim"> </span>
                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Sil </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group  margin-top-20">
                            <label class="control-label col-md-3">Adı Soyadı
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-9">
                                <div class="input-icon right">
                                    <i class="fa"></i>
                                    <input type="text" class="form-control" name="adi_soyadi" id="adi_soyadi" required/> </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3"> Unvan</label>
                            <div class="col-md-9">
                                <select name="unvan" id="unvan" class="form-control ">
                                    <option value="">--Seçiniz--</option>
                                    <option value="Kurucu">Kurucu</option>
                                    <option value="Köpek Bakım Uzmanı">Köpek Bakım Uzmanı</option>
                                    <option value="Uzman Köpek Eğitmeni">Uzman Köpek Eğitmeni</option>
                                    <option value="MARS K-9 İşletme Müdürü">MARS K-9 İşletme Müdürü</option>
                                    <option value="Bahçıvan">Bahçıvan</option>
                                    <option value="Bahçe Bakım Görevlisi">Bahçe Bakım Görevlisi</option>
                                    <option value="Muhasebe Müdürü">Muhasebe Müdürü</option>
                                    <option value="Digeri">Diğeri</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Email Adresi
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-9">
                                <div class="input-icon right">
                                    <i class="fa"></i>
                                    <input type="text" class="form-control" name="email" id="email"/> </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Telefon No.
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-9">
                                <div class="input-icon right">
                                    <i class="fa"></i>
                                    <input type="text" class="form-control" name="telefon" id="telefon" data-inputmask="'mask': '9 (999) 999 99 99'"/> </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Gsm No.
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-9">
                                <div class="input-icon right">
                                    <i class="fa"></i>
                                    <input type="text" class="form-control" name="gsm" id="gsm" data-inputmask="'mask': '9 (999) 999 99 99'" /> </div>
                            </div>
                        </div>
                        <div class="form-group  margin-top-20">
                            <label class="control-label col-md-3">Kısaca Hakkında
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-9">
                                <div class="input-icon right">
                                    <i class="fa"></i>
                                    <textarea type="text" class="form-control" name="hakkinda" id="hakkinda" maxlength="200" > </textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-group  margin-top-20">
                            <label class="control-label col-md-3">Facebook
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-9">
                                <div class="input-icon right">
                                    <i class="fa"></i>
                                    <input type="text" class="form-control" name="facebook" id="facebook" /> </div>
                            </div>
                        </div>
                        <div class="form-group  margin-top-20">
                            <label class="control-label col-md-3">Twitter
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-9">
                                <div class="input-icon right">
                                    <i class="fa"></i>
                                    <input type="text" class="form-control" name="twitter" id="twitter" /> </div>
                            </div>
                        </div>
                        <div class="form-group  margin-top-20">
                            <label class="control-label col-md-3">İnstagram
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-9">
                                <div class="input-icon right">
                                    <i class="fa"></i>
                                    <input type="text" class="form-control" name="instagram" id="instagram"/> </div>
                            </div>
                        </div>
                        <div class="form-group  margin-top-20">
                            <label class="control-label col-md-3">Whatsapp
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-9">
                                <div class="input-icon right">
                                    <i class="fa"></i>
                                    <input type="text" class="form-control" name="whatsapp" id="whatsapp"/> </div>
                            </div>
                        </div>

                    </div>

                    <input type="hidden" name="gmid" id="gmid" value="0"/>

                    <div class="modal-footer">
                        <button type="submit" class="btn green btn-circle add">
                            <span class='glyphicon glyphicon-check'></span> Kaydet
                        </button>
                        <button type="button" class="btn red btn-circle" data-dismiss="modal">
                            <span class='glyphicon glyphicon-remove'></span> Vazgeç
                        </button>
                    </div>

                </form>
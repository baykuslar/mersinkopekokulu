@extends('backend.master')
@section('title') Anasayfa @endsection
@section('govde')
@section('page_level_scripts_css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    {{--<link href="/backend/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />--}}
    {{--<link href="/backend/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />--}}
    {{--<link href="/backend/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />--}}
    {{--<link href="/backend/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet" type="text/css" />--}}
    {{--<link href="/backend/assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet" type="text/css" />--}}
@endsection
    <style type="text/css">
        .thumb-lg {
            height: 88px;
            width: 88px;
        }
        .card {
            background: #fff;
            border-radius: 2px;
            display: inline-block;
            height: auto;
            margin: 1rem;
            position: relative;
            width: 100%;
        }

        .card-1 {
            box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
            transition: all 0.3s cubic-bezier(.25,.8,.25,1);
        }

        .card-1:hover {
            box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);
        }
    </style>

    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="index.html">Anasayfa</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Yöneticiler</span>
                    </li>
                </ul>
            </div>
            <!-- END PAGE BAR -->
            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title"> Managed Datatables
                <small>managed datatable samples</small>
            </h1>
            <!-- END PAGE HEADER-->
            <div class="m-heading-1 border-green m-bordered">
                <button id="sample_editable_1_new" class="btn sbold green btnCreate b-r"> Yeni Kayıt Ekle
                    <i class="fa fa-plus"></i>
                </button>
            </div>
            <div class="row" style="background: #e1e5e9;">
                {{ csrf_field() }}
                <?php $no=1 ?>
                @foreach($ekibimiz as $ekibimiz)
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 item{{$ekibimiz->id}}">
                    <div class="profile-sidebar">
                        <div class="portlet light profile-sidebar-portlet card card-1" style="border-radius: 20px !important;">
                            <div class="profile-userpic">
                                @if($ekibimiz->profil_resim == null)
                                    <img src="/uploads/profil_resimleri/default.png" alt="" class="img-responsive">
                                @else
                                    <img src="/uploads/profil_resimleri/{{$ekibimiz->profil_resim}}" alt="" class="img-responsive">
                                @endif
                            </div>
                            <div class="profile-usertitle">
                                <div class="profile-usertitle-name"> {{$ekibimiz->adi_soyadi}} </div>
                                <div class="profile-usertitle-job"> {{$ekibimiz->unvan}} </div>
                            </div>
                            <div class="profile-userbuttons">
                                <button type="button" class="btn btn-circle green btn-sm guncelle-btn"
                                        data-id="{{$ekibimiz->id}}"
                                        data-profil_resim="/uploads/profil_resimleri/{{$ekibimiz->profil_resim}}"
                                        data-adi_soyadi="{{$ekibimiz->adi_soyadi}}"
                                        data-unvan="{{$ekibimiz->unvan}}"
                                        data-email="{{$ekibimiz->email}}"
                                        data-telefon="{{$ekibimiz->telefon}}"
                                        data-gsm="{{$ekibimiz->gsm}}"
                                        data-hakkinda="{{$ekibimiz->hakkinda}}"
                                        data-facebook="{{$ekibimiz->facebook}}"
                                        data-instagram="{{$ekibimiz->instagram}}"
                                        data-twitter="{{$ekibimiz->twitter}}"
                                        data-whatsapp="{{$ekibimiz->whatsapp}}"
                                        data-username="{{$ekibimiz->username}}">
                                    <i class="fa fa-refresh" aria-hidden="true"></i>
                                    <span class="guncelle">GÜNCELLE</span>
                                </button>
                                <button title="Sil" class="btn btn-circle red btn-sm delete-modal"
                                        data-toggle="tooltip" data-placement="right" title="Sil"
                                        data-id="{{$ekibimiz->id}}" data-title="{{$ekibimiz->adi_soyadi}}"><i class="fa fa-trash"></i> SİL
                                </button>
                            </div>
                            <div class="profile-usermenu">
                                <ul class="nav text-center">
                                    <li class="active"><a href="#"><i class="fa fa-fax"></i> Tel : {{$ekibimiz->telefon}} </a></li>
                                    <li class="active"><a href="#"><i class="fa fa-phone"></i> Gsm: {{$ekibimiz->gsm}} </a></li>
                                    <li><a href="#"><i class="fa fa-envelope"></i> Email: {{$ekibimiz->email}} </a></li>
                                    <li class="active text-center">
                                    <div class="btn-group">
                                        <button class="btn btn-circle green btn-outline
                                        btn-xs dropdown-toggle" type="button"> Facebook
                                            <i class="fa fa-facebook"></i>
                                        </button>
                                    </div>
                                    <div class="btn-group">
                                        <button class="btn btn-circle green btn-outline
                                        btn-xs dropdown-toggle" type="button"> Twitter
                                            <i class="fa fa-twitter"></i>
                                        </button>
                                    </div>
                                    <div class="btn-group">
                                        <button class="btn btn-circle green btn-outline
                                        btn-xs dropdown-toggle" type="button"> İnstagram
                                            <i class="fa fa-instagram"></i>
                                        </button>
                                    </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>

        <!-- Delete Item Modal -->
        <div class="sweet-alert showSweetAlert" id="delete-modal" style="border-radius: 24px !important;">
            <div class="modal-header">
                <div class="waricon">
                    <i style="color:red;font-size: 60px;line-height: 60px;" class="fa fa-exclamation-circle" aria-hidden="true"></i>
                </div>
                <span class="hidden id"></span>
                <h2 style="color:orangered" class="title"> </h2>
                <h3>Bilgisi silinecektir?</h3>
                <p>Bilgiyi tamamen silmek İstediğinizden emin misiniz!</p>
            </div>
            <div class="modal-footer" style="text-align:center !important">
                <button style="margin: 0px 15px;padding: 7px 20px;float: left" type="button" class="btn btn-circle red btn-sm  actionBtn" data-dismiss="modal">
                    <i class="fa fa-trash fa-lg" aria-hidden="true"></i> Delete
                </button>
                <button style="margin: 0px 15px;padding: 7px 20px;" type="button" class="btn btn-circle green btn-sm"  data-dismiss="modal"
                        onclick="$.Notification.autoHideNotify('error', 'top right', 'İşlem Başarısız','Kayıt Silinmedi!...')">
                    <i class="fa fa-times fa-lg" aria-hidden="true"></i> Vazgeç
                </button>
            </div>
        </div>

        <!-- //Edit Modal -->
        <div class="modal fade" id="myModal" role="dialog" aria-labelledby="edit" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                        </button>
                        <h4 class="modal-title custom_align" id="Heading">Kayıt Ekle/Güncelle</h4>
                    </div>
                    <div class="modal-body">
                        @include('backend.kurumsal.ekibimiz.create')
                    </div>
                </div>
            </div>
        </div>
@endsection
@section('page_level_scripts_js')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
@endsection
@section('page_level_scripts_js_end')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
@endsection
@section('dahil_edilecek_js')
    <script type="text/javascript">
        $(document).ready(function() {
            $(":input").inputmask();

            $('.btnCreate').on('click', function () {
                $('#myModal').modal('show');
                forumBosalt();
            });

            $(document).on('click', '.guncelle-btn', function () {
                $('#myModal').modal('show');
                forumDoldur($(this));
            });
        });
        // $(document).ready(function(){
        //     $(":input").inputmask();
        // });
        $(document).on('click', '.delete-modal', function() {
            $('.actionBtn').addClass('delete');
            $('.id').text($(this).data('id'));
            $('.deleteContent').show();
            $('.title').html($(this).data('title'));
            $('#delete-modal').modal('show');
        });
        $('.modal-footer').on('click', '.delete', function() {
            $.ajax({
                url: '/admin/deleteEkibimiz',
                type : 'POST',
                data: {
                    '_token': $('input[name=_token]').val(),
                    'id': $('.id').text()
                },
                success : function(cevap){
                    $('.item' + $('.id').text()).remove();
                    console.log(cevap);
                    if( cevap.giris !== false ){
                        toastr['success']('Kayıtlı Bilgi Silindi!', 'İşlem Başarılı!');
                        window.location.reload(true);
                    }else{
                        toastr['error']('Kayıtlı Bilgi Silinmedi!', 'İşlem Başarısız!');
                    }
                }
            });

        });

        // Add / Edit Data ---------------------------------
        function forumDoldur(btnn) {
            $('#gmid').val(btnn.attr('data-id'));
            $('#profil_resim').attr('src',btnn.attr('data-profil_resim'));
            $('#adi_soyadi').val(btnn.attr('data-adi_soyadi'));
            $('#unvan').val(btnn.attr('data-unvan'));
            $('#email').val(btnn.attr('data-email'));
            $('#telefon').val(btnn.attr('data-telefon'));
            $('#gsm').val(btnn.attr('data-gsm'));
            $('#hakkinda').val(btnn.attr('data-hakkinda'));
            $('#facebook').val(btnn.attr('data-facebook'));
            $('#instagram').val(btnn.attr('data-instagram'));
            $('#twitter').val(btnn.attr('data-twitter'));
            $('#whatsapp').val(btnn.attr('data-whatsapp'));
        }

        function forumBosalt() {
            $('#gmid').val(0);
            $('#profil_resim').attr('');
            $('#adi_soyadi').val('');
            $('#unvan').val('');
            $('#email').val('');
            $('#telefon').val('');
            $('#gsm').val('');
            $('#hakkinda').val('');
            $('#facebook').val('');
            $('#instagram').val('');
            $('#twitter').val('');
            $('#whatsapp').val('');
        }
    </script>
@endsection
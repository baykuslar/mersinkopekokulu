@extends('backend.master')
@section('title') Galeri @endsection
@section('govde')
@section('page_level_scripts_css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="/backend/assets/pages/css/portfolio.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('page_css')
    <link rel="stylesheet" href="/backend/css/custom.css">
@endsection
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="index.html">Anasayfa</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Hakkımızda</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered" id="form_wizard_1">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class=" icon-layers font-red"></i>
                            <span class="caption-subject font-red bold"> Hakkımızda -
                                        <span class="step-title"> Sayfası Düzenle... </span>
                                    </span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <form name="submit_form" class="form-horizontal" id="submit_form" role="form" method="post" action="/admin/hakkimizda/{{$hakkimizda->id}}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            {{method_field('PUT')}}
                            <div class="form-wizard">
                                <div class="form-body">
                                    <ul class="nav nav-pills nav-justified steps">
                                        <li>
                                            <a href="#tab1" data-toggle="tab" class="step">
                                                <span class="number"> 1 </span>
                                                <span class="desc">
                                                    <i class="fa fa-check"></i> Resim Yükleme Alanı</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#tab2" data-toggle="tab" class="step">
                                                <span class="number"> 2 </span>
                                                <span class="desc">
                                                <i class="fa fa-check"></i> Hakkımızda Yazı Alanı </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#tab3" data-toggle="tab" class="step active">
                                                <span class="number"> 3 </span>
                                                <span class="desc">
                                                    <i class="fa fa-check"></i> Banner Resim Yükleme Alanı </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#tab4" data-toggle="tab" class="step">
                                                <span class="number"> 4 </span>
                                                <span class="desc">
                                                    <i class="fa fa-check"></i> Görünüm </span>
                                            </a>
                                        </li>
                                    </ul>
                                    <div id="bar" class="progress progress-striped" role="progressbar">
                                        <div class="progress-bar progress-bar-success"> </div>
                                    </div>
                                    <div class="tab-content">
                                        <div class="alert alert-danger display-none">
                                            <button class="close" data-dismiss="alert"></button> Bazı form hatalarınız var. Lütfen kontrol edin. </div>
                                        <div class="alert alert-success display-none">
                                            <button class="close" data-dismiss="alert"></button> Form doğrulamanız başarılı! </div>
                                        <div class="tab-pane active" id="tab1">
                                            <h3 class="block">Gerekli alanları doldurunuz.</h3>
                                            <div class="row">
                                                <div class="col-md-12">

                                                    <div class="form-group last col-md-2" style="padding: 10px">
                                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                <div class="fileinput-new thumbnail" style="width: 90px; height: 60px;">
                                                                    @if($hakkimizda->resim_1==null)
                                                                        <img src="/frontend/images/hakkimizda/default.jpg" alt="" />
                                                                    @else
                                                                        <img src="/frontend/images/hakkimizda/{{$hakkimizda->resim_1}}" alt="" id="resim_1" />
                                                                    @endif
                                                                </div>
                                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 120px; max-height: 90px;"> </div>
                                                                <div>
                                                                    <span class="btn green btn-file  btn-xs">
                                                                        <span class="fileinput-new"> Resim Seçiniz </span>
                                                                        <span class="fileinput-exists"> Değiştir </span>
                                                                        <input type="file" name="resim_1" id="resim_1">
                                                                    </span>
                                                                    <a href="javascript:;" class="btn red fileinput-exists  btn-xs" data-dismiss="fileinput"> Sil </a>
                                                                </div>
                                                                <label style="margin: 10px">Resim-Başlık-1</label>
                                                            </div>
                                                        <div class="col-md-12" style="margin-top:8px;padding-left: 0px;">
                                                            <input type="text" class="form-control" maxlength="25" name="baslik_1"  value="{{$hakkimizda->baslik_1}}" >
                                                        </div>
                                                    </div>

                                                    <div class="form-group last col-md-2" style="padding: 10px">
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            <div class="fileinput-new thumbnail" style="width: 90px; height: 60px;">
                                                                @if($hakkimizda->resim_2==null)
                                                                    <img src="/frontend/images/hakkimizda/default.jpg" alt="" />
                                                                @else
                                                                    <img src="/frontend/images/hakkimizda/{{$hakkimizda->resim_2}}" alt="" id="resim_2" />
                                                                @endif
                                                            </div>
                                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 120px; max-height: 90px;"> </div>
                                                            <div>
                                                                    <span class="btn green btn-file  btn-xs">
                                                                        <span class="fileinput-new"> Resim Seçiniz </span>
                                                                        <span class="fileinput-exists"> Değiştir </span>
                                                                        <input type="file" name="resim_2" id="resim_2">
                                                                    </span>
                                                                <a href="javascript:;" class="btn red fileinput-exists  btn-xs" data-dismiss="fileinput"> Sil </a>
                                                            </div>
                                                            <label style="margin: 10px">Resim-Başlık-2</label>
                                                        </div>
                                                        <div class="col-md-12" style="margin-top:8px;padding-left: 0px;">
                                                            <input type="text" class="form-control" maxlength="25" name="baslik_2"  value="{{$hakkimizda->baslik_2}}" >
                                                        </div>
                                                    </div>

                                                    <div class="form-group last col-md-2" style="padding: 10px">
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            <div class="fileinput-new thumbnail" style="width: 90px; height: 60px;">
                                                                @if($hakkimizda->resim_3==null)
                                                                    <img src="/frontend/images/hakkimizda/default.jpg" alt="" />
                                                                @else
                                                                    <img src="/frontend/images/hakkimizda/{{$hakkimizda->resim_3}}" alt="" id="resim_3" />
                                                                @endif
                                                            </div>
                                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 120px; max-height: 90px;"> </div>
                                                            <div>
                                                                    <span class="btn green btn-file  btn-xs">
                                                                        <span class="fileinput-new"> Resim Seçiniz </span>
                                                                        <span class="fileinput-exists"> Değiştir </span>
                                                                        <input type="file" name="resim_3" id="resim_3">
                                                                    </span>
                                                                <a href="javascript:;" class="btn red fileinput-exists  btn-xs" data-dismiss="fileinput"> Sil </a>
                                                            </div>
                                                            <label style="margin: 10px">Resim-Başlık-3</label>
                                                        </div>
                                                        <div class="col-md-12" style="margin-top:8px;padding-left: 0px;">
                                                            <input type="text" class="form-control" maxlength="25" name="baslik_3"  value="{{$hakkimizda->baslik_3}}" >
                                                        </div>
                                                    </div>

                                                    <div class="form-group last col-md-2" style="padding: 10px">
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            <div class="fileinput-new thumbnail" style="width: 90px; height: 60px;">
                                                                @if($hakkimizda->resim_4==null)
                                                                    <img src="/frontend/images/hakkimizda/default.jpg" alt="" />
                                                                @else
                                                                    <img src="/frontend/images/hakkimizda/{{$hakkimizda->resim_4}}" alt="" id="resim_4" />
                                                                @endif
                                                            </div>
                                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 120px; max-height: 90px;"> </div>
                                                            <div>
                                                                    <span class="btn green btn-file  btn-xs">
                                                                        <span class="fileinput-new"> Resim Seçiniz </span>
                                                                        <span class="fileinput-exists"> Değiştir </span>
                                                                        <input type="file" name="resim_4" id="resim_4">
                                                                    </span>
                                                                <a href="javascript:;" class="btn red fileinput-exists  btn-xs" data-dismiss="fileinput"> Sil </a>
                                                            </div>
                                                            <label style="margin: 10px">Resim-Başlık-4</label>
                                                        </div>
                                                        <div class="col-md-12" style="margin-top:8px;padding-left: 0px;">
                                                            <input type="text" class="form-control" maxlength="25" name="baslik_4"  value="{{$hakkimizda->baslik_9}}" >
                                                        </div>
                                                    </div>

                                                    <div class="form-group last col-md-2" style="padding: 10px">
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            <div class="fileinput-new thumbnail" style="width: 90px; height: 60px;">
                                                                @if($hakkimizda->resim_5==null)
                                                                    <img src="/frontend/images/hakkimizda/default.jpg" alt="" />
                                                                @else
                                                                    <img src="/frontend/images/hakkimizda/{{$hakkimizda->resim_5}}" alt="" id="resim_5" />
                                                                @endif
                                                            </div>
                                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 120px; max-height: 90px;"> </div>
                                                            <div>
                                                                    <span class="btn green btn-file  btn-xs">
                                                                        <span class="fileinput-new"> Resim Seçiniz </span>
                                                                        <span class="fileinput-exists"> Değiştir </span>
                                                                        <input type="file" name="resim_5" id="resim_5">
                                                                    </span>
                                                                <a href="javascript:;" class="btn red fileinput-exists  btn-xs" data-dismiss="fileinput"> Sil </a>
                                                            </div>
                                                            <label style="margin: 10px">Resim-Başlık-5</label>
                                                        </div>
                                                        <div class="col-md-12" style="margin-top:8px;padding-left: 0px;">
                                                            <input type="text" class="form-control" maxlength="25" name="baslik_5"  value="{{$hakkimizda->baslik_5}}" >
                                                        </div>
                                                    </div>

                                                    <div class="form-group last col-md-2" style="padding: 10px">
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            <div class="fileinput-new thumbnail" style="width: 90px; height: 60px;">
                                                                @if($hakkimizda->resim_6==null)
                                                                    <img src="/frontend/images/hakkimizda/default.jpg" alt="" />
                                                                @else
                                                                    <img src="/frontend/images/hakkimizda/{{$hakkimizda->resim_6}}" alt="" id="resim_6" />
                                                                @endif
                                                            </div>
                                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 120px; max-height: 90px;"> </div>
                                                            <div>
                                                                    <span class="btn green btn-file  btn-xs">
                                                                        <span class="fileinput-new"> Resim Seçiniz </span>
                                                                        <span class="fileinput-exists"> Değiştir </span>
                                                                        <input type="file" name="resim_6" id="resim_6">
                                                                    </span>
                                                                <a href="javascript:;" class="btn red fileinput-exists  btn-xs" data-dismiss="fileinput"> Sil </a>
                                                            </div>
                                                            <label style="margin: 10px">Resim-Başlık-6</label>
                                                        </div>
                                                        <div class="col-md-12" style="margin-top:8px;padding-left: 0px;">
                                                            <input type="text" class="form-control" maxlength="25" name="baslik_6"  value="{{$hakkimizda->baslik_6}}" >
                                                        </div>
                                                    </div>

                                                    <div class="form-group last col-md-2" style="padding: 10px">
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            <div class="fileinput-new thumbnail" style="width: 90px; height: 60px;">
                                                                @if($hakkimizda->resim_7==null)
                                                                    <img src="/frontend/images/hakkimizda/default.jpg" alt="" />
                                                                @else
                                                                    <img src="/frontend/images/hakkimizda/{{$hakkimizda->resim_7}}" alt="" id="resim_7" />
                                                                @endif
                                                            </div>
                                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 120px; max-height: 90px;"> </div>
                                                            <div>
                                                                    <span class="btn green btn-file  btn-xs">
                                                                        <span class="fileinput-new"> Resim Seçiniz </span>
                                                                        <span class="fileinput-exists"> Değiştir </span>
                                                                        <input type="file" name="resim_7" id="resim_7">
                                                                    </span>
                                                                <a href="javascript:;" class="btn red fileinput-exists  btn-xs" data-dismiss="fileinput"> Sil </a>
                                                            </div>
                                                            <label style="margin: 10px">Resim-Başlık-7</label>
                                                        </div>
                                                        <div class="col-md-12" style="margin-top:8px;padding-left: 0px;">
                                                            <input type="text" class="form-control" maxlength="25" name="baslik_7"  value="{{$hakkimizda->baslik_7}}" >
                                                        </div>
                                                    </div>

                                                    <div class="form-group last col-md-2" style="padding: 10px">
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            <div class="fileinput-new thumbnail" style="width: 90px; height: 60px;">
                                                                @if($hakkimizda->resim_8==null)
                                                                    <img src="/frontend/images/hakkimizda/default.jpg" alt="" />
                                                                @else
                                                                    <img src="/frontend/images/hakkimizda/{{$hakkimizda->resim_8}}" alt="" id="resim_8" />
                                                                @endif
                                                            </div>
                                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 120px; max-height: 90px;"> </div>
                                                            <div>
                                                                    <span class="btn green btn-file  btn-xs">
                                                                        <span class="fileinput-new"> Resim Seçiniz </span>
                                                                        <span class="fileinput-exists"> Değiştir </span>
                                                                        <input type="file" name="resim_8" id="resim_8">
                                                                    </span>
                                                                <a href="javascript:;" class="btn red fileinput-exists  btn-xs" data-dismiss="fileinput"> Sil </a>
                                                            </div>
                                                            <label style="margin: 10px">Resim-Başlık-8</label>
                                                        </div>
                                                        <div class="col-md-12" style="margin-top:8px;padding-left: 0px;">
                                                            <input type="text" class="form-control" maxlength="25" name="baslik_8"  value="{{$hakkimizda->baslik_8}}" >
                                                        </div>
                                                    </div>

                                                    <div class="form-group last col-md-2" style="padding: 10px">
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            <div class="fileinput-new thumbnail" style="width: 90px; height: 60px;">
                                                                @if($hakkimizda->resim_9==null)
                                                                    <img src="/frontend/images/hakkimizda/default.jpg" alt="" />
                                                                @else
                                                                    <img src="/frontend/images/hakkimizda/{{$hakkimizda->resim_9}}" alt="" id="resim_9" />
                                                                @endif
                                                            </div>
                                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 120px; max-height: 90px;"> </div>
                                                            <div>
                                                                    <span class="btn green btn-file  btn-xs">
                                                                        <span class="fileinput-new"> Resim Seçiniz </span>
                                                                        <span class="fileinput-exists"> Değiştir </span>
                                                                        <input type="file" name="resim_9" id="resim_9">
                                                                    </span>
                                                                <a href="javascript:;" class="btn red fileinput-exists  btn-xs" data-dismiss="fileinput"> Sil </a>
                                                            </div>
                                                            <label style="margin: 10px">Ana Resim-Başlık</label>
                                                        </div>
                                                        <div class="col-md-12" style="margin-top:8px;padding-left: 0px;">
                                                            <input type="text" class="form-control" maxlength="25" name="baslik_9"  value="{{$hakkimizda->baslik_9}}" >
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                        </div>

                                        <div class="tab-pane" id="tab2">
                                            <h3 class="block">Hakkımızda Yazı Alanı</h3>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Hakkımızda
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-7">
                                                    <textarea  name="hakkimizda" id="maxlength_textarea" class="form-control" rows="20" cols="100" maxlength="1000" placeholder="Maximum 600 karakter yazabilirsiniz.">{{$hakkimizda->hakkimizda}}</textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="tab-pane" id="tab3">
                                            <h3 class="block">Hakkında Özel Alan</h3>
                                            <div class="row">
                                                <div class="col-md-12">

                                                    <div class="form-group last  col-md-9">
                                                        <div class="col-md-12">
                                                            <label class="control-label col-md-3">Özel Başlık</label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control" maxlength="25" name="ozel_baslik"  value="{{$hakkimizda->ozel_baslik}}" >
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12" style="margin-top:8px">
                                                            <label class="control-label col-md-3">Özel Yazı Alanı</label>
                                                            <div class="col-md-9">
                                                                <textarea type="text" class="form-control" name="ozel_yazi">{{$hakkimizda->ozel_yazi}}</textarea>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="tab4">
                                            <h3 class="block">Görünüm</h3>

                                            <h4 class="form-section">Hakkımızda</h4>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Hakkımızda:</label>
                                                <div class="col-md-7">
                                                    <p class="form-control-static" data-display="hakkimizda"> </p>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <a href="javascript:;" class="btn default button-previous">
                                                <i class="fa fa-angle-left"></i> Geri </a>
                                            <a href="javascript:;" class="btn btn-outline green button-next"> İleri
                                                <i class="fa fa-angle-right"></i>
                                            </a>
                                            <button type="submit" class="btn blue button-submit"><i class="fa fa-check"></i> Gönder</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection
@section('page_level_scripts_js')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="/backend/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js" type="text/javascript"></script>
@endsection
@section('page_level_scripts_js_end')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="/backend/assets/pages/scripts/portfolio-1.min.js" type="text/javascript"></script>
@endsection
@section('dahil_edilecek_js')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#panelPrecios [data-toggle="tooltip"]').tooltip({
                animated: 'fade',
                placement: 'bottom',
                html: true
            });

        });
    </script>
@endsection



@extends('backend.master')
@section('title') İş Başvuru Görünüm @endsection
@section('govde')
    <style>
        .m-r{
            margin: 6px;
        }
        .c-l{
            color: #0c203a;
        }
    </style>
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <div class="row">
                <div class="col-md-6" style="text-align: center">
                    <div class="portlet light portlet-fit bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-microphone font-green"></i>
                                <span class="caption-subject bold font-green uppercase"> CV GÖRÜNÜMÜ</span>
                                <span class="caption-helper">- {{$kisi_id->adisoyadi}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="profile-sidebar">
                        <div class="portlet light profile-sidebar-portlet ">
                            <div class="profile-userpic">
                                <img src="/frontend/images/insan-kaynaklari/{{$kisi_id->resim}}" class="img-responsive" alt=""> </div>
                            <div class="profile-usertitle">
                                <div class="profile-usertitle-name"> {{$kisi_id->adisoyadi}}</div>
                                {{--<div class="profile-usertitle-job"> Developer </div>--}}
                            </div>
                            <div class="profile-userbuttons">
                                <button type="button" class="btn btn-circle green btn-sm">{{$kisi_id->gsm}}</button>
                                <button type="button" class="btn btn-circle red btn-sm">{{$kisi_id->email}}</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="portlet light portlet-fit bordered">
                        <div class="portlet-body">
                            <div class="timeline">
                                <!-- TIMELINE ITEM -->
                                <div class="timeline-item">
                                    <div class="timeline-badge">
                                        <img class="timeline-badge-userpic" src="/frontend/images/insan-kaynaklari/{{$kisi_id->resim}}"> </div>
                                    <div class="timeline-body">
                                        <div class="timeline-body-arrow"> </div>
                                        <div class="timeline-body-head">
                                            <div class="timeline-body-head-caption">
                                                <h4>KİŞİSEL BİLGİLERİ</h4>
                                                <div class="profile-usermenu">
                                                    <ul class="nav">
                                                        <li class="active">
                                                            <a href="">
                                                                <i class="icon-user"></i> Adı Soyadı :
                                                                <span class="timeline-body-time c-l">{{$kisi_id->adisoyadi}}</span>
                                                            </a>
                                                        </li>
                                                        <li class="active">
                                                            <a href="">
                                                                <i class="icon-check"></i> Doğum Tarihi:
                                                                <span class="timeline-body-time c-l">{{$kisi_id->dogumtarihi}}</span>
                                                            </a>
                                                        </li>
                                                        <li class="active">
                                                            <a href="">
                                                                <i class="icon-check"></i> Cinsiyeti:
                                                                <span class="timeline-body-time c-l">{{$kisi_id->cinsiyet}}</span>
                                                            </a>
                                                        </li>
                                                        <li class="active">
                                                            <a href="">
                                                                <i class="icon-check"></i> Gsm:
                                                                <span class="timeline-body-time c-l">{{$kisi_id->gsm}}</span>
                                                            </a>
                                                        </li>
                                                        <li class="active">
                                                            <a href="">
                                                                <i class="icon-check"></i> Telefon:
                                                                <span class="timeline-body-time c-l">{{$kisi_id->telefon}}</span>
                                                            </a>
                                                        </li>
                                                        <li class="active">
                                                            <a href="">
                                                                <i class="icon-check"></i> E-mail:
                                                                <span class="timeline-body-time c-l">{{$kisi_id->email}}</span>
                                                            </a>
                                                        </li>
                                                        <li class="active">
                                                            <a href="">
                                                                <i class="icon-check"></i> Adres:
                                                                <span class="timeline-body-time c-l">{{$kisi_id->adres}}</span>
                                                            </a>
                                                        </li>
                                                        <li class="active">
                                                            <a href="">
                                                                <i class="icon-check"></i> Askerlik Durumu:
                                                                <span class="timeline-body-time c-l">{{$kisi_id->ib_askerlik_durumu}}</span>
                                                            </a>
                                                        </li>
                                                        <li class="active">
                                                            <a href="">
                                                                <i class="icon-check"></i> Son Tecil Tarihi :
                                                                <span class="timeline-body-time c-l">{{$kisi_id->ib_son_tecil_tarihi}}</span>
                                                            </a>
                                                        </li>
                                                        <li class="active">
                                                            <a href="">
                                                                <i class="icon-check"></i> Ehliyet:
                                                                <span class="timeline-body-time c-l">{{$kisi_id->ib_ehliyet}}</span>
                                                            </a>
                                                        </li>
                                                        <li class="active">
                                                            <a href="">
                                                                <i class="icon-check"></i> Ehliyet Türü:
                                                                <span class="timeline-body-time c-l">{{$kisi_id->ib_ehliyet_turu}}</span>
                                                            </a>
                                                        </li>
                                                        <h4>EĞİTİM BİLGİLERİ</h4>
                                                        <li class="active">
                                                            <a href="">
                                                                <i class="icon-check"></i> Mezuniyet Durumu:
                                                                <span class="timeline-body-time c-l">{{$kisi_id->ib_mezuniyet}}</span>
                                                            </a>
                                                        </li>
                                                        <li class="active">
                                                            <a href="">
                                                                <i class="icon-check"></i> Okul:
                                                                <span class="timeline-body-time c-l">{{$kisi_id->ib_okul}}</span>
                                                            </a>
                                                        </li>
                                                        <li class="active">
                                                            <a href="">
                                                                <i class="icon-check"></i> Bölüm:
                                                                <span class="timeline-body-time c-l">{{$kisi_id->ib_bolum}}</span>
                                                            </a>
                                                        </li>
                                                        <h4>İŞ DENEYİMLERİ</h4>
                                                        <li class="active">
                                                            <a href="">
                                                                <i class="icon-check"></i> Deneyim-1:
                                                                <span class="timeline-body-time c-l">{{$kisi_id->ib_deneyimbir}}</span>
                                                            </a>
                                                        </li>
                                                        <li class="active">
                                                            <a href="">
                                                                <i class="icon-check"></i> Deneyim-2:
                                                                <span class="timeline-body-time c-l">{{$kisi_id->ib_deneyimiki}}</span>
                                                            </a>
                                                        </li>
                                                        <li class="active">
                                                            <a href="">
                                                                <i class="icon-check"></i> Deneyim-3:
                                                                <span class="timeline-body-time c-l">{{$kisi_id->ib_deneyimuc}}</span>
                                                            </a>
                                                        </li>
                                                        <h4>NOTLAR</h4>
                                                        <li class="active">
                                                            <a href="">
                                                                <i class="icon-check"></i> Talep Ettiğiniz Maaş:
                                                                <span class="timeline-body-time c-l">{{$kisi_id->ib_maas}}</span>
                                                            </a>
                                                        </li>
                                                        <li class="active">
                                                            <a href="">
                                                                <i class="icon-check"></i> Eklemek İstedikleriniz:
                                                                <span class="timeline-body-time c-l">{{$kisi_id->ib_notlar}}</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- END TIMELINE ITEM -->
                                <!-- END TIMELINE ITEM -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>
@endsection
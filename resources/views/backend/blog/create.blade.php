
<form id="frm-galeri" class="form-horizontal" method="post" action="/admin/blog" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="form-body">
        <div class="alert alert-danger display-hide">
            <button class="close" data-close="alert"></button> Bazı form hatalarınız var. Lütfen kontrol ediniz. </div>
        <div class="alert alert-success display-hide">
            <button class="close" data-close="alert"></button> Form doğrulamanız başarılı!
        </div>
        <div class="form-group last">
            <div class="imageupload">
                <label class="control-label col-md-3">Resim Seçin <span class="required"> * </span></label>
                <div class="file-tab col-md-9">
                    <label class="btn btn-primary btn-file">
                        <span>Dosya Seç</span>
                        <!-- The file is stored here. -->
                        <input type="file" name="resim" id="resim" required>
                    </label>
                    <button type="button" class="btn btn-danger">Sil</button>
                </div>
            </div>
        </div>

        <div class="form-group  margin-top-20">
            <label class="control-label col-md-3">Başlık
                <span class="required"> * </span>
            </label>
            <div class="col-md-9">
                <div class="input-icon right">
                    <i class="fa"></i>
                    <input  type="text" class="form-control" name="baslik" id="baslik">
                </div>
            </div>
        </div>
        <div class="form-group  margin-top-20">
            <label class="control-label col-md-3">Açıklaması
                <span class="required"> * </span>
            </label>
            <div class="col-md-9">
                <div class="input-icon right">
                    <i class="fa"></i>
                    <textarea style="min-height: 124px;" type="text" class="form-control" name="aciklamasi" id="aciklamasi" rows="10" cols="10"></textarea>
                </div>
            </div>
        </div>

    <input type="hidden" name="gmid" id="gmid" value="0"/>

    <div class="modal-footer">
        <button type="submit" class="btn green btn-circle add">
            <span class='glyphicon glyphicon-check'></span> Kaydet
        </button>
        <button type="button" class="btn red btn-circle" data-dismiss="modal">
            <span class='glyphicon glyphicon-remove'></span> Vazgeç
        </button>
    </div>
    </div>
</form>

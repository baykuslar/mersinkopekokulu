
<form id="frm-galeri" class="form-horizontal" method="post" action="/admin/kopek-okulu" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="form-body">
        <div class="alert alert-danger display-hide">
            <button class="close" data-close="alert"></button> Bazı form hatalarınız var. Lütfen kontrol ediniz. </div>
        <div class="alert alert-success display-hide">
            <button class="close" data-close="alert"></button> Form doğrulamanız başarılı!
        </div>
          <div class="form-group last">
              <label class="control-label col-md-4">Resim</label>
              <div class="col-md-8">
                  <div class="fileinput fileinput-new" data-provides="fileinput">
                      <div class="fileinput-new thumbnail" style="width: 120px; height: 90px;">
                          <img src="" alt="" id="resim" />
                      </div>
                      <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 120px; max-height: 90px;"> </div>
                      <div>
                          <span class="btn green btn-file btn-xs">
                              <span class="fileinput-new"> Resim Seçiniz </span>
                              <span class="fileinput-exists"> Değiştir </span>
                              <input type="file" name="resim" id="resim">
                          </span>
                          <a href="javascript:;" class="btn red fileinput-exists btn-xs" data-dismiss="fileinput"> Sil </a>
                      </div>
                  </div>
              </div>
          </div>
        <div class="form-group  margin-top-20">
            <label class="control-label col-md-4">Başlık
                <span class="required"> * </span>
            </label>
            <div class="col-md-8">
                <div class="input-icon right">
                    <i class="fa"></i>
                    <input type="text" class="form-control" name="baslik" id="baslik">
                </div>
            </div>
        </div>
        <div class="form-group  margin-top-20">
            <label class="control-label col-md-4">Açıklama
                <span class="required"> * </span>
            </label>
            <div class="col-md-8">
                <div class="input-icon right">
                    <i class="fa"></i>
                    <textarea style="min-height: 124px;" type="text" class="form-control" name="aciklamasi" id="aciklamasi"></textarea>
                </div>
            </div>
        </div>

    <input type="hidden" name="gmid" id="gmid" value="0"/>

    <div class="modal-footer">
        <button type="submit" class="btn green btn-circle add">
            <span class='glyphicon glyphicon-check'></span> Kaydet
        </button>
        <button type="button" class="btn red btn-circle" data-dismiss="modal">
            <span class='glyphicon glyphicon-remove'></span> Vazgeç
        </button>
    </div>
    </div>
</form>

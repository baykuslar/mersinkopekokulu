
<form id="frm-galeri" class="form-horizontal" method="post" action="/admin/referanslar" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="form-body">
        <div class="form-group last">
            <label class="control-label col-md-4"></label>
            <div class="fileinput fileinput-new col-md-5" data-provides="fileinput">
                <div class="fileinput-new thumbnail" style="border: none">
                    <img src="" alt="" id="resim">
                </div>
            </div>
            <div class="imageupload">
                <label class="control-label col-md-4">Resim <span class="required"> * </span></label>
                <div class="file-tab fileinput fileinput-new col-md-8">
                    <label class="btn btn-primary btn-file">
                        <span></span>
                        <input type="file" name="resim" id="resim">
                    </label>
                </div>
            </div>
        </div>

        <div class="form-group  margin-top-20">
            <label class="control-label col-md-4">Başlık (tr)
                <span class=""> * </span>
            </label>
            <div class="col-md-8">
                <div class="input-icon right">
                    <i class="fa"></i>
                    <input type="text" class="form-control" name="baslik_tr" id="baslik_tr" maxlength="60">
                </div>
            </div>
        </div>
        <div class="form-group  margin-top-20">
            <label class="control-label col-md-4">Başlık (en)
                <span class=""> * </span>
            </label>
            <div class="col-md-8">
                <div class="input-icon right">
                    <i class="fa"></i>
                    <input type="text" class="form-control" name="baslik_en" id="baslik_en" maxlength="60">
                </div>
            </div>
        </div>

        <div class="form-group  margin-top-20">
            <label class="control-label col-md-4">Açıklama (tr)
                <span class=""> * </span>
            </label>
            <div class="col-md-8">
                <div class="input-icon right">
                    <i class="fa"></i>
                    <textarea type="text" class="form-control" name="aciklama_tr" id="aciklama_tr" cols="5" rows="5"></textarea>
                </div>
            </div>
        </div>
        <div class="form-group  margin-top-20">
            <label class="control-label col-md-4">Açıklama (en)
                <span class=""> * </span>
            </label>
            <div class="col-md-8">
                <div class="input-icon right">
                    <i class="fa"></i>
                    <textarea type="text" class="form-control" name="aciklama_en" id="aciklama_en" cols="5" rows="5"></textarea>
                </div>
            </div>
        </div>

        <input type="hidden" name="gmid" id="gmid" value="0"/>

        <div class="modal-footer">
            <button type="submit" class="btn green btn-circle add">
                <span class='glyphicon glyphicon-check'></span> Kaydet
            </button>
            <button onclick="location.reload();" type="button" class="btn red btn-circle" data-dismiss="modal">
                <span class='glyphicon glyphicon-remove'></span> Vazgeç
            </button>
        </div>
    </div>
</form>

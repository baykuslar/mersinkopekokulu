@extends('backend.master')
@section('title') Referanslar @endsection
@section('govde')
@section('page_level_scripts_css')
    <link href="/backend/assets/global/plugins/cubeportfolio/css/cubeportfolio.css" rel="stylesheet" type="text/css" />
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="/backend/assets/pages/css/portfolio.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('page_css')
    <link rel="stylesheet" href="/backend/css/custom.css">
@endsection

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li><a href="admin/index">Anasayfa</a><i class="fa fa-circle"></i>
                </li>
                <li><span>Referanslar</span></li>
            </ul>
        </div>
        <div class="m-heading-1 border-green m-bordered">
            <button id="sample_editable_1_new" class="btn sbold green btnCreate b-r"> Yeni Kayıt Ekle
                <i class="fa fa-plus"></i>
            </button>
        </div>

        <div class="portfolio-content portfolio-1">
            <div id="js-grid-juicy-projects" class="cbp">
                <?php $no=1 ?>
                @foreach($referanskayitlari as $kayit)
                    <div class="cbp-item item{{$kayit->id}}">
                        <div class="cbp-caption">
                            <div class="cbp-caption-defaultWrap">
                                @if($kayit->resim == null)
                                    <img src="/uploads/referanslarimiz/default.jpg" alt="" class="img-responsive">
                                @else
                                    <img src="/uploads/referanslarimiz/{{$kayit->resim}}" alt="" class="img-responsive">
                                @endif
                            </div>
                            <div class="cbp-caption-activeWrap">
                                <div class="cbp-l-caption-alignCenter">
                                    <div class="cbp-l-caption-body">
                                        <a class="cbp-singlePage" rel="nofollow">
                                            <button type="button" class="btn btn-circle green btn-sm guncelle-btn "
                                                    data-toggle="tooltip"
                                                    data-placement="left"
                                                    title="Güncelle"
                                                    data-id="{{$kayit->id}}"
                                                    data-baslik_tr="{{$kayit->baslik_tr}}"
                                                    data-baslik_en="{{$kayit->baslik_en}}"
                                                    data-aciklama_tr="{{$kayit->aciklama_tr}}"
                                                    data-aciklama_en="{{$kayit->aciklama_en}}"
                                                    data-resim="/uploads/referanslarimiz/{{$kayit->resim}}">
                                                <i class="fa fa-refresh" aria-hidden="true"></i> <span>GÜNCELLE</span>
                                            </button>
                                        </a>
                                        <a class="cbp-lightbox">
                                            <button title="Sil" class="btn btn-circle red btn-sm delete-modal"
                                                    data-toggle="tooltip" data-placement="right" title="Sil"
                                                    data-id="{{$kayit->id}}" data-title="{{$kayit->aciklamasi}}"><i class="fa fa-trash"></i> SİL
                                            </button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cbp-l-grid-projects-desc uppercase text-center">
                            <b>{{empty($kayit->baslik_tr) ? $kayit->id : $kayit->baslik_tr}}</b>
                        </div>
                        <div class="cbp-l-grid-projects-desc uppercase text-center">
                            {{empty($kayit->aciklama_tr) ? $kayit->id : $kayit->aciklama_tr}}
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="clearfix"> </div>

    </div>
    <!-- END CONTENT BODY -->
</div>

<!-- Delete Item Modal -->
<div class="sweet-alert showSweetAlert" id="delete-modal" style="border-radius: 24px !important;">
    <div class="modal-header">
        <div class="waricon">
            <i style="color:red;font-size: 60px;line-height: 60px;" class="fa fa-exclamation-circle" aria-hidden="true"></i>
        </div>
        <span class="hidden id"></span>
        <h2 style="color:orangered" class="title"> </h2>
        <h3>Bilgisi silinecektir?</h3>
        <p>Bilgiyi tamamen silmek İstediğinizden emin misiniz!</p>
    </div>
    <div class="modal-footer" style="text-align:center !important">
        <button style="margin: 0px 15px;padding: 7px 20px;float: left" type="button" class="btn btn-circle red btn-sm  actionBtn" data-dismiss="modal">
            <i class="fa fa-trash fa-lg" aria-hidden="true"></i> Delete
        </button>
        <button style="margin: 0px 15px;padding: 7px 20px;" type="button" class="btn btn-circle green btn-sm"  data-dismiss="modal"
                onclick="$.Notification.autoHideNotify('error', 'top right', 'İşlem Başarısız','Kayıt Silinmedi!...')">
            <i class="fa fa-times fa-lg" aria-hidden="true"></i> Vazgeç
        </button>
    </div>
</div>

<!-- //Edit Modal -->
<div class="modal fade" id="myModal" role="dialog" aria-labelledby="edit" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button onclick="location.reload();" type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                </button>
                <h4 class="modal-title custom_align" id="Heading"> Kayıt Ekle/Güncelle</h4>
            </div>
            <div class="modal-body">
                @include('backend.referanslar.create')
            </div>
        </div>
    </div>
</div>

@endsection
@section('page_level_scripts_js')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="/backend/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js" type="text/javascript"></script>
@endsection
@section('page_level_scripts_js_end')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="/backend/assets/pages/scripts/portfolio-1.min.js" type="text/javascript"></script>
@endsection
@section('dahil_edilecek_js')
    <script type="text/javascript">
        $(document).ready(function() {
            $(":input").inputmask();

            $('.btnCreate').on('click', function () {
                $('#myModal').modal('show');
                forumBosalt();
            });

            $(document).on('click', '.guncelle-btn', function () {
                $('#myModal').modal('show');
                forumDoldur($(this));
            });

            //delete function
            $(document).on('click', '.delete-modal', function() {
                $('.actionBtn').addClass('delete');
                $('.id').text($(this).data('id'));
                $('.baslik_tr').text($(this).data('baslik_tr'));
                $('.deleteContent').show();
                $('.title').html($(this).data('title'));
                $('#delete-modal').modal('show');
            });
            $('.modal-footer').on('click', '.delete', function() {
                $.ajax({
                    type: 'post',
                    url: '/admin/deleteReferans/'+$('.id').text(),
                    data: {'_token': $('input[name=_token]').val()},
                    success : function(cevap){
                        $('.item' + $('.id').text()).remove();
                        console.log(cevap);
                        window.location.reload(true);
                    }
                });
            });

        });

        function forumDoldur(btnn) {
            $('#gmid').val(btnn.attr('data-id'));
            $('#resim').attr('src',btnn.attr('data-resim'));
            $('#baslik_tr').val(btnn.attr('data-baslik_tr'));
            $('#baslik_en').val(btnn.attr('data-baslik_en'));
            $('#aciklama_tr').val(btnn.attr('data-aciklama_tr'));
            $('#aciklama_en').val(btnn.attr('data-aciklama_en'));
        }

        function forumBosalt() {
            $('#gmid').val(0);
            $('#resim').attr('src','');
            $('#baslik_tr').val('');
            $('#baslik_en').val('');
            $('#aciklama_tr').val('');
            $('#aciklama_en').val('');
        }

    </script>
@endsection



@extends('backend.master')
@section('title') İşlem Geçmişi @endsection
@section('govde')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="index.html">Anasayfa</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Ayarlar</span>
                    </li>
                </ul>
            </div>
            <br>
            <!-- END PAGE BAR -->
            <!-- END PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-body">
                            <div class="table-toolbar">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="btn-group">
                                                <a href="#" class="btn red btn-outline" target="_blank">Admin Paneli içerisinde yapılan işlemler listesi!</a>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="btn-group pull-right">
                                            <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Araçlar
                                                <i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul class="dropdown-menu pull-right">
                                                <li>
                                                    <a href="javascript:;">
                                                        <i class="fa fa-print"></i> Print </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;">
                                                        <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;">
                                                        <i class="fa fa-file-excel-o"></i> Export to Excel </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                <div class="panel-body">
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th class="text-center">#ID</th>
                            @if(Auth::user()->kullanici_turu==5)
                            <th class="text-center">
                                <button type="button" name="bulk_delete" id="bulk_delete" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-remove"></i> ÇOKLU SİL</button>
                            </th>
                            <th class="text-center"> İşlemler</th>
                            @endif
                            <th class="text-center">Yapılan İşlem</th>
                            <th class="text-center">İşlemi Yapan Kişi</th>
                            <th class="text-center">İşlem Yapılan Sayfa</th>
                            <th class="text-center">İşlem Alanı</th>
                            <th class="text-center">İşlem Zamanı</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($islemGecmisikayitlar as $islem)
                            <tr class="item{{$islem->id}}">
                                <td class="text-center">
                                    {{$islem->id}}
                                </td>
                                @if(Auth::user()->kullanici_turu==5)
                                <td class="text-center">
                                    <label class="ascb">
                                        <input type="checkbox" name="student_checkbox[]" class="student_checkbox" value="{{$islem->id}}" />
                                        <span class="checkmark"></span>
                                    </label>
                                </td>
                                <td class="text-center">
                                    <button title="Sil" class="btn btn-xs red delete-modal box-s"
                                            data-toggle="tooltip" data-placement="right" title="Sil"
                                            data-id="{{$islem->id}}" data-title="{{$islem->name}}"><i class="fa fa-trash"></i> SİL
                                    </button>
                                </td>
                                @endif
                                <td class="text-center">{{$islem->yapilan_islem}}</td>
                                <td class="text-center">{{$islem->adi_soyadi}}</td>
                                <td class="text-center">{{$islem->islem_yapilan_sayfa}}</td>
                                <td class="text-center">{{$islem->islem_alani}}</td>
                                <td class="text-center">{{ \Carbon\Carbon::parse($islem->created_at)->format('d/m/Y - h:i:s')}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
    </div> <!-- End Row -->
    </div> <!-- End Row -->
    </div> <!-- End Row -->
    </div> <!-- End Row -->

    </div>
</div>

    <!-- Delete Item Modal -->
    <div class="sweet-alert showSweetAlert" id="delete-modal" style="border-radius: 24px !important;">
        <div class="modal-header">
            <div class="waricon">
                <i style="color:red;font-size: 60px;line-height: 60px;" class="fa fa-exclamation-circle" aria-hidden="true"></i>
            </div>
            <span class="hidden id"></span>
            <h2 style="color:orangered" class="title"> </h2>
            <h3>Bilgisi silinecektir?</h3>
            <p>Bilgiyi tamamen silmek İstediğinizden emin misiniz!</p>
        </div>
        <div class="modal-footer" style="text-align:center !important">
            <button style="margin: 0px 15px;padding: 7px 20px;float: left" type="button" class="btn btn-circle red btn-sm  actionBtn" data-dismiss="modal">
                <i class="fa fa-trash fa-lg" aria-hidden="true"></i> Delete
            </button>
            <button style="margin: 0px 15px;padding: 7px 20px;" type="button" class="btn btn-circle green btn-sm"  data-dismiss="modal"
                    onclick="$.Notification.autoHideNotify('error', 'top right', 'İşlem Başarısız','Kayıt Silinmedi!...')">
                <i class="fa fa-times fa-lg" aria-hidden="true"></i> Vazgeç
            </button>
        </div>
    </div>
@endsection

@section('dahil_edilecek_js')
<!------ /Güncelle End --------------->
<script type="text/javascript">
    $(document).ready(function() {

        $(document).on('click', '#bulk_delete', function(){
            var id = [];
                $('.student_checkbox:checked').each(function(){
                    id.push($(this).val());
                });
                if(id.length > 0)
                {
                    $.ajax({
                        url:"{{ route('multipleDelete')}}",
                        method:"get",
                        data:{id:id},
                        success:function(cevap)
                        {
                            $('.student_checkbox:checked').each(function(){
                                $(this).parents("tr").remove();
                                location.reload();
                            });
                            console.log(cevap);
                            if( cevap.giris !== false ){
                                toastr['success']('Kayıtlı Bilgi Silindi!', 'İşlem Başarılı!');
                            }else{
                                toastr['error']('Kayıtlı Bilgi Silinmedi!', 'İşlem Başarısız!');
                            }
                        }
                    });
                }else{
                        Swal(
                            'UYARI!',
                            'Lütfen en az bir onay kutusu seçin!',
                            'error'
                        )
                    }
            });
        });
</script>

    <script type="text/javascript">
        $(document).ready(function() {

            $(document).on('click', '.delete-modal', function() {
                $('.actionBtn').addClass('delete');
                $('.id').text($(this).data('id'));
                $('.kurum_id').text($(this).data('kurum_id'));
                $('.deleteContent').show();
                $('.title').html($(this).data('title'));
                $('#delete-modal').modal('show');
            });
            $('.modal-footer').on('click', '.delete', function() {
                $.ajax({
                    url: '/admin/deleteislemGecmisi',
                    type : 'POST',
                    data: {
                        '_token': $('input[name=_token]').val(),
                        'id': $('.id').text()
                    },
                    success : function(cevap){
                        $('.item' + $('.id').text()).remove();
                        console.log(cevap);
                        if( cevap.giris !== false ){
                            toastr['success']('Kayıtlı Bilgi Silindi!', 'İşlem Başarılı!');
                        }else{
                            toastr['error']('Kayıtlı Bilgi Silinmedi!', 'İşlem Başarısız!');
                        }
                        location.reload();
                    }
                });

            });

        });


        $('table').dataTable({
                "aaSorting": [[ 0, "desc" ]] // Sort by first column descending
        });
        $(document).ready(function() {
            $('#datatable').dataTable();
            $('#datatable-keytable').DataTable( { keys: true } );
            $('#datatable-responsive').DataTable();
            $('#datatable-scroller').DataTable( { ajax: "public/assets/plugins/datatables/json/scroller-demo.json",
                deferRender: true, scrollY: 380, scrollCollapse: true, scroller: true } );
            var table = $('#datatable-fixed-header').DataTable( { fixedHeader: true } );

        } );
        TableManageButtons.init();

        function validate(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode( key );
            var regex = /[0-9]|\./;
            if( !regex.test(key) ) {
                theEvent.returnValue = false;
                if(theEvent.preventDefault) theEvent.preventDefault();
            }
        }
    </script>
@endsection

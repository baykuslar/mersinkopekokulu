<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <title>Dosya Yükleme Hatası!</title>
    <meta name="description" content="A beautiful replacement for JavaScript's 'alert'">

    <link rel="stylesheet" href="/sweetalert/dist/sweetalert.css">

    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700,700i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Inconsolata" rel="stylesheet">
<style>
    swal {
        background-color: #FEFAE3;
        padding: 17px;
        border: 1px solid #F0E1A1;
        display: block;
        margin: 22px;
        text-align: center;
        color: #61534e;
    }
</style>
</head>
<body>

</body>
<script src="/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
/*    swal({
        title: "Dosya Yükleme Hatası!",
        text: "Lütfen bir PDF dosyasını yükleyiniz!",
        icon: "error",
        button: "Geri Dön!",
    })
        .then((value) => {
        window.location = "/randevu-taleplerim";
    });*/

    swal({ title: "Dosya Yükleme Hatası!",
        text: "Lütfen bir PDF dosyasını yükleyiniz!",
        confirmButtonText:"Geri Dön",
        type: "error" },
        function() {
        window.history.back();
    });
</script>
</html>
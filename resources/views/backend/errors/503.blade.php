<!DOCTYPE html>
<html>
    <head>
        <title>@lang('metronic::hatalar.BeRightBack').</title>

        <link href="/vendor/metronic/css/fonts/fonts.google.com.lato.css" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #B0BEC5;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 72px;
                margin-bottom: 40px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <img border="0" alt="" src="/vendor/metronic/img/logo_{{\App::getLocale()}}.png" width=235>
                <h1>@lang('genel.sistemAdi') </h1>
                <h2>404</h2>
                <h2 class="title">@lang('metronic::hatalar.BeRightBack').</h2>
            </div>
        </div>
    </body>
</html>

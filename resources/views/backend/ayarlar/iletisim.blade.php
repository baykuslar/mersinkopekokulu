<div class="portlet light bordered" id="form_wizard_1">
    <div class="portlet-body form">
        <form class="form-horizontal" id="submit_form" action="/admin/ayarlar/{{$ayarlar->id}}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('PUT') }}
            <div class="form-wizard">
                <div class="form-body">
                    <ul class="nav nav-pills nav-justified steps">
                        <li>
                            <a href="#tab1" data-toggle="tab" class="step">
                                <span class="number"> 1 </span>
                                <span class="desc">
                                <i class="fa fa-check"></i> Çalışma Saatleri </span>
                            </a>
                        </li>
                        <li>
                            <a href="#tab2" data-toggle="tab" class="step">
                                <span class="number"> 2 </span>
                                <span class="desc">
                                    <i class="fa fa-check"></i> Telefon / Email</span>
                            </a>
                        </li>
                        <li>
                            <a href="#tab3" data-toggle="tab" class="step active">
                                <span class="number"> 3 </span>
                                <span class="desc">
                                    <i class="fa fa-check"></i> Adres Bilgileri </span>
                            </a>
                        </li>
                    </ul>
                    <div id="bar" class="progress progress-striped" role="progressbar">
                        <div class="progress-bar progress-bar-success"> </div>
                    </div>
                    <div class="tab-content">
                        <div class="alert alert-danger display-none">
                            <button class="close" data-dismiss="alert"></button> Bazı form hatalarınız var. Lütfen kontrol edin. </div>
                        <div class="alert alert-success display-none">
                            <button class="close" data-dismiss="alert"></button> Form doğrulamanız başarılı! </div>
                        <div class="tab-pane active" id="tab1">
                            <div class="form-group">
                                <label class="control-label col-md-3">Çalışma Saatleri
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-9">
                                    <div class="input-group m-b">
                                <span class="input-group-btn">
                                    <button class="btn default" type="button">
                                        <i class="fa fa-clock-o"> </i> Başlangıç
                                    </button>
                                </span>
                                        <input name="calisma_saatleri_baslangic" value="{{$ayarlar->calisma_saatleri_baslangic}}" type="text" class="form-control timepicker timepicker-24">
                                    </div>
                                    <div class="input-group m-b" style="margin-top: 5px">
                            <span class="input-group-btn">
                                <button class="btn default" type="button">
                                    <i class="fa fa-clock-o"> </i> Bitiş
                                </button>
                            </span>
                                        <input name="calisma_saatleri_bitis" value="{{$ayarlar->calisma_saatleri_bitis}}"  type="text" class="form-control timepicker timepicker-24">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3"> Cumartesi</label>
                                <div class="col-md-9">
                                    <select name="cumartesi" id="cumartesi" class="form-control">
                                        <option value="{{$ayarlar->cumartesi}}">
                                            @if($ayarlar->cumartesi=='Açık')
                                                {{$ayarlar->cumartesi_baslangic}} - {{$ayarlar->cumartesi_bitis}}
                                                @else
                                                {{$ayarlar->cumartesi}}
                                            @endif
                                        </option>
                                        <option name="cumartesi" value="Kapalı">Kapalı</option>
                                        <option name="cumartesi" value="Açık">Açık </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group saat_sec hidden">
                                <label class="control-label col-md-3">Cumartesi Çalışma Saatleri
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-9">
                                    <div class="input-group m-b">
                            <span class="input-group-btn">
                                <button class="btn default" type="button">
                                    <i class="fa fa-clock-o"> </i> Başlangıç
                                </button>
                            </span>
                                        <input name="cumartesi_baslangic" value="{{$ayarlar->cumartesi_baslangic}}" type="text" class="form-control timepicker timepicker-24">
                                    </div>
                                    <div class="input-group m-b" style="margin-top: 5px">
                                <span class="input-group-btn">
                                    <button class="btn default" type="button">
                                        <i class="fa fa-clock-o"> </i> Bitiş
                                    </button>
                                </span>
                                        <input name="cumartesi_bitis" value="{{$ayarlar->cumartesi_bitis}}"  type="text" class="form-control timepicker timepicker-24">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3"> Pazar</label>
                                <div class="col-md-9">
                                    <select name="pazar" id="pazar" class="form-control">
                                        <option value="{{$ayarlar->pazar}}">
                                            @if($ayarlar->pazar=='Açık')
                                                {{$ayarlar->pazar_baslangic}} - {{$ayarlar->pazar_baslangic}}
                                            @else
                                                {{$ayarlar->pazar}}
                                            @endif
                                        </option>
                                        <option name="pazar" value="Kapalı">Kapalı</option>
                                        <option name="pazar" value="Açık">Açık</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group saat_sec_pazar hidden">
                                <label class="control-label col-md-3">Pazar Çalışma Saatleri
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-9">
                                    <div class="input-group m-b">
                            <span class="input-group-btn">
                                <button class="btn default" type="button">
                                    <i class="fa fa-clock-o"> </i> Başlangıç
                                </button>
                            </span>
                                        <input name="pazar_baslangic" value="{{$ayarlar->pazar_baslangic}}" type="text" class="form-control timepicker timepicker-24">
                                    </div>
                                    <div class="input-group m-b" style="margin-top: 5px">
                            <span class="input-group-btn">
                                <button class="btn default" type="button">
                                    <i class="fa fa-clock-o"> </i> Bitiş
                                </button>
                            </span>
                                        <input name="pazar_bitis" value="{{$ayarlar->pazar_bitis}}" type="text" class="form-control timepicker timepicker-24">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab2">
                            <div class="form-group">
                                <label class="control-label col-md-3">Email-1
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="email" value="{{$ayarlar->email}}"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Email-2
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="email_2" value="{{$ayarlar->email_2}}" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Telefon Numarası
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="telefon" data-inputmask="'mask': '9 (999) 999 99 99'" value="{{$ayarlar->telefon}}" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Telefon Numarası
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="telefon_2" data-inputmask="'mask': '9 (999) 999 99 99'" value="{{$ayarlar->telefon_2}}" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Fax Numarası
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="faks" data-inputmask="'mask': '9 (999) 999 99 99'" value="{{$ayarlar->faks}}"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Cep Numarası
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="gsm" data-inputmask="'mask': '9 (999) 999 99 99'" value="{{$ayarlar->gsm}}"/>
                                </div>
                            </div>
                        </div>
                    <div class="tab-pane" id="tab3">
                        <div class="form-group">
                            <label class="control-label col-md-3">Adres
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="adres" id="adres"  value="{{$ayarlar->adres}}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">İl Seçiniz</label>
                            <div class="col-md-9">
                                {{ Form::select('sehir', ['' => 'Seçiniz'] +$sehir,'',array('class'=>'form-control select2', 'id'=>'sehir'))}}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">İlçe Seçiniz</label>
                            <div class="col-md-9">
                                <select name="ilce" id="ilce" class="form-control select2"></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Semt Seçiniz</label>
                            <div class="col-md-9">
                                <select name="mahalle" id="mahalle" class="form-control select2"></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Harita (Konum)</label>
                            <div class="col-md-9">
                                <textarea type="text" class="form-control" name="harita" id="harita" rows="7" placeholder="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3194.9574239648186!2d34.55867675015669!3d36.79557119481196!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1527f4da1d05f39b%3A0xce69c2748cc74af6!2sAfetevler+Mahallesi%2C+3156.+Sk.+No%3A15%2C+33110+Yeni%C5%9Fehir%2FMersin!5e0!3m2!1str!2str!4v1532348264846">{{$ayarlar->harita}}</textarea>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-9">
                        <button type="button" class="btn red btn-outline" data-dismiss="modal">
                            <span class='glyphicon glyphicon-remove'></span> Vazgeç
                        </button>
                        <a href="javascript:;" class="btn default button-previous">
                            <i class="fa fa-angle-left"></i> Geri </a>
                        <a href="javascript:;" class="btn btn-outline green button-next"> İleri
                            <i class="fa fa-angle-right"></i>
                        </a>
                        <button type="submit" class="btn blue button-submit"><i class="fa fa-check"></i> Gönder</button>
                    </div>
                </div>
            </div>
    </div>
    </form>
    </div>
</div>

<div class="row">
    <div class="col-md-12 cl-sm-6 col-xs-12">
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-info-circle"></i>
            <span>Sosyal Medya Adreslerinizi Güncelleyiniz!...</span>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" action="/admin/ayarlar/{{$ayarlar->id}}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="form-group last">
                    <label class="control-label col-md-3">Firma Logosu</label>
                    <div class="col-md-9">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: 200px; height: 65px;">
                                <img src="/uploads/logo/{{$ayarlar->resim}}" alt="" id="resim"/>
                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 65px;"> </div>
                            <div>
                                <span class="btn green btn-file  btn-xs">
                                    <span class="fileinput-new"> Firma Logosu Seçiniz</span>
                                    <span class="fileinput-exists"> Değiştir </span>
                                    <input type="file" name="resim" id="resim">
                                </span>
                                <a href="javascript:;" class="btn red fileinput-exists  btn-xs" data-dismiss="fileinput"> Sil </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">Site Başlığı
                        <span class="required"> * </span>
                    </label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" maxlength="25" id="maxlength_placement" name="site_basligi"  value="{{$ayarlar->site_basligi}}" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">Site Adresi
                        <span class="required"> * </span>
                    </label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="site_adresi" value="{{$ayarlar->site_adresi}}" placeholder="Örnek: www.demo.com"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">Site Açıklaması</label>
                    <div class="col-md-9">
                        <textarea id="maxlength_textarea" class="form-control" maxlength="225" rows="5" placeholder="Maximum 225 karakter yazabiklirsiniz." name="site_aciklamasi">{{$ayarlar->site_aciklamasi}}</textarea>
                    </div>
                </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn green btn-circle add">
                            <span class='glyphicon glyphicon-check'></span> Kaydet
                        </button>
                        <button type="button" class="btn red btn-circle" data-dismiss="modal">
                            <span class='glyphicon glyphicon-remove'></span> Vazgeç
                        </button>
                    </div>
                </form>
            </div> <!-- panel-body -->
        </div> <!-- col -->
    </div> <!-- End row -->
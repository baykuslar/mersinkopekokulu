@extends('backend.master')
@section('title') Ayarlar @endsection
@section('govde')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">

    <div class="clearfix">
        <h4 class="block">Genel Ayarlar</h4>
        <hr>
        <a class="btn blue ayarlar sosyalag-btn"
           data-id="{{$ayarlar->id}}"
           data-facebook="{{$ayarlar->facebook}}"
           data-instagram="{{$ayarlar->instagram}}"
           data-twitter="{{$ayarlar->twitter}}"
           data-youtube="{{$ayarlar->youtube}}">
            <i class="fa fa-youtube" aria-hidden="true"></i>
            <i class="fa fa-facebook" aria-hidden="true"></i>
            <i class="fa fa-instagram" aria-hidden="true"></i>
            <i class="fa fa-twitter" aria-hidden="true"></i>
            <span class="guncelle">Sosyal Medya Ayarları</span>
        </a>

        <a class="btn red ayarlar iletisim"
           data-id="{{$ayarlar->id}}"
           data-calisma_saatleri_baslangic="{{$ayarlar->calisma_saatleri_baslangic}}"
           data-calisma_saatleri_bitis="{{$ayarlar->calisma_saatleri_bitis}}"
           data-pazar="{{$ayarlar->pazar}}"
           data-cumartesi="{{$ayarlar->cumartesi}}"
           data-cumartesi_baslangic="{{$ayarlar->cumartesi_baslangic}}"
           data-cumartesi_bitis="{{$ayarlar->cumartesi_bitis}}"
           data-pazar_baslangic="{{$ayarlar->pazar_baslangic}}"
           data-pazar_bitis="{{$ayarlar->pazar_bitis}}"
           data-sehir="{{$ayarlar->sehir}}"
           data-ilce="{{$ayarlar->ilce}}"
           data-mahalle="{{$ayarlar->mahalle}}"
           data-email="{{$ayarlar->email}}"
           data-email_2="{{$ayarlar->email_2}}"
           data-telefon="{{$ayarlar->telefon}}"
           data-telefon_2="{{$ayarlar->telefon_2}}"
           data-faks="{{$ayarlar->faks}}"
           data-gsm="{{$ayarlar->gsm}}"
           data-harita="{{$ayarlar->harita}}"
           data-adres="{{$ayarlar->adres}}">
            <i class="fa fa-phone" aria-hidden="true"></i>
            <i class="fa fa-envelope" aria-hidden="true"></i>
            <i class="fa fa-clock-o" aria-hidden="true"></i>
            <span class="guncelle">İletişim Bilgileri Ayarları</span>
        </a>
        <a class="btn green ayarlar siteayarlari"
           data-id="{{$ayarlar->id}}"
           data-resim="/uploads/logo/{{$ayarlar->resim}}"
           data-site_basligi="{{$ayarlar->site_basligi}}"
           data-site_adresi="{{$ayarlar->site_adresi}}"
           data-site_aciklamasi="{{$ayarlar->site_aciklamasi}}"
           data-cumartesi="{{$ayarlar->cumartesi}}">
            <i class="fa fa-edit" aria-hidden="true"></i>
            <i class="fa fa-backward" aria-hidden="true"></i>
            <span class="guncelle">Site Ayarları</span>
        </a>
{{--        <a href="javascript:;" class="btn yellow"> Yellow
            <i class="fa fa-search"></i>
        </a>
        <a href="javascript:;" class="btn purple">
            <i class="fa fa-times"></i> Purple </a>
        <a href="javascript:;" class="btn green"> Green
            <i class="fa fa-plus"></i>
        </a>
        <a href="javascript:;" class="btn grey-cascade"> Dark
            <i class="fa fa-link"></i>
        </a>
        <a href="javascript:;" class="btn default"> Default
            <i class="fa fa-user"></i>
        </a>--}}
    </div>
    </div>
    </div>

<!-- //Edit Modal -->
<div class="modal fade" id="myModal" role="dialog" aria-labelledby="sosyalag" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                </button>
                <h4 class="modal-title custom_align"> Kayıt Ekle/Güncelle</h4>
            </div>
            <div class="modal-body">
                @include('backend.ayarlar.sosyalag')
            </div>
        </div>
    </div>
</div>
    <!-- //Edit Modal -->
    <div class="modal fade" id="iletisimModal" role="dialog" aria-labelledby="iletsim" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                    </button>
                    <h4 class="modal-title custom_align"> Kayıt Ekle/Güncelle</h4>
                </div>
                <div class="modal-body">
                    @include('backend.ayarlar.iletisim')
                </div>
            </div>
        </div>
    </div>


    <!-- //Edit Modal -->
    <div class="modal fade" id="siteayarlariModal" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                    </button>
                    <h4 class="modal-title custom_align"> Kayıt Ekle/Güncelle</h4>
                </div>
                <div class="modal-body">
                    @include('backend.ayarlar.siteayarlari')
                </div>
            </div>
        </div>
    </div>
@endsection

@section('dahil_edilecek_js')
    <script type="text/javascript">
        $(document).ready(function() {
            /*Cumartesi saat Ekle Çıkar*/
            $('#cumartesi').on('change', function() {
                if (this.value == 'Açık'){
                    $('.saat_sec').removeClass('hidden');
                }else if(this.value == 'Kapalı'){
                    $('.saat_sec').addClass('hidden');
                }else{
                    $('.saat_sec').addClass('hidden');
                }
            });
            $('#pazar').on('change', function() {
                if (this.value == 'Açık'){
                    $('.saat_sec_pazar').removeClass('hidden');
                }else if(this.value == 'Kapalı'){
                    $('.saat_sec_pazar').addClass('hidden');
                }else{
                    $('.saat_sec_pazar').addClass('hidden');
                }
            });
            $(":input").inputmask();

            $(document).on('click', '.sosyalag-btn', function () {
                $('#myModal').modal('show');
                sosyalagAyarlari($(this));
            });
            $(document).on('click', '.iletisim', function () {
                $('#iletisimModal').modal('show');
                iletisimAyarlari($(this));
            });
            $(document).on('click', '.siteayarlari', function () {
                $('#siteayarlariModal').modal('show');
                siteAyarlari($(this));
            });
            $('.create-modal').on('click', function() {
                $('#createModal [name=sehir]').val('');
                $('#createModal [name=ilce]').empty();
                $('#createModal [name=mahalle]').empty();
            });
        });

        function sosyalagAyarlari(btnn) {
            $('#gmid').val(btnn.attr('data-id'));
            $('#facebook').val(btnn.attr('data-facebook'));
            $('#instagram').val(btnn.attr('data-instagram'));
            $('#twitter').val(btnn.attr('data-twitter'));
            $('#youtube').val(btnn.attr('data-youtube'));
        }
        function iletisimAyarlari(btnn) {
            $('#gmid').val(btnn.attr('data-id'));
            $('#calisma_saatleri_baslangic').val(btnn.attr('data-calisma_saatleri_baslangic'));
            $('#calisma_saatleri_bitis').val(btnn.attr('data-calisma_saatleri_bitis'));
            $('#pazar').val(btnn.attr('data-pazar'));
            $('#cumartesi').val(btnn.attr('data-cumartesi'));
            $('#cumartesi_baslangic').val(btnn.attr('data-cumartesi_baslangic'));
            $('#cumartesi_bitis').val(btnn.attr('data-cumartesi_bitis'));
            $('#pazar_baslangic').val(btnn.attr('data-pazar_baslangic'));
            $('#pazar_bitis').val(btnn.attr('data-pazar_bitis'));
            $('#iletisimModal [name=sehir]')[0].setDeger(btnn.attr('data-sehir'),
                btnn.attr('data-ilce'),
                btnn.attr('data-mahalle')
            );
            $('#email').val(btnn.attr('data-email'));
            $('#email_2').val(btnn.attr('data-email_2'));
            $('#telefon').val(btnn.attr('data-telefon'));
            $('#telefon_2').val(btnn.attr('data-telefon_2'));
            $('#faks').val(btnn.attr('data-faks'));
            $('#gsm').val(btnn.attr('data-gsm'));
            $('#harita').val(btnn.attr('data-harita'));
            $('#adres').val(btnn.attr('data-adres'));
        }
        function siteAyarlari(btnn) {
            $('#gmid').val(btnn.attr('data-id'));
            $('#resim').attr('src',btnn.attr('data-resim'));
            $('#site_basligi').val(btnn.attr('data-site_basligi'));
            $('#site_aciklamasi').val(btnn.attr('data-site_aciklamasi'));
            $('#site_adresi').val(btnn.attr('data-site_adresi'));
        }
    </script>
@endsection



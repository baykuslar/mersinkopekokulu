
<div class="row">
    <div class="col-md-12 cl-sm-6 col-xs-12">
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-info-circle"></i>
            <span>Sosyal Medya Adreslerinizi Güncelleyiniz!...</span>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" action="/admin/ayarlar/{{$ayarlar->id}}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                    <div class="form-group">
                        <label class="control-label col-md-3">Facebook
                            <span class="required"> * </span>
                        </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="facebook" value="{{$ayarlar->facebook}}"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Twitter
                            <span class="required"> * </span>
                        </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="twitter" value="{{$ayarlar->twitter}}"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">İnstagram
                            <span class="required"> * </span>
                        </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="instagram" value="{{$ayarlar->instagram}}"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Youtube
                            <span class="required"> * </span>
                        </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="youtube" value="{{$ayarlar->youtube}}" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Pinterest
                            <span class="required"> * </span>
                        </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="pinterest" value="{{$ayarlar->pinterest}}" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Tumblr
                            <span class="required"> * </span>
                        </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="tumblr" value="{{$ayarlar->tumblr}}" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn green btn-circle add">
                            <span class='glyphicon glyphicon-check'></span> Kaydet
                        </button>
                        <button type="button" class="btn red btn-circle" data-dismiss="modal">
                            <span class='glyphicon glyphicon-remove'></span> Vazgeç
                        </button>
                    </div>
                </form>
            </div> <!-- panel-body -->
        </div> <!-- col -->
    </div> <!-- End row -->
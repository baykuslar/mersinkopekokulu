
<form id="frm-galeri" class="form-horizontal" method="post" action="/admin/sssorular" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="form-body">
        <div class="form-group  margin-top-20">
            <label class="control-label col-md-3">Başlık
                <span class=""> * </span>
            </label>
            <div class="col-md-9">
                <div class="input-icon right">
                    <i class="fa"></i>
                    <input type="text" class="form-control" name="baslik" id="baslik" maxlength="60">
                </div>
            </div>
        </div>

        <div class="form-group  margin-top-20">
            <label class="control-label col-md-3">Açıklama
                <span class=""> * </span>
            </label>
            <div class="col-md-9">
                <div class="input-icon right">
                    <i class="fa"></i>
                    <textarea type="text" class="form-control" name="aciklamasi" id="aciklamasi" rows="10" cols="5"> </textarea>
                </div>
            </div>
        </div>

        <input type="hidden" name="gmid" id="gmid" value="0"/>

        <div class="modal-footer">
            <button type="submit" class="btn green btn-circle add">
                <span class='glyphicon glyphicon-check'></span> Kaydet
            </button>
            <button type="button" class="btn red btn-circle" data-dismiss="modal">
                <span class='glyphicon glyphicon-remove'></span> Vazgeç
            </button>
        </div>
    </div>
</form>

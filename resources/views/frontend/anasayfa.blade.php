@include('frontend.moduller.slider')
@section('css_dahil_edilecek_stiller')
    <link rel="stylesheet" href="/eklentiler/popup-portfolyo/dist/css/lightbox.min.css">
@endsection
<div class="container pt--30">
    <div class="policy-block border-four-column">
        <div class="row">
            <div class="col-lg-3 col-sm-6">
                <div class="policy-block-single">
                    <div class="icon">
                        <span class="ti-stack-overflow"></span>
                    </div>
                    <div class="text">
                        <h3>@lang('genel.egitimuzman')</h3>
                        <p>@lang('genel.egitimuzmanacikla')</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="policy-block-single">
                    <div class="icon">
                        <span class="ti-home"></span>
                    </div>
                    <div class="text">
                        <h3>@lang('genel.kopekoteli')</h3>
                        <p>@lang('genel.kopekoteliacikla')</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="policy-block-single">
                    <div class="icon">
                        <span class="ti-direction-alt"></span>
                    </div>
                    <div class="text">
                        <h3>@lang('genel.pea')</h3>
                        <p>@lang('genel.peaacikla')</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="policy-block-single">
                    <div class="icon">
                        <span class="ti-world"></span>
                    </div>
                    <div class="text">
                        <h3>@lang('genel.dogalyasam')</h3>
                        <p>@lang('genel.dogalyasamacikla')</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Home -2 => Slider Block 1 -->
<div class="pt--50">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-xl-9">
                <div class="slider-header-block tab-header d-flex border-bottom mb--20">
                    <div class="block-title-2 mb-0 border-0">
                        <h2>@lang('genel.marsk9galeri')</h2>
                    </div>
                    <ul class="pm-tab-nav tab-nav-style-2 nav nav-tabs hidden-xs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active">{{$viewAyarlar->{'site_baslik_'.session('dil')} }}</a>
                        </li>
                    </ul>
                </div>

                <div class=" tab-content pm-slider-tab-content">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="petmark-slick-slider petmark-slick-slider--wrapper-2 border grid-column-slider  arrow-type-two" data-slick-setting='{
                                "autoplay": true, "autoplaySpeed": 3000,"slidesToShow": 4,"rows" :2, "arrows": true}'
                             data-slick-responsive='[{"breakpoint":1200, "settings": {"slidesToShow": 3} },{"breakpoint":768, "settings": {"slidesToShow": 2} },{"breakpoint":480, "settings": {"slidesToShow": 1,"rows" :1} }]'>
                            @foreach($anasayfaresim as $resim)
                                <div class="single-slide">
                                    <div class="pm-product portfolio">
                                        <div class="portfolio__image">
                                            @if($resim->resim == null)
                                                <img src="/uploads/anasayfa-resim/default.jpg" alt="" class="img-responsive">
                                            @else
                                                <img src="/uploads/anasayfa-resim/{{$resim->resim}}" alt="" class="img-responsive">
                                            @endif
                                            <div class="hover-conents portfolio__content">
                                                <ul class="product-btns portfolio__icon">
                                                    <li><a class="popup-link" href="{{ empty( $resim->resim) ?  '/uploads/anasayfa-resim/default.jpg' : '/uploads/anasayfa-resim/' . $resim->resim}}"
                                                           data-lightbox="example-set" data-title="{{$resim->{'baslik_'.session('dil')} }}"> <i class="ion-ios-search"></i></a></li>
                                                </ul>
                                            </div>
                                            <div class="content">
                                                <h3 class="kes-tek" style="font-size: 13px;margin: 15px"> {{$resim->{'baslik_'.session('dil')} }} </h3>
                                                <div class="text-orange">
                                                    <span style="font-size: 11px">{{$viewAyarlar->{'site_baslik_'.session('dil')} }}</span>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-xl-3 pt--50 pt-lg-0 hidden-xs">
                <div class="block-title-2">
                    <h2>MARS K-9 AT ÇİFTLİĞİ</h2>
                </div>
                <img src="/uploads/anasayfa-resim/rasim.jpg" alt="" class="img-responsive">

            </div>
        </div>
    </div>
</div>
<!-- Home-2 => Promotion Block 1 -->
<section class="pt--50 space-db--30">
    <h2 class="d-none">{{$viewAyarlar->{'site_baslik_'.session('dil')} }}</h2>
    <div class="container">
        <div class="row">

            <div class="col-lg-6 col-sm-12  col-md-12">
                <div class="gizle">
                    <div style="margin: 0px 0px 10px 0px;padding: 0px !important;" class="col-md-12 animation bounce animation-visible" data-animation="bounce" data-animation-delay="0">
                        <div style="min-height:130px;background-image: url('/uploads/samples/banner-ust.png');width: 100%;height:auto;padding: 10px 10px;border-radius: 6px;color: #ffffff" class="call-to-action animation fadeInUp animation-visible" data-animation="fadeInUp">
                            <div class="cta-txt">
                                <div class="col-md-10" style="float: left">
                                    <a href="javascript:void(0)"><h2 class="fn-b" style="color: #baff00;font-size: 24px;line-height: 1.1;">@lang('genel.mersinilk')</h2></a>
                                    <span class="fn-y" style="font-size: 16px;">@lang('genel.merkeztarim')</span>
                                </div>
                                <div class="col-md-2  hidden-xs" style="float: right">
                                    <figure style="height: 40px;width: 40px;display: inline-block;margin-bottom: 0px;" class="thumbnail"><a href="javascript:void(0)"><img src="/frontend/images/tarim.png"  class="img-responsive"></a></figure>
                                    <figure style="height: 40px;width: 40px;display: inline-block;margin-bottom: 0px" class="thumbnail"><a href="javascript:void(0)"><img src="/frontend/images/mersinb_s_logo.png"  class="img-responsive"></a></figure>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-sm-12  col-md-12">
                <div class="gizle">
                    <div style="margin: 0px 0px 10px 0px;padding: 0px !important;" class="col-md-12 animation bounce animation-visible" data-animation="bounce" data-animation-delay="0">
                        <div style="min-height:130px;background-image: url('/uploads/samples/banner-ust.png');width: 100%;height:auto;padding: 10px 10px;border-radius: 6px;color: #ffffff" class="call-to-action animation fadeInUp animation-visible" data-animation="fadeInUp">
                            <div class="cta-txt">
                                <div class="col-md-12" style="float: left">
                                    <a href="javascript:void(0)"><h2 class="fn-b" style="color: #baff00;font-size: 24px;line-height: 1.1">@lang('genel.akdeniztek')</h2></a>
                                    <span class="fn-y" style="font-size: 16px;">@lang('genel.akdeniztekaciklama')</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section class="pt--50 space-db--30 hidden-xs">
    <div class="container">
        <a class="promo-image overflow-image">
            <img src="/uploads/samples/banner.png" alt="">
        </a>
    </div>
</section>

<div class="pt--50">
    <div class="container">
        <div class="block-title-2">
            <h2>@lang('genel.ozelyetistirme')</h2>
        </div>
        <div class="petmark-slick-slider petmark-slick-slider--wrapper-2 border normal-slider" data-slick-setting='{
                                                                "autoplay": true,
                                                                "autoplaySpeed": 3000,
                                                                "slidesToShow": 3,
                                                                "arrows": true
                                                        }'
             data-slick-responsive='[{"breakpoint":991, "settings": {"slidesToShow": 2} },
                                    {"breakpoint":768, "settings": {"slidesToShow": 1}
                                }]'>

            @foreach($ozelkopekler as $ozelkopek)
                <div class="single-slide">
                    <div class="pm-product  product-type-list">
                        <div class="image">
                            <a href="product-details.html"><img src="/frontend/images/kopeklerimiz/{{$ozelkopek->resim}}" alt=""></a>

                            {{--<span class="onsale-badge">Sale!</span>--}}
                        </div>
                        <div class="content">
                            <h3> <a href="product-details.html"> {{$ozelkopek->baslik}} </a></h3>
                            {{--<div class="price text-orange">
                                <span class="old">$200</span>
                                <span>$300</span>
                            </div>--}}
                            <div class="rating-widget mt--20">
                                <a href="" class="single-rating"><i class="fas fa-star"></i></a>
                                <a href="" class="single-rating"><i class="fas fa-star"></i></a>
                                <a href="" class="single-rating"><i class="fas fa-star"></i></a>
                                <a href="" class="single-rating"><i class="fas fa-star-half-alt"></i></a>
                                <a href="" class="single-rating"><i class="far fa-star"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

    </div>
</div>
<div class="pt--50 pb--50">
    <div class="container">

        <div class="petmark-slick-slider brand-slider  border normal-slider grid-border-none" data-slick-setting='{
                            "autoplay": true,
                            "autoplaySpeed": 3000,
                            "slidesToShow": 5,
                            "arrows": true
                        }'
             data-slick-responsive='[
                            {"breakpoint":991, "settings": {"slidesToShow": 4} },
                            {"breakpoint":768, "settings": {"slidesToShow": 3} },
                            {"breakpoint":480, "settings": {"slidesToShow": 2} },
                            {"breakpoint":320, "settings": {"slidesToShow": 1} }
                        ]'>

            <div class="single-slide">
                <a href="#" class="overflow-image brand-image">
                    <img src="/frontend/petmark/image/brand/brand.png" alt="">
                </a>
            </div>
            <div class="single-slide">
                <a href="#" class="overflow-image brand-image">
                    <img src="/frontend/petmark/image/brand/brand-2.png" alt="">
                </a>
            </div>
            <div class="single-slide">
                <a href="#" class="overflow-image brand-image">
                    <img src="/frontend/petmark/image/brand/brand-3.png" alt="">
                </a>
            </div>
            <div class="single-slide">
                <a href="#" class="overflow-image brand-image">
                    <img src="/frontend/petmark/image/brand/brand-4.png" alt="">
                </a>
            </div>
            <div class="single-slide">
                <a href="#" class="overflow-image brand-image">
                    <img src="/frontend/petmark/image/brand/brand-5.png" alt="">
                </a>
            </div>
            <div class="single-slide">
                <a href="#" class="overflow-image brand-image">
                    <img src="/frontend/petmark/image/brand/brand-6.png" alt="">
                </a>
            </div>
        </div>

    </div>
</div>
@section('head_js')
    <script src="/eklentiler/popup-portfolyo/dist/js/lightbox-plus-jquery.min.js"></script>
@endsection
@include ('frontend.moduller.modal')

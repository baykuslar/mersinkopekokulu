@extends('frontend.master')
@section('title') @lang('genel.videogaleri') @endsection
@section('govde')
			<nav aria-label="breadcrumb" class="breadcrumb-wrapper">
				<div class="container">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/">@lang('genel.anasayfa')</a></li>
						<li class="breadcrumb-item active" aria-current="page">@lang('genel.videogaleri')</li>
					</ol>
				</div>
			</nav>
            <section class="section light" id="services" style="padding:40px 0px 0px 0px !important;">
					<div class="container">
						<div class="row">
							<div class="col-md-6" data-animation="fadeInLeft" data-animation-delay="0">
								<div class="myaccount-content">
									<h3>@lang('genel.videogaleri')</h3>
									<div class="welcome mb-20">
										<p>
											@lang('genel.videosayfaaciklama')
										</p>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<figure class="alignnone video-holder blog-video">
                                <iframe class="w-100" src="{{$videolar[0]->url_adresi}}" width="612" height="380" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
								</figure>
							</div>
						</div>

						<!-- Featured section -->
						<section class="featured-section featured-section__alt parallax-bg" data-stellar-ratio="2" data-stellar-background-ratio="0.5" style="margin-top:0px">
							<div class="row">
								<div class="col-sm-6 col-md-3">
									</div>
								</div>
							
						</section>
                        </div>
                        </section>

			<!-- Page Content -->
			<section class="page-content" style="padding:40px 0px 0px 0px;">
				<div class="container">
					<div class="row">
						<div class="col-md-12" data-animation="fadeInLeft" data-animation-delay="0">
							<div class="myaccount-content">
								<div class="welcome mb-20">
									<p>
									<div class="project-feed project-feed__2cols row" style="margin-bottom:0px">
										@foreach($videolar as $video)
											<div class="col-sm-6 col-md-6 project-item" style="border-bottom: 1px dashed #cecece;margin-bottom: 30px">
												<div class="project-item-inner blog-post post-details single-block" style="margin-bottom: 20px">
													<figure class="alignnone video-holder blog-video">
														<iframe class="w-100" src="{{$video->url_adresi}}" width="558" height="325" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
													</figure>
													<div class="project-desc">
														<h6 class="title"><a href="#">{{$video->{'baslik_'.session('dil')} }}</a></h6>
														<span class="desc">{{$video->{'aciklama_'.session('dil')} }}</span>
													</div>
												</div>
											</div>
										@endforeach
									</div>
									</p>
								</div>
							</div>
						</div>
					</div>

				</div>
			</section>
			<!-- Page Content / End -->
@endsection

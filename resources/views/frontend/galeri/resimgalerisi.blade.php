@extends('frontend.master')
@section('title') @lang('genel.resimgaleri') @endsection
@section('govde')
@section('css_dahil_edilecek_stiller')
	<link href="/css/paginate.css" rel="stylesheet">
	<link rel="stylesheet" href="/eklentiler/popup-portfolyo/dist/css/lightbox.min.css">
@endsection
	<nav aria-label="breadcrumb" class="breadcrumb-wrapper">
		<div class="container">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="/">@lang('genel.anasayfa')</a></li>
				<li class="breadcrumb-item active" aria-current="page">@lang('genel.resimgaleri')</li>
			</ol>
		</div>
	</nav>
    <main id="content" class="main-content-wrapper overflow-hidden">
        <!-- Portfolio Start -->
        <div class="portfolio-area portfolio-blog-masonry">
            <div class="pb--30">
                <div class="text-center">
                    <div class="portfolio-filters messonry-button">
                        <button data-filter="*" class="is-checked">@lang('genel.tumu')</button>
                        @foreach($galerikategori as $kategori)
                        <button data-filter=".{{$kategori->id}}">{{$kategori->{'baslik_'.session('dil')} }}</button>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="portfolio-wrapper mesonry-list row">
                <div class="resizer "></div>
                @foreach($resimgalerisi as $galeri)
                    <div class=" portfolio-item portfolio-25 {{$galeri->kategori_id}}">
                        <div class="portfolio">
                            <div class="portfolio__image">
                                @if($galeri->resim == null)
                                    <img src="/uploads/galeri/default.jpg" alt="" class="img-responsive">
                                @else
                                    <img src="/uploads/galeri/{{$galeri->resim}}" alt="" class="img-responsive">
                                @endif
                            </div>
                            <div class="portfolio__content">
                                <div class="portfolio__icon">
                                    <a class="popup-link" href="{{ empty( $galeri->resim) ?  '/uploads/galeri/default.jpg' : '/uploads/galeri/' . $galeri->resim}}"
                                       data-lightbox="example-set" data-title="{{$galeri->{'baslik_'.session('dil')} }}">     <i class="fa fa-search"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
{{--						<div class="row">
                <div class="col col-xs-12">
                    <div class="pagination-wrapper">
                        {{ $resimgalerisi->links('frontend.moduller.paginate') }}
                    </div>
                </div>
            </div>--}}

        </div>
        <!-- Portfolio End -->
    </main>
@endsection
@section('head_js')
<script src="/eklentiler/popup-portfolyo/dist/js/lightbox-plus-jquery.min.js"></script>
@endsection

@extends('frontend.master')
@section('title') @lang('genel.egitimlerimiz') @endsection
@section('govde')
    <nav aria-label="breadcrumb" class="breadcrumb-wrapper">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">@lang('genel.anasayfa')</a></li>
                <li class="breadcrumb-item active" aria-current="page">@lang('genel.egitimlerimiz')</li>
            </ol>
        </div>
    </nav>
    <section class="blog-page-section with-sidebar">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-xl-12 order-lg-2 mb--40  left-slide-margin">
                    <div class="row">
                        @foreach($kayitlar as $kayit)
                        <div class="col-xl-3 col-md-4 mb--30">
                            <div class="blog-post">
                                <a href="/mersin-kopek/{{$kayit->slug}}" class="blog-image">
                                    @if($kayit->resim == null)
                                        <img src="/uploads/egitimlerimiz/default.jpg" alt="Duvar saati" class="img-responsive">
                                    @else
                                        <img src="/uploads/egitimlerimiz/{{$kayit->resim}}" alt="Duvar saati" class="img-responsive">
                                    @endif
                                </a>
                                <div class="blog-content mt--15 text-center">
                                    <header>
                                        <h3 class="blog-title"> <a href="/mersin-kopek/{{$kayit->slug}}">{{$kayit->{'baslik_'.session('dil')} }} </a></h3>
                                        {{--<div class="post-meta">
                                            <span class="post-author">
                                              <i class="fas fa-user"></i>
                                              <span class="text-gray">Posted by : </span>admin</span>
                                        </div>--}}
                                    </header>
                                    <article>
                                        <span class="kes-340-5">
                                            {!! $kayit->{'aciklama_'.session('dil')} !!}
                                        </span>
                                    </article>
                                    <div class="blog-btn pb--10 pt--10">
                                        <a href="/mersin-kopek/{{$kayit->slug}}" class="btn btn-rounded btn-outlined--primary">@lang('genel.devaminioku')</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>

    <style>
        .aktif > a{
            color: red !important;
        }
    </style>

@endsection

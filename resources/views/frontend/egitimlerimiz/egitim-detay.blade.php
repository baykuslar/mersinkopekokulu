@extends('frontend.master')
@section('title') {{$kayitlar->{'baslik_'.session('dil')} }} @endsection
@section('govde')
    <style>
        .aktif > a{
            color: red !important;
        }
    </style>
    <nav aria-label="breadcrumb" class="breadcrumb-wrapper">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">@lang('genel.anasayfa')</a></li>
                <li class="breadcrumb-item"><a href="/egitimlerimiz">@lang('genel.egitimlerimiz')</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{$kayitlar->{'baslik_'.session('dil')} }}</li>
            </ol>
        </div>
    </nav>
    <main class="section-padding shop-page-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-xl-9 order-lg-2 mb--40">
                    <div class="shop-toolbar">
                        <div class="row align-items-center">
                            <div class="col-5 col-md-3 col-xl-4">
                                <!-- Product View Mode -->
                                <div class="product-view-mode">
                                    <h2 class="sidebar-title">{{$kayitlar->{'baslik_'.session('dil')} }}</h2>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="blog-posts-container">
                        <div class="blog-post blog-style-list">
                            <div class="row">
                                <div class="col-lg-12 col-md-12">
                                    <div class="blog-content mt-md-0">
                                        <header>
                                            {{--<h3 class="blog-title"> <a href="blog-details.html">Blog image post </a></h3>--}}
                                            <div class="post-meta">
                                                <span class="post-author">
                                                  <i class="fas fa-user"></i>
                                                  <span class="text-gray">@lang('genel.sahip') : </span>{{$viewAyarlar->{'site_baslik_'.session('dil')} }}</span>
                                                                                <span class="post-separator">|</span>
                                                                                <span class="post-date">
                                                  <i class="far fa-calendar-alt"></i>
                                                  <span class="text-gray">@lang('genel.guncelleme') : </span>
                                                 {{\Carbon\Carbon::parse($kayitlar->created_at)->format('d/m/Y')}}
                                                </span>
                                                {{$kayitlar->hit}}
                                            </div>
                                        </header>
                                        <article>
                                            <h3 class="d-none sr-only">{{$kayitlar->{'baslik_'.session('dil')} }}</h3>
                                            <p>{!! $kayitlar->{'aciklama_'.session('dil')}  !!}</p>
                                        </article>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-xl-3 order-lg-1">
                    <div class="sidebar-widget">
                        <div class="single-sidebar">
                            <h2 class="sidebar-title">@lang('genel.egitimlerimiz')</h2>
                                @foreach($viewEgitimlerimiz as $egitim)
                                    <div class="btn-block">
                                        <a href="/mersin-kopek/{{$egitim->slug}}" class="btn btn-rounded btn-outlined--primary sol-btn @if( $egitim->slug == $slug ) hover-btn @endif ">{{$egitim->{'baslik_'.session('dil')} }}</a>
                                    </div>
                                @endforeach
                            <h2 class="sidebar-title bsk">@lang('genel.ilgiligorsel')</h2>
                            @if($kayitlar->resim == null)
                                <img src="/uploads/egitimlerimiz/default.jpg" alt="{{$kayitlar->{'baslik_'.session('dil')} }}" class="img-responsive">
                            @else
                                <img src="/uploads/egitimlerimiz/{{$kayitlar->resim}}" alt="{{$kayitlar->{'baslik_'.session('dil')} }}" class="img-responsive">
                            @endif
                        </div>

                        <div class="single-sidebar">
                            <h2 class="sidebar-title">@lang('genel.etiketler')</h2>
                            <ul class="sidebar-tag-list">
                                @foreach(explode(',', $kayitlar->meta_keywords) as $kelime)
                                    <li><a href="#"> {{$kelime}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    </div>
@endsection

@extends('frontend.master')
@section('title') @lang('genel.anasayfa') @endsection
@section('govde')
@section('css_dahil_edilecek_stiller')
	<style>
		#harita {
			width:100%;
			height:550px;
		}
		.resim{
			overflow: hidden;
			max-width: 300px;
			height: 100%;
			display: block;
		}
		.baslikadi{
			margin-top: 10px;
			padding: 6px;
			color: #000;
			font-size: 15px;
			font-weight: 500;
		}
		.telefone{
			font-size: 14px;
			font-weight: 300;
			padding: 5px;
		}
		.telefone > span{
			padding-left: 6px;
		}
		.adres{
			font-size: 12px;
			font-weight: 300;
			padding: 5px;
		}
		.adres > span{
			padding-left: 6px;
		}
		.infoBox-close2 {
			padding: 8px;
			position: absolute;
			width: 160px;
			height: 30px;
			line-height: 12px;
			top: 130px;
			left: 19px;
			color: #ffffff;
			border-radius: 5px;
			z-index: 20;
			box-shadow: 0px 0px 0px 3px rgba(220, 255, 4, 0.60);
			cursor: pointer;
			text-align: center;
			background: rgba(255, 143, 0, 0.91);
			font-size: 15px
		}
		.infoBox-close2:hover {
			color: #000000;
			box-shadow: 0px 0px 0px 3px rgba(0, 240, 255, 0.67);
			background: rgb(0, 251, 255);
		}
		.infoBox-close {
			position: absolute;
			width: 25px;
			height: 25px;
			line-height: 25px;
			top: 17px;
			right: 17px;
			color: #000000;
			border-radius: 100%;
			z-index: 20;
			box-shadow: 0px 0px 0px 3px rgba(0, 188, 255, 0.4);
			cursor: pointer;
			text-align: center;
			background: rgba(0, 255, 233, 0.65);
			padding-top: 5px !important;
		}
		.infoBox-close:hover {
			color: #fbaf17;
			box-shadow: 0px 0px 0px 5px rgba(255, 255, 255, 0.77);
			background: rgb(0, 0, 0);
		}
		.fa-remove:before, .fa-close:before, .fa-times:before {
			content: "\f00d";
		}
		img[src="https://maps.gstatic.com/mapfiles/api-3/images/mapcnt6.png"] {
			display: none;
		}
		.gm-style-iw {
			width: 300px !important;
		}
		.gm-ui-hover-effect{
			display: none !important;
		}
		.gm-style-iw-d{
			overflow: hidden !important;
		}
		.gm-style .gm-style-iw-c {
			position: absolute;
			box-sizing: border-box;
			overflow: hidden;
			top: 0;
			left: 0;
			transform: translate(-50%,-100%);
			background-color: white;
			border-radius: 5px;
			padding: 12px !important;
			box-shadow: 0 2px 7px 1px rgba(0,0,0,0.3);
		}
		.adp-directions{
			width: 100% !important;
		}
	</style>
@endsection
	<style>
		#btn-gonder{
			padding: 10px 15px;
		}
	</style>

<nav aria-label="breadcrumb" class="breadcrumb-wrapper">
	<div class="container">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="/">@lang('genel.anasayfa')</a></li>
			<li class="breadcrumb-item active" aria-current="page">@lang('genel.iletisim')</li>
		</ol>
	</div>
</nav>
<section class="contact-page-section section-padding bg-gray">
	<div class="container">
		<div class="ct-section-title-2 mb--60 text-center">
			<h2>@lang('genel.iletisimformu')</h2>
		</div>
		<form id="frm-iletisim" name="frm" role="form" class="">
			<input type="hidden" id="token" name="_token" value="{{csrf_token()}}">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label>@lang('genel.adiniz') <span class="required">*</span></label>
						<input type="text"
							   value=""
							   class="form-control"
							   name="adi_soyadi" id="adi_soyadi" required>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label>@lang('genel.email') <span class="required">*</span></label>
						<input type="email"
							   value=""
							   data-msg-required="Lütfen e-mail adresinizi yazınız?"
							   class="form-control"
							   name="email"
							   id="email" required>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label>@lang('genel.telefon') <span class="required">*</span></label>
						<input type="text"
							   value=""
							   class="form-control"
							   name="telefon"
							   id="telefon" required>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label>@lang('genel.konu') </label>
						<input type="text"
							   value=""
							   class="form-control"
							   name="konu"
							   id="konu" required>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-9">
					<div class="form-group">
						<label>@lang('genel.mesaj')  <span class="required">*</span></label>
						<textarea
								rows="10"
								class="form-control"
								name="mesaj"
								id="mesaj" required></textarea>
					</div>
				</div>
				<div class="col-md-3">
					<div class="col-md-6">
						<label>@lang('genel.frdogru') <span class="required">*</span></label>
						<div class="form-group">
							<div id="dv-captha" class="g-recaptcha"></div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="submit-btn">
							<button class="btn btn-black" type="button" id="btn-gonder" data-method="confirm" data-loading-text="Lütfen Bekleyiniz!">@lang('genel.gonder') </button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>

</section>
				<!-- Google Map / End -->
<section class="ct-feature-section">
	<div class="container">
		<div class="row justify-content-center text-center mb--20">
			<div class="col-lg-4 col-md-6 col-sm-8 col-12">
				<div class="ct-section-title-2">
					<h2 class="mb--15">@lang('genel.iletisimbilgileri')</h2>
					<p>@lang('genel.iletigonder')</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-3 col-sm-6 mt--30">
				<div class="ct-feature">
					<div class="icon">
						<i class="fas fa-map-marker-alt"></i>
					</div>
					<h3>@lang('genel.adres')</h3>
					<p>{{$viewAyarlar->adres}}</p>
				</div>
			</div>
			<div class="col-lg-3 col-sm-6 mt--30">
				<div class="ct-feature">
					<div class="icon">
						<i class="fas fa-phone"></i>
					</div>
					<h3>@lang('genel.telefon')</h3>
					<p><a href="tel://+9{{$viewAyarlar->gsm}}">{{$viewAyarlar->gsm}}</a><br>
						<a href="tel://+9{{$viewAyarlar->telefon}}">{{$viewAyarlar->telefon}}</a>
					</p>
				</div>
			</div>
			<div class="col-lg-3 col-sm-6 mt--30">
				<div class="ct-feature">
					<div class="icon">
						<i class="far fa-envelope"></i>
					</div>
					<h3>@lang('genel.email')</h3>
					<p><a href="mailto:{{$viewAyarlar->email}}">{{$viewAyarlar->email}}</a><br>
						<a href="mailto:{{$viewAyarlar->email_2}}">{{$viewAyarlar->email_2}}</a>
					</p>
				</div>
			</div>
			<div class="col-lg-3 col-sm-6 mt--30">
				<div class="ct-feature">
					<div class="icon">
						<i class="fas fa-link"></i>
					</div>
					<h3>İnternet</h3>
					<p><a href="https://www.mersinkopekokulu.com">www.mersinkopekokulu.com</a><br>
						<a href="https://www.mersinkopekegitim.com">www.mersinkopekegitim.com</a><br>
						<a href="https://www.mersinkopekmamasi.com">www.mersinkopekoteli.com</a>
					</p>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="page-content" style="z-index: 999999999">
		<div id="harita"></div>
		<br><br>
		<div class="container">
			<div style="float: left; padding-right: 5px;margin-left: 9px; margin-top:10px;margin-bottom: 9px;width: 80%">
				<div id="locationField">
					<input id="baslangicKonumu" value="" class="form-control" size="10" placeholder="Adresinizi Giriniz" type="text">
				</div>
			</div>
			<div style="padding-left: 5px !important;"><a href="javascript:void(0);" onclick="javascript:harita.rotaAl()" class="btn btn-success" style="padding: 12px 16px;margin-top: 10px;margin-bottom: 9px;">@lang('genel.rota')</a></div>
			<div style="direction: ltr;" id="adresListesi"></div>
		</div>
</section>
<div class="clearfix"></div>
@endsection
@section('js_harita')
	<script type="text/javascript">
		var nokta = {};
		var bilgiler = [];
		var harita;
		var noktalar = [];
		var koordinat = {};
		var etiketlerYüklendi = false;
		var devam = false;

		function haritaOlusturucu() {
			harita = new google.maps.Map(document.getElementById('harita'), {mapTypeId: google.maps.MapTypeId.ROADMAP});
			harita.dirService = new google.maps.DirectionsService();
			harita.dirRenderer = new google.maps.DirectionsRenderer();
			harita.adresler = document.getElementById('adresListesi');


			bilgiler =[];
			bilgiler.push('<div class="resim"><img src="/uploads/harita-images/maps.png"></div>');
			bilgiler.push('<div class="baslikadi">Mersin Köpek Okulu - <i style="font-size: 12px;color: orangered">Mars K9 Köpek Okulu</i></div>');
			bilgiler.push('<div class="telefone"><i class="fa fa-phone"></i> <span>0 (554) 459 69 22\n</span></div>');
			bilgiler.push('<div class="telefone"><i class="fa fa-envelope"></i> <span>rasimtekin@hotmail.com</span></div>');
			bilgiler.push('<div class="adres"><i class="fa fa-map-marker"></i> <span>GÖZNE YOLU ÜZERİ DALAK DERESİ KÖYÜ </span></div>');
			bilgiler.push('<div class="infoBox-close2"><b>Köpek Okulu ve Oteli</b></div>');
			bilgiler.push('<div class="infoBox-close"><i class="fa fa-times"></i></div>');
			koordinat = {lat: 36.931797, lng: 34.560028 };
			nokta = {
				"lat": 36.931797,
				"lng": 34.560028,
				"baslik": 'MSP CREATIVE',
				"rakim": 17,
				"koordinat" : koordinat,
				"pencere" : new google.maps.InfoWindow({
					maxWidth:550,
					content: bilgiler.join('')
				}),
				"isaretci" : new google.maps.Marker({
					"position": koordinat,
					"map" : harita,
					/* "animation" : google.maps.Animation.DROP,*/
					"animation" : google.maps.Animation.BOUNCE,
					"title" : "MSP CREATIVE",
				})
			};
			noktalar[1] = nokta;
			noktalar[1].isaretci.addListener('click', function() {
				noktalar[1].pencere.open(harita, noktalar[1].isaretci);
			});

			$(document).on('click','.infoBox-close',function () {
				noktalar[1].pencere.close();
			});
			harita.pencereleriKapat = function() {
				$.each(noktalar, function (id, nokta) {
					if(nokta){
						nokta.pencere.close();
					}
				});
			};

			harita.rotaAl = function(){
				var baslangicAdresi = $('#baslangicKonumu').val();

				var dirRequest = {
					origin      : baslangicAdresi,
					destination : harita.isaretci.getPosition(),
					travelMode  : google.maps.DirectionsTravelMode.DRIVING,
					unitSystem  : google.maps.DirectionsUnitSystem.IMPERIAL
				};

				harita.dirService.route(dirRequest, harita.rotaGoster);
			};

			harita.rotaGoster = function (dirResult, dirStatus) {
				if (dirStatus != google.maps.DirectionsStatus.OK) {
					alert('Directions failed: ' + dirStatus);
					return;
				}
				// Show directions
				harita.dirRenderer.setMap(harita);
				harita.dirRenderer.setPanel(harita.adresler);
				harita.dirRenderer.setDirections(dirResult);
			};

			harita.autocomplete = new google.maps.places.Autocomplete(document.getElementById('baslangicKonumu'));

			harita.mevcutPozisyon = function(){
				return new google.maps.LatLng(harita.isaretci.getPosition().lat(), harita.isaretci.getPosition().lng());
			};

			konumlan(1);
		}

		function konumlan(id){
			harita.pencereleriKapat();
			harita.setZoom(noktalar[id].rakim);
			harita.isaretci = noktalar[id].isaretci;
			harita.panTo({lat: noktalar[id].lat, lng: noktalar[id].lng});
			noktalar[id].pencere.open(harita, noktalar[id].isaretci);
		}
	</script>
	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyASN92GVikQdhjUWg-D1Nvf2m7eNdjBGcg&signed_in=true&libraries=places&callback=haritaOlusturucu"></script>
@endsection
@section('dahil_edilecek_js')
	<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>
	<script type="text/javascript">
        _vToken = '';
        var onloadCallback = function() {
            grecaptcha.render('dv-captha', {
                'sitekey' : '{{env('reCAPTCHA_SITE_ANAHTARI')}}',
                'callback' : function(token){
                    _vToken = token;
                    console.log(_vToken);
                },
                'expired-callback' : function(e){
                    _vToken = '';
                },
                'error-callback' : function(e){
                    _vToken = '';
                }
            });
        };

        $( document ).ready(function() {
            $('#btn-gonder').on('click', function (e) {
                e.preventDefault();
                if( _vToken == '' ) {
                    sweetAlert("Kayıt Başarısız", "Lütfen robot olmadığınızı doğrulayın.", "error");
                    return false;
                }
                var iletisim = {};
                iletisim.adi_soyadi = $('#adi_soyadi').val();
                iletisim.email = $('#email').val();
                iletisim.telefon = $('#telefon').val();
                iletisim.konu = $('#konu').val();
                iletisim.mesaj = $('#mesaj').val();
                iletisim._token = $('#token').val();
                iletisim._vToken = _vToken;
                $.ajax({
                    url : '/iletisim-gonder',
                    type : 'POST',
                    data : iletisim,
                    success : function(cevap){
                        if( cevap.success == true ){
                            swal({ title: "Kayıt Tamamlandı", text: "Kaydınız başarıyla tamamlanmıştır. Size en kısa sürede ulaşılacaktır.", type: "success" }, function() {
                                window.location = "/iletisim";
                            });
                        }else{
                            sweetAlert("Kayıt Başarısız", "Kayıt işlemi bir nedenden dolayı tamamlanamadı. Lütfen robot olmadığınızı doğrulayın.", "error");
                        }
                    }
                });
                var buton = $(this);
                buton.button('loading');
            });
        });
	</script>
@endsection

<!DOCTYPE html>
<html lang="TR">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @include('frontend.moduller.meta-taglari')
    @include('frontend.moduller.font')
    @include('frontend.moduller.favicon')
<!--webchat -->
    <link rel="stylesheet" type="text/css" href="/eklentiler/whatsapp-chat/css/whatschat-layout.css"/>
    <link rel="stylesheet" type="text/css" href="/eklentiler/whatsapp-chat/css/style/whatschat-style1.css"/>
    <link id="whatschat-color" rel="stylesheet" href="/eklentiler/whatsapp-chat/css/colors/color-green.css">


    <!-- All css files are included here. -->
        <link rel="stylesheet" href="/frontend/petmark/css/plugins.css">
        <link rel="stylesheet" href="/frontend/petmark/css/main.css">
        <link rel="stylesheet" href="/frontend/petmark/css/custom.css">

    <!-- EKLENTİLER -->
    <link href="/eklentiler/notification/css/notification.css" rel="stylesheet">
    <link href="/eklentiler/alertconfirm/jquery-confirm-master/dist/jquery-confirm.min.css" rel="stylesheet">
    <link href="/eklentiler/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">
    <link href="/eklentiler/fonts/font-awesome.min.css" rel="stylesheet" >
    <link href="/eklentiler/fontawesome-5/css/all.min.css" rel="stylesheet" type="text/css" />
    <link href="/eklentiler/material-floating-button/mfb.min.css" rel="stylesheet" type="text/css" />
    <link href="/eklentiler/jquery-input-mask/inputmask.css" rel="stylesheet">
    <script src="/eklentiler/material-floating-button/mfb.min.js"></script>



    <link href="/frontend/assets/css/custom.css" rel="stylesheet">
    <link href="/frontend/css/custom.css" rel="stylesheet">
    <link href="/css/custom.css" rel="stylesheet">
    <link href="/frontend/css/modal-style.css" rel="stylesheet">
    @yield('head_js') <!--// İlk Edilecek yollar -->
    <!--webchat -->
    <link rel="stylesheet" type="text/css" href="/eklentiler/whatsapp-chat/css/font-awesome.min.css">
    @yield('style')
<!--Start of Tawk.to Script-->
    @if($viewWebchat->aktif_pasif=='on')
    <!--Start of Tawk.to Script-->
        <!--Start of Tawk.to Script-->
        <script type="text/javascript">
            var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
            (function(){
                var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
                s1.async=true;
                s1.src='https://embed.tawk.to/5eb1eb5f81d25c0e58492080/default';
                s1.charset='UTF-8';
                s1.setAttribute('crossorigin','*');
                s0.parentNode.insertBefore(s1,s0);
            })();
        </script>
        <!--End of Tawk.to Script-->
    @else
    @endif

    @yield('style')
    @yield('css_dahil_edilecek_stiller')
</head>
<body class=" petmark-theme-2">
<div class="site-wrapper">
    <header class="header petmark-header-2">
        

@if($viewGirismodal->aktif_pasif == 'on')
    <!-- Modal -->
    <div class="modal fade modal-quick-view" id="quickModal" tabindex="-1" role="dialog" aria-labelledby="quickModal" data-backdrop="static" data-keyboard="false"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="pm-product-details">
                    <div class="row modal-body">
                        <div class="col-md-12 mt-12 mt-lg-0">
                            <div class="description-block">
                                <div class="header-block">
                                    <h2>Bu İnternet sitesi ve Domain Satılıktır.</h2>
                                </div>
                                <!-- Price -->

                                <h1 class="price">FİYAT: <span class="old-price">180.000 ₺</span><b style="font-size:33px"> <i class="fas fa-check-circle"></i> 155.000 ₺ </b> </h1>
                                <!-- Sharing Block 2 -->
                                <div class="share-block-2 mt--30">
                                    <h4>İLETİŞİM</h4>
                                    <ul class="social-btns social-btns-3">
                                        <li>
                                        <button type="button" id="mybutton" class="btn btn-success" style="position:absolute"><a href="#" class="phone"><i class="fab fa-phone-f"></i> 0.530 263 70 13</a></li></button>    
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@else
@endif
     
        <!-- Site Wrapper Starts -->
        {{-- @include ('frontend.moduller.ust-bar')
        @include ('frontend.moduller.menu-bar')
    </header>
        @yield('govde')
        @include('frontend.moduller.footer')
    </div>
@include('frontend.moduller.whatsapp-chat') --}}
{{--@include('frontend.moduller.material-floating-button')--}}
    <!-- All jquery file included here
    ================================================== -->
<script src="/frontend/petmark/js/plugins.js"></script>
<script src="/frontend/petmark/js/ajax-mail.js"></script>
<script src="/frontend/petmark/js/custom.js"></script>

<!-- GOOGLE_RECAPTCHA_KEY  JS-->
<script src='https://www.google.com/recaptcha/api.js'></script>
<!-- EKLENTİLER -->
<script src="/eklentiler/notification/js/notify.min.js"></script>
<script src="/eklentiler/notification/js/notify-metro.js"></script>
<script src="/eklentiler/notification/js/notifications.js"></script>
<script src="/eklentiler/alertconfirm/jquery-confirm-master/dist/jquery-confirm.min.js"></script>
<script src="/eklentiler/sweetalert/dist/sweetalert.min.js"></script>
<script src="/eklentiler/sweetalert/dist/jquery.sweet-alert.init.js"></script>
{{--<script src="/eklentiler/jquery-input-mask/jquery-3.2.1.min.js"></script>--}}

{{--<script src="/eklentiler/whatsapp-chat/js/jquery.min.js"></script>--}}
<script src="/eklentiler/whatsapp-chat/js/bootstrap.min.js"></script>
<script src="/eklentiler/whatsapp-chat/js/whatschat-layout.js"></script>
<script src="/eklentiler/whatsapp-chat/js/whatschat-style1.js"></script>

@yield('dahil_edilecek_js') <!--// Dahil Edilecek yollar -->
<script type="text/javascript">
    @if( !empty( session('onayMesaji') ) )
        toastr['success']('{{session('onayMesaji')}}', 'İşlem Başarılı');
    <?php \Session::put('onayMesaji', '' );?>
            @endif

            @if( !empty( session('hataMesaji') ) )
        toastr['error']('{{session('hataMesaji')}}', 'İşlem Başarısız');
    <?php \Session::put('hataMesaji', '' );?>
    @endif
</script>
@yield('js_harita')
<script>
    $(document).ready(function(){
        $.ajaxSetup({ cache: false });
        $('#search').keyup(function(){
            $('#result').html('');
            $('#state').val('');
            var searchField = $('#search').val();
            var expression = new RegExp(searchField, "i");
            $.getJSON('/frontend/data.json', function(data) {
                $.each(data, function(key, value){
                    if (value.name.search(expression) != -1 || value.location.search(expression) != -1)
                    {
                        $('#result').append('<li class="list-group-item link-class"><img src="'+value.image+'" height="40" width="40" class="img-thumbnail" /> '+value.name+' | <span class="text-muted">'+value.location+'</span></li>');
                    }
                });
            });
        });

        $('#result').on('click', 'li', function() {
            var click_text = $(this).text().split('|');
            $('#search').val($.trim(click_text[0]));
            $("#result").html('');
        });
    });
    <!-- Sadece Rakam Yızılı imput alanına START "autofocus maxlength="11" required="" onKeyPress="return numbersonly(this, event)""-->
    function numbersonly(myfield, e, dec)
    {
        var key;
        var keychar;

        if (window.event)
            key = window.event.keyCode;
        else if (e)
            key = e.which;
        else
            return true;
        keychar = String.fromCharCode(key);

        // control keys
        if ((key==null) || (key==0) || (key==8) ||
            (key==9) || (key==13) || (key==27) )
            return true;

        // numbers
        else if ((("0123456789").indexOf(keychar) > -1))
            return true;

        // decimal point jump
        else if (dec && (keychar == "."))
        {
            myfield.form.elements[dec].focus();
            return false;
        }
        else
            return false;
    }
    <!-- Sadece Rakam Yızılı imput alanına END -->
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#quickModal').modal('show');
    })
</script>
    </body>
</html>

@extends('frontend.master')
@section('title') SSS @endsection
@section('govde')
    <nav aria-label="breadcrumb" class="breadcrumb-wrapper">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">@lang('genel.anasayfa')</a></li>
                <li class="breadcrumb-item active" aria-current="page">@lang('genel.sssorular')</li>
            </ol>
        </div>
    </nav>

    <section class="sec-padding faq-home faq-page pt_90">
        <div class="container"  id="accordion-style-1">
            <div class="row">
                <!-- Accordion -->
                    <div class="col-md-12 mx-auto">
                        <div class="accordion" id="accordion">
                            <?php $aktifiYaptim = false;?>
                            @foreach($sssorular as $soru)
                            <div class="card">
                                <div class="card-header">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link btn-block text-left " type="button" data-toggle="collapse" data-target="#collapseOne{{$soru->id}}"  aria-controls="collapseOne">
                                            <i class="ti-headphone-alt btn-lg"></i> <i class="fa fa-angle-double-right mr-3"></i>{{$soru->{'baslik_'.session('dil')} }}
                                        </button>
                                    </h5>
                                </div>

                                <div id="collapseOne{{$soru->id}}" class="collapse fade @if( $aktifiYaptim == false  ) show <?php $aktifiYaptim = true;?>  @endif" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="card-body">
                                        <big>{{$soru->{'aciklama_'.session('dil')} }}</big>
                                        <a class="ml-3"><strong>{{$viewAyarlar->{'site_baslik_'.session('dil')} }} <i class="fa fa-angle-double-right"></i></strong></a>
                                    </div>
                                </div>
                            </div>
                                   @endforeach
                        </div>
                    </div>
                </div>
                <!-- .// Accordion -->

            </div>
    </section>
    <div class="clearfix" style="margin-bottom: 30px;"></div>
@endsection

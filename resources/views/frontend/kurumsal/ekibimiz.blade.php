@extends('frontend.master')
@section('title') Ekibimiz @endsection
@section('govde')
    <style>
        .card {
            background: #fff;
            border-radius: 2px;
            display: inline-block;
            height: auto;
            margin: 1rem;
            position: relative;
            width: 100%;
        }

        .card-1 {
            box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
            transition: all 0.3s cubic-bezier(.25,.8,.25,1);
            border: 1px solid #d4d4d4;
            border-radius: 15px;
        }

        .card-1:hover {
            box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);
        }
    </style>
    
    <nav aria-label="breadcrumb" class="breadcrumb-wrapper">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">ANASAYFA</a></li>
                <li class="breadcrumb-item active" aria-current="page">EKİBİMİZ</li>
            </ol>
        </div>
    </nav>
    <section class="service-section-4 bg-image" data-bg="/uploads/ekibimiz/bg.jpg">
        <div class="container">
            <div class="row">
                @foreach($ekibimiz as $ekip)
                <div class="col-lg-3 col-md-6 mb--30">
                    <div class="pricing-card">
                        <header>
                            <h2>{{$ekip->unvan}}</h2>
                        </header>
                        <div class="card-body">
                            @if($ekip->profil_resim == null)
                                <img src="/uploads/ekibimiz/default.png" alt="" class="img-responsive">
                            @else
                                <img src="/uploads/ekibimiz/{{$ekip->profil_resim}}" alt="agent" class="img-responsive">
                            @endif
                            <div class="card-btn">
                                <a href="#" class="btn btn-black">{{$ekip->adi_soyadi}}</a>
                            </div>
                            <ul class="pricing-list">
                                <li>{{$ekip->unvan}}</li>
                                <li><a href="mailto:{{$ekip->email}}"> {{$ekip->email}}</a></li>
                                <li>{{$ekip->telefon}}</li>
                                <li>{{$ekip->gsm}}</li>
                                <li> <b>Kısaca Hakkında: </b> {{$ekip->hakkinda}}</li>
                            </ul>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection

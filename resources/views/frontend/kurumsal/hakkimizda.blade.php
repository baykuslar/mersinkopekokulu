@extends('frontend.master')
@section('title') @lang('genel.hakkimizda') @endsection
@section('govde')
    <nav aria-label="breadcrumb" class="breadcrumb-wrapper">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">@lang('genel.anasayfa')</a></li>
                <li class="breadcrumb-item active" aria-current="page">@lang('genel.hakkimizda')</li>
            </ol>
        </div>
    </nav>

    <main id="content" class="main-content-wrapper overflow-hidden">
        <div class="about-area ">
            <div class="container-fluid p-0">
                <div class="row no-gutters align-items-center">
                    <div class="col-lg-6">
                        <div class="img-box text-center">
                            <img src="/uploads/hakkimizda/{{$hakkimizda->resim_9}}" alt="{{$viewAyarlar->{'site_baslik_'. session('dil')} }}">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="row">
                            <div class="col-10 offset-1">
                                <div class="about-text text-center">
                                    <h2 class="heading-secondary">
                                        <strong>{{$viewAyarlar->{'site_baslik_'. session('dil')} }}</strong>
                                    </h2>
                                    <h3 class="heading-secondary">
                                        <strong>{{$hakkimizda->{'ozel_baslik_'. session('dil')} }}</strong>
                                    </h3>
                                    <h5 class="heading-secondary">
                                        <strong> {{$hakkimizda->{'ozel_yazi_'. session('dil')} }}</strong>
                                    </h5>
                                    <p class="mb--40 mb-sm--30">{{$hakkimizda->{'hakkimizda_'. session('dil')} }} <span style="color: orangered"><small>@lang('genel.egitimuzman')</small> <big> Rasim TEKİN</big></span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="fact-area">
            <div class="container-fluid p-0">
                <div class="row no-gutters">
                    <div class="col-lg-3 col-sm-6">
                        <div class="fact">
                            <div class="fact__icon">
                                <img src="/uploads/hakkimizda/{{$hakkimizda->resim_1}}" alt="{{$viewAyarlar->{'site_baslik_'. session('dil')} }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="fact">
                            <div class="fact__icon">
                                <img src="/uploads/hakkimizda/{{$hakkimizda->resim_2}}" alt="{{$viewAyarlar->{'site_baslik_'. session('dil')} }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="fact">
                            <div class="fact__icon">
                                <img src="/uploads/hakkimizda/{{$hakkimizda->resim_3}}" alt="{{$viewAyarlar->{'site_baslik_'. session('dil')} }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="fact">
                            <div class="fact__icon">
                                <img src="/uploads/hakkimizda/{{$hakkimizda->resim_4}}" alt="{{$viewAyarlar->{'site_baslik_'. session('dil')} }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection

@extends('frontend.master')
@section('title') İnsanKaynakları @endsection
@section('govde')
    @section('style')
    <style type="text/css">
        .form-control {
            height: 36px;
            padding: 7px 24px;
            font-size: 14px;
        }
        .input-group-addon {
            padding: 7px 24px;
            font-size: 14px;
        }
        .form-group{
            font-size: 14px;
        }
    </style>
    @endsection

<nav aria-label="breadcrumb" class="breadcrumb-wrapper">
    <div class="container">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">ANASAYFA</a></li>
            <li class="breadcrumb-item active" aria-current="page">İNSAN KAYNAKLARI</li>
        </ol>
    </div>
</nav>

    <!-- Page Content -->
    <section class="page-content" style="padding-top:40px">
        <div class="container">
            <div class="tab-pane fade active show" id="dashboad" role="tabpanel">
                <div class="myaccount-content">
                    <h3>İNSAN KAYNAKLARI</h3>

                    <div class="welcome mb-20">
                        <p>Merhaba, <strong>Mars K-9 Köpek Okulu ve Köpek Oteli</strong> ekibine katılmak için lütfen aşağıdaki formu doldurunuz.
                            Başvurularınız <strong> insan kaynakları </strong> bölümümüzce değerlendirildikten sonra en kısa sürede sizlere geri dönüş sağlanacaktır.</p>
                    </div>
                </div>
            </div>

            <hr class="xs">
            <!----------------------------- galeri START--------------------------------------------------------------------->

            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default" style="box-shadow: 4px 4px 4px 1px rgba(0, 0, 0, 0.1);">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h3 class="panel-title">
                                            <button class="alert alert-info alert-dismissable" style="margin-bottom: 0px;">İŞ BAŞVURU FORMU</button>
                                        </h3>
                                    </div>
                                    <div class="col-md-6 pull-right">
                                        <!-------- breadcrumb ------->
                                        <ol class="breadcrumb pull-right" style="margin-bottom: 0px;margin-top: 8px;">
                                            <li >
                                                <button class="btn" style="background: red;">
                                                    <span style="color: white;">Lütfen gerekli alanları doldurunuz!. <i class="fa fa-info-circle fa-lg"></i></span>
                                                </button>
                                            </li>
                                        </ol>
                                        <!-------- breadcrumb ------->
                                    </div>
                                    @if(session('status'))
                                        <div class="alert alert-success" style="font-size: 20px;padding: 24px;position: absolute;">
                                            <a class="close" data-dismiss="alert">×</a>
                                            <strong>İşlem Başarılı!</strong> {{ session('status') }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="panel-body" style="background: #eaeaea">
                                <form class="form-horizontal" name="siteayarformu" action="/insan-kaynaklari" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="row">
                                        <div class="col-md-3">
                                            <button type="button" class="btn btn-block btn-xs" style="background:#848484;color:white">
                                                <span>Kişisel Bilgilerini Doldurunuz! </span>
                                                <i class="fa fa-sort-amount-asc"></i>
                                            </button>
                                        </div>
                                        <div class="col-md-9"> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Başvuru Fotoğrafı <span class="error">*</span></label>
                                        <div class="col-md-9">
                                            <input id="resim" type="file" name="resim" required/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label  class="col-md-3 control-label">Adınız Soyadınız <span class="error">*</span></label>
                                        <div class="col-md-9">
                                            <input class="form-control" type="text" name="adisoyadi" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Doğum Tarihi <span class="error">*</span></label>
                                        <div class="col-md-9">
                                            <input class="form-control" type="text" name="dogumtarihi" placeholder="gün/ay/yıl" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Cinsiyeti <span class="error">*</span></label>
                                        <div class="col-md-9">
                                            <select class="select2 form-control"  name="cinsiyet" autocomplete="off" required>
                                                <option selected="selected" value="">Seçiniz</option>
                                                <option  name="cinsiyet" value="Kız">Kız</option>
                                                <option  name="cinsiyet" value="Erkek">Erkek</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Gsm <span class="error">*</span></label>
                                        <div class="col-md-9">
                                            <input class="form-control"  type="text" name="gsm" autocomplete="off" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Telefon</label>
                                        <div class="col-md-9">
                                            <input class="form-control" type="text" name="telefon">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">E-mail <span class="error">*</span></label>
                                        <div class="col-md-9">
                                            <input class="form-control" type="email" name="email" autocomplete="off" >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Adres</label>
                                        <div class="col-md-9">
                                            <input class="form-control" type="text" name="adres" autocomplete="off">
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Askerlik Durumu <span class="error">*</span></label>
                                        <div class="col-md-3">
                                            <select class="select2 form-control"  name="ib_askerlik_durumu" id="ib_askerlik_durumu" autocomplete="off" >
                                                <option selected="selected" value="">Seçiniz</option>
                                                <option  name="ib_askerlik_durumu" value="Var">Var</option>
                                                <option  name="ib_askerlik_durumu" value="Yok">Yok</option>
                                                <option  name="ib_askerlik_durumu" value="Tecilli">Tecilli</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6 son-tecil hidden">
                                            <label class="col-md-4 control-label">Son Tecil Tarihi <span class="error">*</span></label>
                                            <div class="col-md-8">
                                                <input class="form-control" type="text" name="ib_son_tecil_tarihi" data-mask="99/99/9999" placeholder="gün/ay/yıl"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Ehliyet <span class="error">*</span></label>
                                        <div class="col-md-9">
                                            <select class="select2 form-control" name="ib_ehliyet" id="ib_ehliyet" autocomplete="off" >
                                                <option selected="selected" value="">Seçiniz</option>
                                                <option  name="ib_ehliyet" value="Var">Var</option>
                                                <option  name="ib_ehliyet" value="Yok">Yok</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group ehliyet_turu hidden">
                                        <label class="col-md-3 control-label">Ehliyet Türü <span class="error">*</span></label>
                                        <div class="col-md-9">
                                            <input class="form-control" type="text" name="ib_ehliyet_turu" placeholder="B Sınıf Ehliyet">
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <button type="button" class="btn btn-block btn-xs" style="background:#848484;color:white">
                                                <span>Eğitim Bilgilerini Doldurunuz! </span>
                                                <i class="fa fa-sort-amount-asc"></i>
                                            </button>
                                        </div>
                                        <div class="col-md-9"> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Mezuniyet Durumu <span class="error">*</span></label>
                                        <div class="col-md-9">
                                            <select class="select2 form-control" name="ib_mezuniyet" autocomplete="off" >
                                                <option value="" selected="selected">Seçiniz</option>
                                                <option name="ib_mezuniyet" value="İlkokul">İlkokul</option>
                                                <option name="ib_mezuniyet" value="Ortaokul">Ortaokul</option>
                                                <option name="ib_mezuniyet" value="Önlisans">Önlisans</option>
                                                <option name="ib_mezuniyet" value="Lisans">Lisans</option>
                                                <option name="ib_mezuniyet" value="Yüksek">Yüksek Lisans</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Okul </label>
                                        <div class="col-md-9">
                                            <input class="form-control" type="text" name="ib_okul">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Bölüm </label>
                                        <div class="col-md-9">
                                            <input class="form-control" type="text" name="ib_bolum">
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <button type="button" class="btn btn-block btn-xs" style="background:#848484;color:white">
                                                <span>İş Deneyimlerinizi Doldurunuz! </span>
                                                <i class="fa fa-sort-amount-asc"></i>
                                            </button>
                                        </div>
                                        <div class="col-md-9"> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Deneyim-1</label>
                                        <div class="col-md-9">
                                            <input class="form-control" type="text" name="ib_deneyimbir" autocomplete="off" maxlength="60">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Deneyim-2</label>
                                        <div class="col-md-9">
                                            <input class="form-control" type="text" name="ib_deneyimiki" autocomplete="off" maxlength="60">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Deneyim-3</label>
                                        <div class="col-md-9">
                                            <input class="form-control" type="text" name="ib_deneyimuc" autocomplete="off" maxlength="60">
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <button type="button" class="btn btn-block btn-xs" style="background:#848484;color:white">
                                                <span>Notlar! </span>
                                                <i class="fa fa-sort-amount-asc"></i>
                                            </button>
                                        </div>
                                        <div class="col-md-9"> </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Talep Ettiğiniz Maaş</label>
                                        <div class="col-md-9 input-group bootstrap-touchspin">
                                            <span class="input-group-addon bootstrap-touchspin-prefix">₺</span>
                                            <input  class="form-control" type="number" name="ib_maas">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Eklemek İstedikleriniz</label>
                                        <div class="col-md-9">
                                            <textarea class="form-control" type="text" name="ib_notlar" autocomplete="off"> </textarea>
                                        </div>
                                    </div>

                                    <div class="row" style="float:right;margin-right: 1px">
                                        <button class="btn btn-success waves-effect waves-light">
                                            <input type="submit" value="Gönder" style="border:none;background:none;"></button>
                                    </div>
                                </form>
                            </div> <!-- panel-body -->
                        </div> <!-- panel -->
                    </div> <!-- col -->
                </div> <!-- End row -->
            </div> <!-- container -->




            <!----------------------------- galeri END--------------------------------------------------------------------->
        </div>
    </section>
    <!-- Page Content / End -->
@section('dahil_edilecek_js')
    <script>
        $(document).ready(function($) {
            /*Ehliyet varis Ekle Çıkar*/
            $('#ib_ehliyet').on('change', function() {
                if (this.value == 'Var'){
                    $('.ehliyet_turu').removeClass('hidden');
                }else if(this.value == 'Yok'){
                    $('.ehliyet_turu').addClass('hidden');
                }else{
                    $('.ehliyet_turu').addClass('hidden');
                }
            });

            /*Askerlik tecili Ekle Çıkar*/
            $('#ib_askerlik_durumu').on('change', function() {
                if (this.value == 'Tecilli'){
                    $('.son-tecil').removeClass('hidden');
                }else if(this.value == 'Var'){
                    $('.son-tecil').addClass('hidden');
                }else if(this.value == 'Var'){
                    $('.son-tecil').addClass('hidden');
                }else{
                    $('.son-tecil').addClass('hidden');
                }
            });
        });
    </script>
    @endsection
@endsection

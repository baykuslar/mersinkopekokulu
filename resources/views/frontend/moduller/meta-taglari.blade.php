
<!-- Meta Tagları START ================================================== -->
<meta charset="utf-8">
<meta name="content-language" content="TR-TR" />
<meta name="language" content="TR" />
<meta http-equiv="content-style-type" content="text/css" />
<meta name="description" content="@if( !empty($description) ) {{$description}} @else Köpek eğitimleri bu alanda ülke çapında isim yapmış, gösteri ve showlarda yer almış, yarışmalara
                    katılmış ve yurtdışında bir çok eğitim, seminer ve kurslara katıl olan Rasim Tekin ve ekibi tarafından profesyonel ve pozitif sisteme dayalı
                    olarak verilmektedir. Eğitimlerimiz 1500m2'lik çim sahada yapılmaktadır. Gerektiğinde ve talep olduğunda müşterilerimiz de bizzat gelerek
                    eğitimleri izleyebilmekte ve iştirak edebilmektedir. @endif"/>
<meta name="keywords" content="@if( !empty($metakw) ) {{$metakw}} @else  mersin köpek okulu, köpek okulu, mersin köpek okulu, rasim tekin, köpek eğitimi, mersin köpek eğitimi,
                     sosyalleşme eğitimi, köpek oteli, mersin köpek oteli, yaşam alanı, mersin köpek yaşam alanı, eğitim merkezi, referanslarımız, köpeklerimiz,
                      video servisimiz, çiftliğimiz, köpek çiftliğimiz, dinlenme alanımız, mersin köpek oteli ve pansiyonu, mersin köpek okulu iletişim, @endif"/>
<meta name="abstract" content="Köpek eğitimleri bu alanda ülke çapında isim yapmış, gösteri ve showlarda yer almış,
                      yarışmalara katılmış ve yurtdışında bir çok eğitim, seminer ve kurslara katıl olan Rasim Tekin ve ekibi tarafından profesyonel
                      ve pozitif sisteme dayalı olarak vereilmektedir. Eğitimlerimiz 1500m2'lik çim sahada yapılmaktadır.
                      Gerektiğinde ve talep olduğunda müşterilerimiz de bizzat gelerek eğitimleri izleyebilmekte ve iştirak edebilmektedir." />
<meta name="title" content="Grafik Tasarım, Web Tasarım, Yazılım Hizmetleri, Bc Tasarım" />
<meta name="revisit-after" content="3 days" />
<meta name="googlebot" content="index, follow, noarchive" />
<meta name="robots" content="index, follow, noarchive" />
<meta name="google" content="notranslate" />
<meta name="google-site-verification" content="Buraya Google ın Verdiği Kodu Giriniz." />
<meta name="copyright" content="Mars K9 Köpek Okulu ve Oteli Copyright © 2012. Tüm Hakları Saklıdır." />
<meta name="owner" content="Baykuş Creative" />
<meta name="author" content="Mersin Kopek Okulu, info@mersinkopekokulu.com" />
<meta name="designer" content="İrfan YILMAZ" />
<meta name="publisher" content="Baykuş Creative" />
<meta name="generator" content="laravel 5.5" />
<meta name="reply-to" content="baykuscreative@gmail.com" />
<title>{{$viewAyarlar->{'site_baslik_'.session('dil')} }} - @yield('title')</title>
<!-- Meta Tagları END-->

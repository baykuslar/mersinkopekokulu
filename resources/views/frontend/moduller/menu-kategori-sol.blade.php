<div class="header-bottom">
    <div class="container">
        <div class="row align-items-center">
            <!-- Category Nav Start -->
            <div class="col-lg-3 col-md-6 col-sm-5 order-md-1 order-sm-1 order-2">
                <div class="category-nav-wrapper">
                    <div class="category-nav bg-orange">
                        <h2 class="category-nav__title primary-bg" id="js-cat-nav-title"><i class="fa fa-bars"></i>
                            <span>@lang('genel.egitimlerimiz')</span></h2>
                        <ul class="category-nav__menu" id="js-cat-nav">
                            @foreach($viewEgitimlerimiz as $egitim)
                            <li class="category-nav__menu__item {{--has-children--}}">
                                <a href="{{--javascript:void(0)--}}/mersin-kopek/{{$egitim->slug}}">{{$egitim->{'baslik_'. session('dil')} }}</a>
                                <div class="category-nav__submenu mega-menu three-column">
                                    <div class="category-nav__submenu--inner">
                                        <h3 class="category-nav__submenu__title">
                                            <a href="/mersin-kopek/{{$egitim->slug}}">{{$egitim->{'baslik_'. session('dil')} }}</a>
                                        </h3>
                                        <ul>
                                            <li>
                                                <a href="/mersin-kopek/{{$egitim->slug}}">
                                                    @if($egitim->resim == null)
                                                        <img src="/uploads/egitimlerimiz/default.jpg" alt="{{$egitim->{'baslik_'. session('dil')} }}" class="img-responsive menu-s">
                                                    @else
                                                        <img src="/uploads/egitimlerimiz/{{$egitim->resim}}" alt="{{$egitim->{'baslik_'. session('dil')} }}" class="img-responsive menu-s">
                                                    @endif
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                            @endforeach
                                <li class="category-nav__menu__item">
                                    <a href="/egitimlerimiz">@lang('genel.egitimlerimiz')</a>
                                </li>
                        </ul>
                    </div>
                </div>
                <!-- Category Nav End -->
            </div>
            <!-- Category Widget -->
            <div class="col-lg-5 col-md-6 order-md-2 order-sm-3 order-1">
                <form class="category-widget" action="{{route('arama_sonuc')}}" method="post">
                    {{csrf_field()}}
                    <input type="search" name="aranan" placeholder="@lang('genel.aramayapiniz')"  value="{{ old('aranan') }}" >
                    <button type="submit" class="search-submit"><i class="fas fa-search"></i> @lang('genel.aramayap')</button>
                </form>
            </div>
            <!-- Call Widget -->
            <div class="col-lg-4 col-md-12 col-sm-7 order-md-3 order-sm-2 order-3 hidden-xs hidden-sm">
                <div class="call-widget">
                    <p>@lang('genel.bizeulasin') : <i class="icon ion-ios-telephone"></i>
                        <span class="font-weight-mid" style="font-size: 17px">{{$viewAyarlar->gsm}}</span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

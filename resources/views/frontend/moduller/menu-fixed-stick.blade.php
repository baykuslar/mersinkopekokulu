<div class="fixed-header sticky-init sticky-color">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-3">
                <!-- Sticky Logo Start -->
                <a class="sticky-logo" href="/">
                    @if (\App::getLocale() == 'tr')
                        <img src="/uploads/logo/mars-logo-sticky_tr.png">
                    @else
                        <img src="/uploads/logo/mars-logo-sticky_en.png">
                    @endif
                </a>
                <!-- Sticky Logo End -->
            </div>
            <div class="col-lg-9">
                <!-- Sticky Mainmenu Start -->
                <nav class="sticky-navigation white-color">
                    <ul class="mainmenu sticky-menu">
                        <li class="mainmenu__item ">
                            <a href="/" class="mainmenu__link">@lang('genel.anasayfa')</a>
                        </li>
                        <li class="mainmenu__item menu-item-has-children">
                            <a href="#" class="mainmenu__link">@lang('genel.kurumsal')</a>
                            <ul class="sub-menu">
                                <li>
                                    <a href="/hakkimizda">@lang('genel.hakkimizda')</a>
                                </li>
                               {{-- <li>
                                    <a href="/ekibimiz">@lang('genel.ekibimiz')</a>
                                </li>--}}
                                {{--<li>
                                    <a href="/insan-kaynaklari">@lang('genel.insankaynaklari')</a>
                                </li>--}}
                            </ul>
                        </li>
                        <li class="mainmenu__item menu-item-has-children">
                            <a href="#" class="mainmenu__link">@lang('genel.galeri')</a>
                            <ul class="sub-menu">
                                <li>
                                    <a href="/resimgalerisi">@lang('genel.galeri')</a>
                                </li>
                                <li>
                                    <a href="/videogalerisi">@lang('genel.videolar')</a>
                                </li>
                            </ul>
                        </li>
                        <li class="mainmenu__item ">
                            <a href="/sssorular" class="mainmenu__link">@lang('genel.sss')</a>
                        </li>
                        <li class="mainmenu__item ">
                            <a href="/blog" class="mainmenu__link">@lang('genel.blog')</a>
                        </li>
                        <li class="mainmenu__item ">
                            <a href="/referanslar" class="mainmenu__link">@lang('genel.referanslar')</a>
                        </li>
                        <li class="mainmenu__item ">
                            <a href="/iletisim" class="mainmenu__link">@lang('genel.iletisim')</a>
                        </li>
                    </ul>
                    <div class="sticky-mobile-menu  d-lg-none">
                        <span class="sticky-menu-btn"></span>
                    </div>
                </nav>
                <!-- Sticky Mainmenu End -->
            </div>
        </div>
    </div>
</div>

<section class="hero-area-two">
    <div class="container">
        <div class="row">
            <div class="col-xl-9 col-lg-8 col-xs-12 col-sm-12">
                <div class=" petmark-slick-slider  home-slider" data-slick-setting='{"autoplay": true,"autoplaySpeed": 8000,"slidesToShow": 1,"dots": true}'>
                    @foreach($sliderler as $slider)
                    <div class="single-slider home-content bg-image responsive-img" data-bg="/uploads/slider/{{$slider->resim}}">
                        <div class="container hidden-xs">
                            <div class="row">
                                <div class="col-lg-10">
                                    <h2 class="sh hidden-xs"> <span class="text-primary"></span> {{$slider->{'ozel_baslik_'.session('dil')} }}</h2>
                                    <h4 class="mt--30 sh">{{$slider->{'baslik_'.session('dil')} }}</h4>
                                </div>
                            </div>
                        </div>
                        <span class="herobanner-progress"></span>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-5 pt--50 pt-md-0 hidden-xs">
                <a class="promo-image overflow-image mb-0">
                    <img src="/frontend/petmark/image/product/hero-promo-product.jpg" alt="">
                </a>
            </div>
        </div>
    </div>
</section>

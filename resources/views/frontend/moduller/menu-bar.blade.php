<div class="header-middle bg-primary">
    <div class="header-nav-wrapper">
        <div class="container">
            <div class="row align-items-center">
                <!-- Template Logo -->
                <div class="col-lg-3 col-md-4 col-sm-5 col-6">
                    <div class="site-brand  text-center text-lg-left">
                        <a href="/" class="brand-image">
                            @if (\App::getLocale() == 'tr')
                                <img src="/uploads/logo/mars-logo_tr.png">
                            @else
                                <img src="/uploads/logo/mars-logo_en.png">
                            @endif
                        </a>
                    </div>
                </div>
                <!-- Main Menu -->
                <div class="col-lg-9 d-none d-lg-block">
                    <nav class="main-navigation white-color">
                        <!-- Mainmenu Start -->
                        <ul class="mainmenu">
                            <li class="mainmenu__item ">
                                <a href="/" class="mainmenu__link">@lang('genel.anasayfa')</a>
                            </li>
                            <li class="mainmenu__item menu-item-has-children">
                                <a href="#" class="mainmenu__link">@lang('genel.kurumsal')</a>
                                <ul class="sub-menu">
                                    <li>
                                        <a href="/hakkimizda">@lang('genel.hakkimizda')</a>
                                    </li>
                                    {{--<li>
                                        <a href="/ekibimiz">@lang('genel.ekibimiz')</a>
                                    </li>--}}
                                   {{-- <li>
                                        <a href="/insan-kaynaklari">@lang('genel.insankaynaklari')</a>
                                    </li>--}}
                                </ul>
                            </li>
                            <li class="mainmenu__item menu-item-has-children">
                                <a href="#" class="mainmenu__link">@lang('genel.galeri')</a>
                                <ul class="sub-menu">
                                    <li>
                                        <a href="/resimgalerisi">@lang('genel.galeri')</a>
                                    </li>
                                    <li>
                                        <a href="/videogalerisi">@lang('genel.videolar')</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="mainmenu__item ">
                                <a href="/sssorular" class="mainmenu__link">@lang('genel.sss')</a>
                            </li>
                            <li class="mainmenu__item ">
                                <a href="/blog" class="mainmenu__link">@lang('genel.blog')</a>
                            </li>
                            <li class="mainmenu__item ">
                                <a href="/referanslar" class="mainmenu__link">@lang('genel.referanslar')</a>
                            </li>
                            <li class="mainmenu__item ">
                                <a href="/iletisim" class="mainmenu__link">@lang('genel.iletisim')</a>
                            </li>
                        </ul>
                        <!-- Mainmenu End -->
                    </nav>
                </div>
                <!--  Mobile Menu -->
                <div class="col-6 d-flex d-lg-none order-2 mobile-absolute-menu">
                    <!-- Main Mobile Menu Start -->
                    <div class="mobile-menu"></div>
                    <!-- Main Mobile Menu End -->
                </div>
            </div>
        </div>
    </div>
</div>
@include('frontend.moduller.menu-kategori-sol')
@include('frontend.moduller.menu-fixed-stick')

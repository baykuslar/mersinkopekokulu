
{{--<div class="wc-style10">
<!-- Button Style 6 -->
<a class="wc-list wc-button6" data-number="919099153528" data-message="Please help me! I have got wrong product - ORDER ID is : #654321485" >
    <i class="fa fa-whatsapp"></i>
</a>
</div>--}}
<!-- WhatsChat Container -->

<!-- Start Style & Color Setting Tab (This is only for Live Demo) -->

<!-- Start Page Container -->
<div class="container">
    <!-- WhatsChat Container -->
    <div class="wc-style1 wc-right-bottom">

        <!-- Start Floating Panel Container -->
        <div class="wc-panel">
            <!-- Panel Header -->
            <div class="wc-header" style="    padding: 12px 5px;">
                <i style="font-size: 35px;
    width: 50px;
    padding-top: 5px;float: left" class="fa fa-whatsapp" aria-hidden="true"></i>
                <div>

                    <strong> @lang('genel.chatbaslik') </strong>
                    <h6 style="color: white"> @lang('genel.whatsapsohbet')</h6>
                </div>
            </div>

            <!-- Panel Content -->
            <div class="wc-body">
                <ul>
                    <!-- Start Single Contact List -->
                    @foreach($viewWhatsapptemsilciler as $temsilci)
                        @if($temsilci->aktif_pasif=='on')
                            <li class="wc-list hoveri" data-number="{{$temsilci->whatsapp}}" target="_blank" data-message="">
                                <div class="d-flex  bd-highlight">
                                    <!-- Profile Picture -->
                                    <div class="wc-img-cont">
                                        <img class="wc-user-img" src="/uploads/whatsapptemsilciler/{{$temsilci->resim}}" alt="{{$temsilci->adi_soyadi}}" />
                                         @if($temsilci->durum=='Çevirimiçi')
                                        <span style="background-color: green;border: 1.5px solid lawngreen;" class="wc-status-icon wc-offline"></span>
                                          @elseif($temsilci->durum=='Meşgul')
                                        <span style="background-color: yellow;border: 1.5px solid orange;" class="wc-status-icon wc-offline"></span>
                                          @elseif($temsilci->durum=='Çevirimdışı')
                                        <span style="background-color: red;border: 1.5px solid darkred;" class="wc-status-icon wc-offline"></span>
                                          @endif
                                    </div>
                                    <!-- Display Name & Last Seen -->
                                    <div class="wc-user-info">
                                        <span>{{$temsilci->adi_soyadi}}</span>
                                        <p>{{$temsilci->{'unvan_' . session('dil')} }}</p>
                                    </div>
                                </div>
                            </li>
                        @endif
                     @endforeach
                    <!--/ End Single Contact List -->
                </ul>
            </div>
        </div>
        <!--/ End Floating Panel Container -->

        <!-- Start Right Floating Button-->
        <div class="wc-button wc-right-bottom">
            <i class="fa fa-whatsapp" aria-hidden="true"></i>
        </div>
        <!--/ End Right Floating Button-->

    </div>
</div>
<!--/ End Page Container -->
<style>
    .wc-style1 .wc-button .fa-whatsapp, .wc-style1 .wc-button .fa-whatsapp:hover {
        -webkit-transition: all 0.5s ease;
        -moz-transition: all 0.5s ease;
        -ms-transition: all 0.5s ease;
        -o-transition: all 0.5s ease;
        transition: all 0.5s ease;
    }
    .wc-style1 .wc-button .fa-whatsapp:hover {
        -webkit-transform: rotate(270deg);
        -moz-transition: rotate(270deg);
        -ms-transition: rotate(270deg);
        -o-transition: rotate(270deg);
        transform: rotate(270deg);
        box-shadow: 0 0 1px 15px rgba(64, 62, 62, 0.14), 0 0 1px 30px rgba(138, 59, 88, 0.1), 0 0 1px 45px rgba(138, 59, 88, 0.1);
    }
    .wc-style1 .wc-button .fa-whatsapp {
        position: relative;
        padding: 13px;
        width: 50px;
        height: 50px;
        font-size: 25px;
        text-align: center;
        line-height: 0.8;
        border-radius: 50%;
        z-index: 1;
    }
</style>

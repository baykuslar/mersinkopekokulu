<!-- Footer Alanı -->
<footer class="site-footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="single-footer contact-block">
                    <h3 class="footer-title">@lang('genel.iletisim')</h3>
                    <div class="single-footer-content">
                        <p class="text-italic"> {{ $viewAyarlar->{'site_aciklama_'.session('dil')} }}</p>
                        <ul class="footer-list">
                            <li><a href="tel://+9{{$viewAyarlar->gsm}}">{{$viewAyarlar->gsm}}</a></li>
                            <li><a href="mailto:{{$viewAyarlar->email}}">{{$viewAyarlar->email}}</a></li>
                            <li><a href="mailto:{{$viewAyarlar->email_2}}">{{$viewAyarlar->email_2}}</a></li>
                            <li><a href="#">{{$viewAyarlar->adres}}</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-sm-6">
                <div class="single-footer contact-block">
                    <h3 class="footer-title">@lang('genel.calismasaatleri')</h3>
                    <div class="single-footer-content">
                        <p style="margin-bottom: 4px;">@lang('genel.hartaici') : <b>{{ $viewAyarlar->calisma_saatleri_baslangic }} @lang('genel.ile') {{ $viewAyarlar->calisma_saatleri_bitis}}</b></p>
                        <p style="margin-bottom: 4px;">
                            @lang('genel.cumartesi'):  <b>
                                @if($viewAyarlar->cumartesi=='Kapalı')
                                    @lang('genel.kapali')
                                @else
                                    {{ $viewAyarlar->cumartesi_baslangic }} @lang('genel.ile') {{ $viewAyarlar->cumartesi_bitis}}
                                @endif
                            </b>
                        </p>
                        <p style="margin-bottom: 4px;">
                            @lang('genel.pazar') :
                            <b>
                                @if($viewAyarlar->pazar=='Kapalı')
                                    @lang('genel.kapali')
                                @else
                                    {{ $viewAyarlar->pazar_baslangic }} @lang('genel.ile') {{ $viewAyarlar->pazar_bitis}}
                                @endif
                            </b>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-sm-6 hidden-xs">
                <div class="single-footer contact-block">
                    <h3 class="footer-title">@lang('genel.kisalink')</h3>
                    <div class="single-footer-content">
                        <ul class="footer-list">
                            <li><a href="/">@lang('genel.anasayfa')</a></li>
                            <li><a href="/hakkimizda">@lang('genel.hakkimizda')</a></li>
                            <li><a href="/resimgalerisi">@lang('genel.galeri')</a></li>
                            <li><a href="/sssorular">@lang('genel.sss')</a></li>
                            <li><a href="/blog">@lang('genel.blog')</a></li>
                            <li><a href="/egitimlerimiz">@lang('genel.egitimlerimiz')</a></li>
                            <li><a href="#">Gizlilik Politikası</a></li>
                            <li><a href="#">Sözleşme Örneği</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single-footer contact-block">
                    <h3 class="footer-title">@lang('genel.bizeaboneol')</h3>
                    <div class="single-footer-content">
                        <p>
                            @lang('genel.bizeaboneolyazi')
                        </p>
                        <div class="widget-content">
                            <ul>
                                <li><a href="https://www.mersinkopekokulu.com">www.mersinkopekokulu.com</a></li>
                                <li><a href="https://www.mersinkopekegitim.com">www.mersinkopekegitim.com</a></li>
                                <li><a href="https://www.mersinkopekoteli.com">www.mersinkopekmamasi.com</a></li>
                            </ul>
                        </div>
                        <div class="pt-2">
                            <div class="input-box-with-icon">
                                <input type="text" placeholder="@lang('genel.mailgirin')">
                                <button><i class="fas fa-envelope"></i></button>
                            </div>
                        </div>
                        <p class="social-icons" style="margin-top: 16px">
                            @if($viewAyarlar->facebook != null or !empty($viewAyarlar->facebook) or $viewAyarlar->facebook !="")
                                <a href="{{$viewAyarlar->facebook}}"><i class="fab fa-facebook-f"></i></a>
                            @endif
                            @if($viewAyarlar->twitter != null or !empty($viewAyarlar->twitter) or $viewAyarlar->twitter !="")
                                <a href="{{$viewAyarlar->twitter}}"><i class="fab fa-twitter"></i></a>
                            @endif
                            @if($viewAyarlar->instagram!= null or !empty($viewAyarlar->instagram) or $viewAyarlar->instagram !="")
                                <a href="{{$viewAyarlar->instagram}}"><i class="fab fa-instagram"></i></a>
                            @endif
                            @if($viewAyarlar->youtube != null or !empty($viewAyarlar->youtube) or $viewAyarlar->youtube !="")
                                <a href="{{$viewAyarlar->youtube}}"><i class="fab fa-youtube"></i></a>
                            @endif
                            @if($viewAyarlar->pinterest != null or !empty($viewAyarlar->pinterest) or $viewAyarlar->pinterest !="")
                                <a href="{{$viewAyarlar->pinterest}}"><i class="fab fa-pinterest"></i></a>
                            @endif
                            @if($viewAyarlar->tumblr != null or !empty($viewAyarlar->tumblr) or $viewAyarlar->tumblr !="")
                                <a href="{{$viewAyarlar->tumblr}}"><i class="fab fa-tumblr"></i></a>
                            @endif
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-block-2 text-center hidden-xs">
            <ul class="footer-list list-inline justify-content-center">
                <li><a href="">@lang('etiketler.kopekokulu')</a></li>
                <li><a href="">@lang('etiketler.kopekotel')</a></li>
                <li><a href="">@lang('etiketler.kopekotel')</a></li>
                <li><a href="">@lang('etiketler.marsk9')</a></li>
                <li><a href="">@lang('etiketler.mersinegitim')</a></li>
                <li><a href="">@lang('etiketler.kopekegi')</a></li>
                <li><a href=""> @lang('etiketler.uzmanke')</a></li>
            </ul>
            <ul class="footer-list list-inline justify-content-center">
                <li><a href="">@lang('etiketler.kopekotel')</a></li>
                <li><a href="">@lang('etiketler.kopek')</a></li>
                <li><a href="">@lang('etiketler.okul')</a></li>
                <li><a href="">@lang('etiketler.otel')</a></li>
                <li><a href="">@lang('etiketler.egitim')</a></li>
                <li><a href="">@lang('etiketler.uzman')</a></li>
                <li><a href="">@lang('etiketler.baribak')</a></li>
                <li><a href=""> @lang('etiketler.egitimmerkezi')</a></li>
                <li><a href="">@lang('etiketler.rasim')</a></li>
                <li><a href="">@lang('etiketler.tekin')</a></li>
                <li><a href="">@lang('etiketler.mars')</a></li>
                <li><a href="">@lang('etiketler.k9')</a></li>
            </ul>
    {{--        <div class="payment-block pt-3">
                <img src="/frontend/petmark/image/icon-logo/payment-icons.png" alt="">
            </div>--}}
        </div>
    </div>
    <div class="footer-copyright">
        <p>{{ $viewAyarlar->{'site_baslik_'.session('dil')} }} Copyright © 2012 - <?php echo date('Y'); ?>.  @lang('genel.copyright') <a class="f-o" href="https://baykuscreative.com/" target="_blank">BAYKUSCREATIVE </a></p>
    </div>
</footer>

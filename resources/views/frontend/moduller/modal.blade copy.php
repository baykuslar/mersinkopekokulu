

@if($viewGirismodal->aktif_pasif == 'on')
    <!-- Modal -->
    <div class="modal fade modal-quick-view" id="quickModal" tabindex="-1" role="dialog" aria-labelledby="quickModal"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="pm-product-details">
                    <button type="button" class="close ml-auto" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="row modal-body">
                        <!-- Blog Details Image Block -->
                        <div class="col-md-6">
                            <div class="image-block">
                                <!-- Zoomable IMage -->
                                <img class="img-responsive" src="/uploads/ozel-alan/{{$viewGirismodal->resim}}"
                                     data-zoom-image="/uploads/ozel-alan/{{$viewGirismodal->resim}}" alt=""/>
                            </div>
                        </div>

                        <div class="col-md-6 mt-4 mt-lg-0">
                            <div class="description-block">
                                <div class="header-block">
                                    <h3> {{$viewGirismodal->baslik}}</h3>
                                </div>
                                <!-- Price -->
                                <p class="price"><span class="old-price">250$</span>300$</p>
                                <!-- Rating Block -->
                                <div class="rating-block d-flex  mt--10 mb--15">
                                    <p class="rating-text"><a href="#comment-form">See all features</a></p>
                                </div>
                                <!-- Blog Short Description -->
                                <div class="product-short-para">
                                    <p>
                                        {{$viewGirismodal->aciklamasi}}
                                    </p>
                                </div>
                                <div class="status">
                                    <i class="fas fa-check-circle"></i>300 IN STOCK
                                </div>
                                <!-- Amount and Add to cart -->
                                <form action="./" class="add-to-cart">
                                    <div class="count-input-block">
                                        <input type="number" class="form-control text-center" value="1">
                                    </div>
                                    <div class="btn-block">
                                        <a href="#" class="btn btn-rounded btn-outlined--primary">Sepete Ekle</a>
                                    </div>
                                </form>
                                <!-- Sharing Block 2 -->
                                <div class="share-block-2 mt--30">
                                    <h4>SHARE THIS PRODUCT</h4>
                                    <ul class="social-btns social-btns-3">
                                        <li><a href="#" class="facebook"><i class="fab fa-facebook-f"></i></a></li>
                                        <li><a href="#" class="twitter"><i class="fab fa-twitter"></i></a></li>
                                        <li><a href="#" class="google"><i class="fab fa-google-plus-g"></i></a></li>
                                        <li><a href="#" class="pinterest"><i class="fab fa-pinterest-p"></i></a></li>
                                        <li><a href="#" class="linkedin"><i class="fab fa-linkedin-in"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="/blog-sayfasi/1"><button class="btn btn-btn btn-success"> AYRINTILI İNCELEYİNİZ!</button> </a>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">KAPAT</button>
                </div>
            </div>
        </div>
    </div>
@else
@endif



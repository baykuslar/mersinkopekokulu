
@if($viewGirismodal->aktif_pasif == 'on')
    <!-- Modal -->
    <div class="modal fade modal-quick-view" id="quickModal" tabindex="-1" role="dialog" aria-labelledby="quickModal" data-backdrop="static" data-keyboard="false"
         aria-hidden="true">
        <div class="modal-dialog" role="document" style="max-width: 452px">
            <div class="modal-content">
<!--                <iframe class="w-100" src="/uploads/video/drone-video.MP4" width="100%" height="1000px" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen  allow="autoplay" allowFullScreen></iframe>-->
                <video id="myVideo" width="100%" height="800px">
                    <source src="/uploads/video/drone-video.MP4" type="video/mp4" webkit-playsinline />
                </video>

                    <button type="button" id="mybutton" class="btn btn-success" style="position:absolute" data-dismiss="modal">KAPAT</button>

            </div>
        </div>
    </div>
@else
@endif
<style>
    .modal-dialog {
        width: 100%;
        height: 100%;
    }
</style>

<script>
    var video = document.querySelector('video');
    video.muted = true;
    video.audio = true;
    video.playsinline = true;
    video.autoplay = true;
    video.controls = true

    document.getElementById('mybody').addEventListener('click', function() {
        var video = document.querySelector('video');
        video.muted = false;
    });

    document.getElementById('mybutton').addEventListener('click', function() {
        var video = document.querySelector('video');
        video.muted = true;
        video.pause();
    });
</script>


<div class="header-top text-white bg-primary">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-4 text-center text-md-left hidden-xs">
                <p class="font-weight-300"><i class="fa fa-phone telSet"></i> <a class="telSet" href="tel://+9{{$viewAyarlar->gsm}}">{{$viewAyarlar->gsm}}</a></p>
            </div>
            <div class="col-md-8">
                <div class="header-top-nav white-color right-nav ">

                    <div class="nav-item slide-down-wrapper">

                        <a class="slide-down--btn" href="javascript:void(0);">
                            @if (\App::getLocale() == 'tr')
                               <img src="/uploads/lang-flag/tr.png" alt="@lang('genel.dil')">
                                @lang('genel.turkce')
                            @else
                                <img src="/uploads/lang-flag/en.png" alt="@lang('genel.dil')">
                                @lang('genel.ingilizce')
                            @endif
                            <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-list slide-down--item">
                            <li><a href="/dil?lang=tr"><img src="/uploads/lang-flag/tr.png" alt="Türkçe"> @lang('genel.turkce')</a></li>
                            <li><a href="/dil?lang=en"><img src="/uploads/lang-flag/en.png" alt="English"> @lang('genel.ingilizce')</a></li>
                        </ul>

                    </div>
                    <div class="nav-item slide-down-wrapper hidden-xs">
                        <span><a class="irfan" href="/kopeklerimiz">@lang('genel.ucretsizsahip')</a></span>
                        </a>
                    </div>
                    <div class="nav-item">
                        <i class="fa fa-user"></i> <a href="/admin"> 	@lang('genel.admingiris')</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

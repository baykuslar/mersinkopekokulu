@extends('frontend.master')
@section('title') {{$kayit->{'baslik_'. session('dil')} }} @endsection
@section('govde')

    <nav aria-label="breadcrumb" class="breadcrumb-wrapper">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">@lang('genel.anasayfa')</a></li>
                <li class="breadcrumb-item"><a href="/referanslar">@lang('genel.referanslar')</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{$kayit->{'baslik_'. session('dil')} }}</li>
            </ol>
        </div>
    </nav>
    
    <section class="blog-page-section with-sidebar">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-xl-9 mb--60 order-lg-2  left-slide-margin">
                    <div class="blog-post post-details single-block">
                        <a href="blog-details.html" class="blog-image">
                            @if($kayit->resim == null)
                                <img src="/uploads/referanslarimiz/default.jpg" alt="{{$kayit->{'baslik_'. session('dil')} }}" class="img-responsive">
                            @else
                                <img src="/uploads/referanslarimiz/{{$kayit->resim}}" alt="{{$kayit->{'baslik_'. session('dil')} }}" class="img-responsive">
                            @endif
                        </a>
                        <div class="blog-content mt--30">
                            <header>
                                <h3 class="blog-title text-primary"> {{$kayit->{'baslik_'. session('dil')} }}</h3>
                                <div class="post-meta">
                                        <span class="post-author">
                                            <i class="fas fa-user"></i>
                                            <span class="text-gray">@lang('genel.sahip') : </span>{{$viewAyarlar->{'site_baslik_'. session('dil')} }}</span>
                                    <span class="post-separator">|</span>
                                    <span class="post-date">
                                        <i class="far fa-calendar-alt"></i>
                                        <span class="text-gray">@lang('genel.guncelleme') : </span>
                                         {{\Carbon\Carbon::parse($kayit->created_at)->format('d/m/Y')}}
                                    </span>
                                    <span class="post-separator">|</span>
                                    <span class="post-date">
                                        <i class="far fa-eye"></i>
                                        <span class="text-gray">@lang('genel.guruntulemesayisi') : </span>
                                         {{$kayit->hit}}
                                    </span>
                                </div>
                            </header>
                            <article>
                                <p class="p-0"> {!! $kayit->{'aciklama_'. session('dil')}  !!}</p>
                            </article>
                        </div>
                    </div>

                </div>
                <div class="col-lg-4 col-xl-3 order-lg-1">
                    <div class="sidebar-widget">
                        <div class="single-sidebar">
                            <h2 class="sidebar-title">@lang('genel.digerreferans')</h2>
                            <ul class="sidebar-list">
                                @foreach($bloglar as $blok)
                                    <li class="@if( $blok->slug == $slug ) hover-baslik @endif "><a href="/referans/{{$blok->slug}}" class="kes-tek-sira"> {{$blok->{'baslik_'. session('dil')} }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="single-sidebar">
                            <a class="promo-image overflow-image">
                                <img src="image/product/slidebar-promo-product.jpg" alt="">
                            </a>
                        </div>
                        <div class="single-sidebar">
                            <h2 class="sidebar-title">@lang('genel.etiketler')</h2>
                            <ul class="sidebar-tag-list">
                                @foreach(explode(',', $kayit->meta_keywords) as $kelime)
                                    <li><a href="#"> {{$kelime}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

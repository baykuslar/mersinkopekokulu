@extends('frontend.master')
@section('title') @lang('genel.referanslar') @endsection
@section('govde')
	<nav aria-label="breadcrumb" class="breadcrumb-wrapper">
		<div class="container">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="index.html">@lang('genel.anasayfa')</a></li>
				<li class="breadcrumb-item active" aria-current="page">@lang('genel.referanslar')</li>
			</ol>
		</div>
	</nav>

	<!-- Main Wrapper Start -->
	<main id="content" class="main-content-wrapper overflow-hidden service-wrapper-2">
		<section class="service-section-1">
			<div class="container">
				<div class="row">
					@foreach($referanslarimiz as $referans)
					<div class="col-xl-3 col-md-6 mb--30">
						<div class="blog-post">
							<a href="/referans/{{$referans->slug}}" class="blog-image">
								<img src="/uploads/referanslarimiz/{{$referans->resim}}">
							</a>
							<div class="blog-content mt--15 text-center">
								<header>
									<h3 class="blog-title"> <a href="/referans/{{$referans->slug}}">{{$referans->{'baslik_'. session('dil')} }}</a></h3>
									<div class="post-meta">
										<span class="post-author">
										  <i class="fas fa-user"></i>
										  <span class="text-gray">@lang('genel.sahip') : </span>
										  {{$viewAyarlar->{'site_baslik_'.session('dil')} }}
										</span>
									</div>

									</header>
								<article>
									<p>
										{{$referans->{'aciklama_'. session('dil')} }}
									</p>
								</article>
								<div class="blog-btn pb--10 pt--10">
									<a href="/referans/{{$referans->slug}}" class="btn btn-rounded btn-outlined--primary">@lang('genel.devaminioku')</a>
								</div>
							</div>
						</div>
					</div>
					@endforeach
				</div>
			</div>
		</section>
	</main>

			<!-- Page Content -->
			<section class="page-content">
				<div class="container">

					<div class="spacer-lg"></div>

					<hr class="lg">

					<h3>REFERANSLARIMIZ - İLGİLİ FOTOĞRAFLAR</h3>

					<div class="prev-next-holder text-right">
						<a class="prev-btn" id="carousel-prev"><i class="fa fa-angle-left"></i></a>
						<a class="next-btn" id="carousel-next"><i class="fa fa-angle-right"></i></a>
					</div>

					<div class="owl-carousel-wrapper row">
						<div id="owl-carousel" class="owl-carousel">
						@foreach($galerireferanslarresimleri as $galerireferans)
							<div class="project-item">
								<div class="project-item-inner">
									<figure class="alignnone project-img">
										<img src="/frontend/images/galeri/{{$galerireferans->resim}}" width="276" height="200">
										<div class="overlay">
											<a href="/frontend/images/galeri/{{$galerireferans->resim}}" class="popup-link zoom"><i class="fa fa-search-plus"></i></a>
										</div>
									</figure>
									<div class="project-desc">
									  <h4 class="title"><a href="#">
											  @if($galerireferans->adi==null)
												  {{$galerireferans->id}}
											  @else
												  {{$galerireferans->adi}}
											  @endif
										  </a></h4>
									</div>
								</div>
						   </div>
						@endforeach
						</div>
					</div>

				</div>
			
			</section>
			<!-- Page Content / End -->
@endsection
@extends('frontend.master')
@section('title') @lang('genel.blog')  @endsection
@section('govde')
    @section('css_dahil_edilecek_stiller')
        <link href="/css/paginate.css" rel="stylesheet">
    @endsection
    <nav aria-label="breadcrumb" class="breadcrumb-wrapper">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">@lang('genel.anasayfa')</a></li>
                <li class="breadcrumb-item active" aria-current="page">@lang('genel.blog')</li>
            </ol>
        </div>
    </nav>

    <section class="blog-page-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <!--  -->
                    <div class="blog-posts-container">
                        @foreach($kayitlar as $kayit )
                        <div class="blog-post blog-style-list">
                            <div class="row">
                                <div class="col-lg-5 col-md-6">
                                    <a href="/blog/{{$kayit->slug}}" class="blog-image">
                                        @if($kayit->resim == null)
                                            <img src="/uploads/blog/default.jpg" alt="Duvar saati" class="img-responsive">
                                        @else
                                            <img src="/uploads/blog/{{$kayit->resim}}" alt="Duvar saati" class="img-responsive">
                                        @endif
                                    </a>
                                </div>
                                <div class="col-lg-7 col-md-6">
                                    <div class="blog-content mt--15 mt-md-0">
                                        <header>

                                            <h3 class="blog-title"> <a href="/blog/{{$kayit->slug}}">{{$kayit->{'baslik_'.session('dil')} }} </a></h3>
                                            <div class="post-meta">
                                                <span class="post-author">
                                                  <i class="fas fa-user"></i>
                                                  <span class="text-gray">@lang('genel.sahip') : </span>{{$viewAyarlar->{'site_baslik_'.session('dil')} }}</span>
                                                                                <span class="post-separator">|</span>
                                                                                <span class="post-date">
                                                  <i class="far fa-calendar-alt"></i>
                                                  <span class="text-gray">@lang('genel.guncelleme') : </span>
                                                  {{\Carbon\Carbon::parse($kayit->created_at)->format('d/m/Y')}}
                                                </span>
                                            </div>
                                        </header>
                                        <article>
                                            <h3 class="d-none sr-only">{{$kayit->{'baslik_'.session('dil')} }}</h3>
                                            <p class="kes-700-7">{{$kayit->{'aciklama_'.session('dil')} }}</p>
                                        </article>
                                        <footer class="blog-post-footer">
                                            <div class="blog-btn">
                                                <a href="/blog/{{$kayit->slug}}" class="btn btn-rounded btn-outlined--primary">@lang('genel.devaminioku')</a>
                                            </div>
                                        </footer>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach

                    </div>

                    <div class="mt--20">
                    <div class="row">
                        <div class="col col-xs-12">
                            <div class="pagination-wrapper">
                                {{ $kayitlar->links('frontend.moduller.paginate') }}
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@extends('frontend.master')
@section('title') {{$kayit->baslik}} @endsection
@section('govde')

    <nav aria-label="breadcrumb" class="breadcrumb-wrapper">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">@lang('genel.anasayfa')</a></li>
                <li class="breadcrumb-item"><a href="/blog">@lang('genel.blog')</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{$kayit->{'baslik_'.session('dil')} }}</li>
            </ol>
        </div>
    </nav>
    
    <section class="blog-page-section with-sidebar">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-xl-9 mb--60 order-lg-2  left-slide-margin">
                    <div class="blog-post post-details single-block">
                        <a href="blog-details.html" class="blog-image">
                            @if($kayit->resim == null)
                                <img src="/uploads/blog/default.jpg" alt="{{$kayit->{'baslik_'.session('dil')} }}" class="img-responsive">
                            @else
                                <img src="/uploads/blog/{{$kayit->resim}}" alt="{{$kayit->{'baslik_'.session('dil')} }}" class="img-responsive">
                            @endif
                        </a>
                        <div class="blog-content mt--30">
                            <header>
                                <h3 class="blog-title text-primary"> {{$kayit->{'baslik_'.session('dil')} }}</h3>
                                <div class="post-meta">
                                        <span class="post-author">
                                            <i class="fas fa-user"></i>
                                            <span class="text-gray">@lang('genel.sahip') : </span>{{$viewAyarlar->{'site_baslik_'. session('dil')} }}</span>
                                    <span class="post-separator">|</span>
                                    <span class="post-date">
                                        <i class="far fa-calendar-alt"></i>
                                        <span class="text-gray">@lang('genel.guncelleme') : </span>
                                         {{\Carbon\Carbon::parse($kayit->created_at)->format('d/m/Y')}}
                                    </span>
                                    <span class="post-separator">|</span>
                                    <span class="post-date">
                                        <i class="far fa-eye"></i>
                                        <span class="text-gray">@lang('genel.guruntulemesayisi') : </span>
                                         {{$kayit->hit}}
                                    </span>
                                </div>
                            </header>
                            <article>
                                <p class="p-0"> {!! $kayit->{'aciklama_'.session('dil')} !!}</p>
                            </article>
                        </div>
                    </div>
                    <div class="comment-block-wrapper single-block">
                        <h3>@lang('genel.yorumlar')</h3>
                        @foreach($yorumlar as $yorum)
                        <div class="single-comment">
                            <div class="comment-avatar">
                                <p class="introduction">
                                    {{$yorum->adi_soyadi}}
                                </p>
                            </div>
                            <div class="comment-text">
                                <h5 class="author"> {{$yorum->adi_soyadi}}  <a class=" reply-btn" style="padding: 5px 15px"> {{\Carbon\Carbon::parse($yorum->created_at)->format('d/m/Y - H.i.s')}}</a></h5>
                                <p>{{$yorum->yorum}}</p>
                            </div>

                        </div>
                         @endforeach
                    </div>

                    <div class="replay-form-wrapper single-block">
                        <h3 class="mt-0">@lang('genel.cevapbirak')</h3>
                        <p>@lang('genel.epostagizli')*</p>
                        <form  onsubmit="return formuGonder();" name="frm" role="form"  class="blog-form">
                            <div class="row">
                                {{csrf_field()}}
                                <input type="hidden" id="blog_id" name="blog_id" value="{{$kayit->id}}">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="yorum">Yorumunuz</label>
                                        <textarea name="yorum" id="yorum" cols="30" rows="5" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="adi_soyadi">Adınız Soyadınız *</label>
                                        <input type="text" name="adi_soyadi" id="adi_soyadi" class="form-control">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="email">E-mail Adresiniz *</label>
                                        <input type="text" name="email" id="email" class="form-control">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div id="dv-captha" class="g-recaptcha"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="submit" data-method="confirm" data-loading-text="Lütfen Bekleyiniz!" class="btn btn-black">Yorum Gönder</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
                <div class="col-lg-4 col-xl-3 order-lg-1">
                    <div class="sidebar-widget">
                        <div class="single-sidebar">
                            <h2 class="sidebar-title">@lang('genel.digeryazi')</h2>
                            <ul class="sidebar-list">
                                @foreach($bloglar as $blok)
                                    <li class="@if( $blok->slug == $slug ) hover-baslik @endif ">
                                        <a href="/blog/{{$blok->slug}}" class="kes-tek-sira"> {{$blok->{'baslik_'.session('dil')} }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="single-sidebar">
                            <a class="promo-image overflow-image">
                                @if($kayit->resim == null)
                                    <img src="/uploads/blog/default.jpg" alt="{{$kayit->{'baslik_'.session('dil')} }}" class="img-responsive">
                                @else
                                    <img src="/uploads/blog/{{$kayit->resim}}" alt="{{$kayit->{'baslik_'.session('dil')} }}" class="img-responsive">
                                @endif
                            </a>
                        </div>
                        <div class="single-sidebar">
                            <h2 class="sidebar-title">@lang('genel.etiketler')</h2>
                            <ul class="sidebar-tag-list">
                                @foreach(explode(',', $kayit->meta_keywords) as $kelime)
                                    <li><a href="#"> {{$kelime}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('dahil_edilecek_js')
    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>
    <script language="javascript">
        _vToken = '';
        var onloadCallback = function() {
            grecaptcha.render('dv-captha', {
                'sitekey' : '{{env('reCAPTCHA_SITE_ANAHTARI')}}',
                'callback' : function(token){
                    _vToken = token;
                    console.log(_vToken);
                },
                'expired-callback' : function(e){
                    _vToken = '';
                },
                'error-callback' : function(e){
                    _vToken = '';
                }
            });
        };
        function kontrol(){
            if(frm.adi_soyadi.value == "")
            {
                sweetAlert("Eksik Bilgi!", "Ad Soyad alanını boş bırakmayınız.", "error");
                frm.adi_soyadi.focus();
                return false;
            }

            if(frm.email.value == "")
            {
                sweetAlert("Eksik Bilgi!", "E-Mail Adres alanını boş bırakmayınız.", "error");
                frm.email.focus();
                return false;
            }

            if(frm.yorum.value == "")
            {
                sweetAlert("Eksik Bilgi!", "Yorum alanını boş bırakmayınız.", "error");
                frm.yorum.focus();
                return false;
            }

            return true;
        }
        function formuGonder() {
            if( _vToken == '' ) {
                sweetAlert("Kayıt Başarısız", "Lütfen robot olmadığınızı doğrulayın.", "error");
                return false;
            }
            if (kontrol()) {
                $.ajax({ method: 'POST', url: '/yorum-gonder', data: $(document.frm).serialize() })
                    .done(function() {
                        swal({ title: "Kayıt Tamamlandı", text: "Kaydınız başarıyla tamamlanmıştır. Size en kısa sürede ulaşılacaktır.", type: "success" }, function() {
                            window.location.reload();
                        });
                    })
                    .fail(function(e) {
                        sweetAlert("Kayıt Başarısız", "Kayıt işlemi bir nedenden dolayı tamamlanamadı.", "error");
                    })
            }
            return false;
        }
    </script>
@endsection

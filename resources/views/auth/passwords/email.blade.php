<!DOCTYPE html>
<html style="background: none !important;">
<head>
    <meta charset="utf-8">
    <title>BaykusCreative | AdminPanel</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="Preview page of Metronic Admin Theme #1 for " name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="/backend/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="/backend/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="/backend/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/backend/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="/backend/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="/backend/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="/backend/assets/pages/css/login.min.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="favicon.ico" />
    <!-- Settings (Remove it on Production) -->
    <link href="/backend/assets/login/plugins/settings/jquery.colorpanel.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/eklentiler/bootstrap-toastr/toastr.min.css"/>
</head>
<body class=" login">
<!-- BEGIN LOGO -->
<div class="logo">
    <a href="index.html">
        <img src="/backend/assets/pages/img/logo-big.png" alt="" /> </a>
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content" style="border-radius:10px !important;">
    <!-- BEGIN LOGIN FORM -->
    <!-- END LOGIN FORM -->
    <!-- BEGIN FORGOT PASSWORD FORM -->
    <form role="form" method="POST" action="#" id="sifre-formu">
        <h3 class="font-green">Parolanızı mı unuttunuz ?</h3>
        <p> Şifrenizi sıfırlamak için e-posta adresinizi aşağıya girin. </p>
        <div class="form-group">
            <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" id="email" />
        </div>
        <div class="form-actions">
            <input type="hidden" value="{{csrf_token()}}" id="jeton" name="jeton">
            <button type="button" onclick="window.location.href='/admin' " class="btn green btn-outline">Geri</button>
            <button type="submit" class="btn btn-success uppercase pull-right">Gönder</button>
        </div>
    </form>
</div>
<div style="width: 400px;overflow: hidden;position: relative;margin:auto">
    <div class="copyright" onclick="window.open('http://www.baykuscreative.com/')"> <?php echo date('Y'); ?> ©   BaykusCreative </div>
</div>

<!-- BEGIN CORE PLUGINS -->
<script src="/backend/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="/backend/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="/backend/assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="/backend/assets/pages/scripts/login.min.js" type="text/javascript"></script>

<script src="/eklentiler/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function($) {
        $('#sifre-formu').on('submit', function (e) {
            e.preventDefault();
            var email = $('#email').val();
            var jeton = $('#jeton').val();
            if( email.length < 9 ) {
                toastr['error']('E-mail adresiniz eksik veya hatalıdır!', 'Eksik Bilgi!');
                return false;
            }

            $.ajax({
                url : '/password/email',
                type : 'POST',
                data : { email : email, _token:jeton },
                success : function(cevap){
                    if( cevap.giris !== false ){
                        window.location.href ='/';
                        toastr['success']('Şifre değiştirme linki kayıtlı mail adresinize gönderildi!', 'İşlem Başarılı!');
                    }else{
                        toastr['error']('Kayıtlı email adresinizi kontrol edin!', 'Giriş Başarısız!');
                    }
                }
            });
        })
    });
</script>
</body>
</html>
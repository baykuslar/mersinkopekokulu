<!DOCTYPE html>
<html style="background: none !important;">
<head>
    <meta charset="utf-8">
    <title>BaykusCreative | AdminPanel</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="Preview page of Metronic Admin Theme #1 for " name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="/backend/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="/backend/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="/backend/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/backend/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="/backend/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="/backend/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="/backend/assets/pages/css/login.min.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="favicon.ico" />
    <!-- Settings (Remove it on Production) -->
    <link href="/backend/assets/login/plugins/settings/jquery.colorpanel.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/eklentiler/bootstrap-toastr/toastr.min.css"/>
</head>
<body class=" login">
<!-- BEGIN LOGO -->
<div class="logo">
    <a href="index.html">
        <img src="/backend/assets/pages/img/logo-big.png" alt="" /> </a>
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content" style="border-radius:10px !important;">
    <!-- BEGIN LOGIN FORM -->
    <form role="form" method="POST" action="#" id="giris-formu">
        <h3 class="form-title font-green" style="padding: 15px;">Giriş Yapınız!</h3>
        <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">E-mail</label>
            <input id="email" type="email" class="form-control renk" name="email" value="" placeholder="Email adresiniz" autofocus>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Şifreniz</label>
            <input id="password" type="password" class="form-control renk" name="password" placeholder="Şifreniz" >
        </div>
        <div class="form-actions">
            <input type="hidden" value="{{csrf_token()}}" id="jeton" name="jeton">
            <button type="submit" class="btn green uppercase">
                GİRİŞ YAPINIZ
            </button>
            <label class="rememberme check mt-checkbox mt-checkbox-outline">
                <input type="checkbox" name="remember" />Beni Hatırla
                <span></span>
            </label>
        </div>
        <div class="create-account">
            <p>
                <a href="{{ url('/password/email') }}">Şifremi Unuttum?</a>
            </p>
        </div>
    </form>
    <!-- END LOGIN FORM -->
</div>
<div style="width: 400px;overflow: hidden;position: relative;margin:auto">
    <div class="copyright" onclick="window.open('http://www.baykuscreative.com/')"> <?php echo date('Y'); ?> ©   BaykusCreative </div>
</div>

<!-- BEGIN CORE PLUGINS -->
<script src="/backend/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="/backend/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
<script src="/backend/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="/backend/assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="/backend/assets/pages/scripts/login.min.js" type="text/javascript"></script>

<script src="/eklentiler/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function($) {
        $('#giris-formu').on('submit', function (e) {
            e.preventDefault();
            var email = $('#email').val();
            var jeton = $('#jeton').val();
            if( email.length < 9 ) {
                toastr['error']('E-mail adresiniz eksik veya hatalı!', 'Eksik Bilgi!');
                return false;
            }
            var password = $('#password').val();
            var jeton = $('#jeton').val();
            if( password.length < 6 ) {
                toastr['error']('Şifreniz eksik veya hatalı!', 'Eksik Bilgi!');
                return false;
            }

            $.ajax({
                url : '/admin',
                type : 'POST',
                data : { email : email, password : password, _token:jeton },
                success : function(cevap){
                    switch ( cevap.giris ){
                        case 0:
                            toastr['error']('Kullanıcı bilgilerinizi doldurunuz!', 'Giriş Başarısız!');
                            break;
                        case 1:
                            toastr['success']('Kullanıcı bilgileriniz doğru!', 'Giriş Başarılı!');
                            window.location.href ='/admin/index';
                            break;
                        case 2:
                            toastr['error']('Kullanıcı Pasif! Lütfen baykuscreative@gmail.com ye mail atınız!', 'Giriş Başarısız!');
                            break;
                        case 3:
                            toastr['error']('Kullanıcı email veya şifre hatalı, kontrol edip tekrar deneyiniz!', 'Giriş Başarısız!');
                            break
                    }
                }
            });
        })
    });

</script>
</body>
</html>
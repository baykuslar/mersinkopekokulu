<?php

namespace App\Http\ViewComposers;

use App\Modeller\Ayarlar;
use App\Modeller\Girismodal;
use App\Modeller\Egitimlerimiz;
use App\Modeller\Webbilgileri;
use App\Modeller\Webchat;
use App\Modeller\Whatsapptemsilciler;
use App\Modeller\Iletisim;
use App\Modeller\Insankaynaklari;
use App\Modeller\IslemGecmisi;
use Cache;
use Illuminate\Contracts\View\View;

class Forummail
{
    protected $urunkategoriler, $whatsapptemsilciler, $kacmailvar,$kacisbasvurusuvar,$guniciislemler, $ayarlar, $girismodal, $webchat,$islemGecmisikayitlar,$egitimlerimiz,$webbilgileri,$dkacgun,$hkacgun;

    public function __construct()
    {
        $this->whatsapptemsilciler = Whatsapptemsilciler::orderBy('id', 'DESC')->get();
        $this->egitimlerimiz = Egitimlerimiz::orderBy('id', 'ASC')->get();
        $this->kacmailvar = Iletisim::where('aktif_pasif', 'on')->count();
        $this->ayarlar = Ayarlar::first();
        $this->girismodal = Girismodal::first();
        $this->webchat = Webchat::first();
        $this->kacisbasvurusuvar = Insankaynaklari::where('aktif_pasif', 'on')->count();

        $this->islemGecmisikayitlar = IslemGecmisi::orderBy('yapilan_islem', 'DESC')->get();
        $this->guniciislemler = 0;
        foreach ($this->islemGecmisikayitlar as $islem){
            if (\Carbon\Carbon::parse($islem->updated_at)->format('d/m/Y')==date('d/m/Y')){
                $this->guniciislemler++;
            }
        }

        $this->webbilgileri = Webbilgileri::first();
        $dbitis =  $this->webbilgileri->domain_bitis_tarihi;
        $hbitis =  $this->webbilgileri->hosting_bitis_tarihi;
        $d = strtotime($dbitis);
        $h = strtotime($hbitis);

        $bugun = date('d.m.Y');
        $b = strtotime($bugun);

        $dsonuc = $d-$b;
        $hsonuc = $h-$b;
        $this->dkacgun = $dsonuc / (60*60*24);
        $this->hkacgun = $hsonuc / (60*60*24);

    }

    public function compose(View $view)
    {
        $view->with('viewWhatsapptemsilciler', $this->whatsapptemsilciler);
        $view->with('viewEgitimlerimiz', $this->egitimlerimiz);
        $view->with('viewForummail', $this->kacmailvar);
        $view->with('viewAyarlar', $this->ayarlar);
        $view->with('viewGirismodal', $this->girismodal);
        $view->with('viewWebchat', $this->webchat);
        $view->with('viewGunisciislemler', $this->guniciislemler);
        $view->with('viewIslemGecmisikayitlar', $this->islemGecmisikayitlar);
        $view->with('viewIsbasvurusu', $this->kacisbasvurusuvar);
        $view->with('viewWebbilgileri', $this->webbilgileri);
        $view->with('viewWebbilgilerid', $this->dkacgun);
        $view->with('viewWebbilgilerih', $this->hkacgun);
    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class YoneticiYetkiMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $kullanici = array("5","4","3","2","1");

        if ((Auth::user()->kullanici_turu) != (is_array($kullanici))) {
            return redirect('/admin/yetkiniz-yok');
        }

        return $next($request);
    }
}

<?php

namespace App\Http\Middleware;

use Closure;

class Yerellestirme
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( !\Session::has('dil') ) \Session::put('dil', 'tr');

        \App::setLocale( \Session::get('dil') );
        
        return $next($request);
    }
}

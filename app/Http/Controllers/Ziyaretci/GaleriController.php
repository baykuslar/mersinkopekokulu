<?php

namespace App\Http\Controllers\Ziyaretci;
use App\Http\Controllers\Controller;
use App\Modeller\Galerikategori;
use App\Modeller\Video;
use App\Modeller\Galeri;
use App\Http\Requests;
use Auth;
use DB;
use Image;
use Validator;
use Response;

class GaleriController extends Controller
{
    //Video Eğitimi
    public function videogalerisi()
    {
        \Visitor::log();
        $videolar= Video::orderBy('updated_at','desc')->get();
        return view('frontend.galeri.videogalerisi',
            [
                'videolar' => $videolar,
            ]);
    }

    //Resim Galerisi
    public function resimgalerisi()
    {
        \Visitor::log();
        $resimgalerisi= Galeri::orderBy('updated_at','ASC')->get()/*paginate(48)*/;
        $galerikategori = Galerikategori::all();
        return view('frontend.galeri.resimgalerisi',
            [
                'resimgalerisi' => $resimgalerisi,
                'galerikategori' => $galerikategori,
            ]);
    }

}
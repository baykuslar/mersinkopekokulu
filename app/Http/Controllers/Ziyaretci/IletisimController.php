<?php

namespace App\Http\Controllers\Ziyaretci;
use App\Http\Controllers\Controller;
use App\Modeller\Iletisim;
use App\Http\Requests;
use Auth;
use Image;
use Response;
use Illuminate\Support\Facades\Input;

class IletisimController extends Controller
{
    //
    public function index()
    {
        \Visitor::log();
        return view('frontend.iletisim.index');
    }

    public function getUserIp()
    {
        if( isset( $_SERVER["HTTP_CLIENT_IP"] ) ) {
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        } elseif( isset( $_SERVER["HTTP_X_FORWARDED_FOR"] ) ) {
            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        } else {
            $ip = $_SERVER["REMOTE_ADDR"];
        }
        return $ip;
    }

    public  function  iletisimGonder()
    {
        $vToken = Input::get('_vToken');
        if( $this->robotMu($vToken) ) return response()->json(['success' => false]);
        $gonderenemail = Input::get('email');
        $gonderenadi = Input::get('adi_soyadi');
        $alicimail = 'mersinkopekokulu@gmail.com';
        $aliciadi = 'Mars K-9 Köpek eğitim Merkezi';
        //Mail Gönder
        $mailBasligi = 'WEBSİTE ÜZERİNDEN MAİLİNİZ VAR!';
        $gelenBilgiler = Input::all();
        unset($gelenBilgiler['_token']);
        unset($gelenBilgiler['_vToken']);
        $kayit = new Iletisim($gelenBilgiler);
        $kayit->save();
        \Mail::send(
            'frontend.forum-mail',
            ['kayit' => $kayit],
            function ($message) use ($mailBasligi, $gelenBilgiler,$gonderenemail,$gonderenadi,$alicimail,$aliciadi ) {
                $message->from('mersinkopekokulu@gmail.com', 'www.mersinkopekokulu.com');
                $message->to($alicimail, 'www.mersinkopekokulu.com')->subject( $mailBasligi );
                $message->bcc($gonderenemail, $aliciadi)->subject($mailBasligi);
            }
        );
        return response()->json(['success' => true]);
    }

    public  function robotMu($vToken)
    {
        $url ='https://www.google.com/recaptcha/api/siteverify?secret='.env('reCAPTCHA_GIZLI_ANAHTARI').'&response=' . $vToken . '&remoteip=' .  $this->getUserIp();
        $cevap = file_get_contents($url);
        $cevap = json_decode($cevap)->success;
        return  empty($cevap);
    }

}
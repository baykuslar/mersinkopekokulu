<?php

namespace App\Http\Controllers\Ziyaretci;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Modeller\Insankaynaklari;
use Validator;
use Response;
use Image;
use Illuminate\Support\Facades\Input;

class InsankaynaklariController extends Controller
{

    // index item
    public function index()
    {
        \Visitor::log();
        $islemGecmisikayitlar = IslemGecmisi::orderBy('yapilan_islem', 'DESC')->get();
        $guniciislemler = 0;
        foreach ($islemGecmisikayitlar as $islem){
            if (\Carbon\Carbon::parse($islem->created_at)->format('d/m/Y')==date('d/m/Y')){
                $guniciislemler++;
            }
        }
        $basvurukayitsor		=	\DB::select("SELECT count(id) as sayi FROM tb_is_basvuru_formu ");
        $basvurutopla	        =	$basvurukayitsor[0]->sayi;

        $kayitlar = Insankaynaklari::orderBy('id')->get();
        return view('frontend.kurumsal.insan-kaynaklari',
            [
                'is_basvuru_formu'  => $kayitlar,
                'basvurutopla'      => $basvurutopla,
            ] );
    }

    public function pdf($id)
    {
        $basvurukayitsor		=	\DB::select("SELECT count(id) as sayi FROM tb_is_basvuru_formu ");
        $basvurutopla	        =	$basvurukayitsor[0]->sayi;

        $kisi_id = Insankaynaklari::find($id);
        $kayitlar = Insankaynaklari::orderBy('id')->get();
        return view('insan-kaynaklari.isbasvuru-pdf',
            [
                'kisi_id'           => $kisi_id,
                'is_basvuru_formu'  => $kayitlar,
                'basvurutopla'      => $basvurutopla,
            ]);
    }

    // store item
    public function store(Request $request)
    {
        $filename = null;
        if( !empty( $request->file('resim') ) ){
            $resim = $request->file('resim');
            $filename = time() . '.' . $resim->getClientOriginalExtension();
//            $publicAvatarDizini = base_path('../mc.mspcreative.com.tr/uploads/personel_avatar/');
            $publicAvatarDizini = public_path('/frontend/images/insan-kaynaklari/');
            $resim->move( $publicAvatarDizini, $filename );
            Image::make( $publicAvatarDizini . $filename )->resize(720, 720)->save( $publicAvatarDizini . $filename );
        }

        //Gelen bilgileri al
        $gelenBilgiler = Input::all();

        //Fazlalık olan datalari sil (tabloda olmayan)
        unset($gelenBilgiler['_token']);

        //Kalan verileri düzenle
        $gelenBilgiler['resim'] = $filename;
        //Gelen veriler ile yeni bir kayıt oluştur
        $kayit = new Insankaynaklari($gelenBilgiler);
        $kayit->save();
        return back()->with('status', 'Kaydınız başarıyla oluşturuldu.');
    }

}

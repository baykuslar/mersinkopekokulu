<?php

namespace App\Http\Controllers\Ziyaretci;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Modeller\Egitimlerimiz;
use Auth;
use DB;
use Image;
use Validator;
use Response;

class EgitimlerimizController extends Controller
{
    //
    public function index()
    {
        \Visitor::log();
        $kayitlar= Egitimlerimiz::orderBy('id', 'ASC')->get();
        return view('frontend.egitimlerimiz.index',
            [
                'kayitlar' => $kayitlar,
            ]);
    }

    public function mersinEgitim($slug)
    {

        $kategori = Egitimlerimiz::where('slug', $slug)->get();

        if( $kategori->count() < 1 ) dd('Slug Hatalı');
        $kategori = $kategori[0];

        $kayitlar= Egitimlerimiz::find($kategori->id);
        $kayitlar->hit++;
        $kayitlar->save();
        return view('frontend.egitimlerimiz.egitim-detay',
            [
                'kayitlar' => $kayitlar,
                'kategori' => $kategori,
                'slug' => $slug,
                 'description' => $kategori->meta_description,
                 'metakw' => $kategori->meta_keywords,
            ]);
    }

}

<?php

namespace App\Http\Controllers\Ziyaretci;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Modeller\Yorumlar;
use Auth;
use Image;
use Response;
use Alert;
use Illuminate\Support\Facades\Input;

class YorumlarController extends Controller
{
    //
    public function index()
    {
        \Visitor::log();
        $yorumlar = Yorumlar::all();
        return view('frontend.yorumlar.index',
            [
                'yorumlar' => $yorumlar,
            ]);
    }

    public  function  yorumGonder()
    {
        $gelenBilgiler = Input::all();
        unset($gelenBilgiler['_token']);
        $kayit = new User($gelenBilgiler);
        $kayit->save();
        return response('', 204);
    }


}

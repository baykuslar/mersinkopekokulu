<?php

namespace App\Http\Controllers\Ziyaretci;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Input;

class ZiyaretciController extends Controller
{
    public function index()
    {
        return response()->view('auth.login');
    }

    public function dilDegistir(){
        $dil = Input::get('lang');

        if( empty($dil) || !in_array($dil, ['tr', 'en']) ) return redirect('/');

        \Session::put('dil', $dil);
        return redirect('/');
    }

    public function oturumAc()
    {
        return response()->view('auth.login');
    }

    public function emailHatirlatmaFormu(){
        return response()->view('auth.passwords.email');
    }

    public function oturumDogrula()
    {
        $email = Input::get('email');
        $password = Input::get('password');
        $oturumAcildi = 0;//bilgileri doldur

        if(  !empty($email) && !empty($password) ) {
            $user = User::where('email', $email)->get()->first();
            if($user->aktif_pasif=='on'){
                $oturumAcildi = \Auth::attempt(['email' => $email, 'password' => $password]);
                $oturumAcildi = $oturumAcildi?1:3;
                //1=oturum açıldı-başarılı
                //2=kull. passif
                //3=kulll hatalı veya şifre hatalı
            }else{
                $oturumAcildi = 2;
            }
        }

        return response()->json(['giris' => $oturumAcildi]);
    }

    /*public function oturumDogrula()
    {
        $email = Input::get('email');
        $password = Input::get('password');

        if(  !empty($email) && !empty($password) ) {
            $user = User::where('email', $email)->get()->first();
            if($user->aktif_pasif=='on'){
                if(\Auth::attempt(['email' => $email, 'password' => $password])){
                    Session::put('yetki',$user->kullanici_turu);
                    //Session::get('yetki');
                };
            }
        }

        return redirect('/admin/index');

    }*/
}

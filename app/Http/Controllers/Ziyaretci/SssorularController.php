<?php

namespace App\Http\Controllers\Ziyaretci;
use App\Http\Controllers\Controller;
use App\Modeller\Sssorular;
use App\Http\Requests;
use Auth;
use DB;
use Image;
use Validator;
use Response;

class SssorularController extends Controller
{
    public function index()
    {
        \Visitor::log();
        $sssorular= Sssorular::all();
        $sssorularilk= Sssorular::first();
        return view('frontend.sss.index',
            [
                'sssorular' => $sssorular,
                'sssorularilk' => $sssorularilk,
            ]);
    }


}

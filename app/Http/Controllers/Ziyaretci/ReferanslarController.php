<?php

namespace App\Http\Controllers\Ziyaretci;
use App\Http\Controllers\Controller;
use App\Modeller\Kopekokulu;
use App\Modeller\Galeri;
use App\Modeller\Gayrimenkul;
use App\Modeller\Referanslar;
use App\Http\Requests;
use Auth;
use DB;
use Image;
use Validator;
use Response;

class ReferanslarController extends Controller
{
    //Köpek Eğitimi
    public function referanslar()
    {
        \Visitor::log();
        $galerireferanslarresimleri= Galeri::where('resim_kategori', 'Köpek Referanslar')->get();
        $referanslarimiz= Referanslar::all();
        return view('frontend.referanslar.referanslar',
            [
                'galerireferanslarresimleri' => $galerireferanslarresimleri,
                'referanslarimiz' => $referanslarimiz,
            ]);
    }

    // show item
    public function show($slug)
    {
        $bloglar = Referanslar::all();
        $blog = Referanslar::where('slug', $slug)->get();

        if( $blog->count() < 1 ) dd('Slug Hatalı');
        $blog = $blog[0];

        $kayitlar= Referanslar::find($blog->id);
        $kayitlar->hit++;
        $kayitlar->save();
        return view('frontend.referanslar.referans-detay',
            [
                'bloglar' => $bloglar,
                'kayit' => $kayitlar,
                'blog' => $blog,
                'slug' => $slug,
                'description' => $blog->meta_description,
                'metakw' => $blog->meta_description
            ]);
    }

}
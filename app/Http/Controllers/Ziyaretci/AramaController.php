<?php

namespace App\Http\Controllers\Ziyaretci;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Modeller\Egitimlerimiz;
use Auth;
use DB;
use Image;
use Validator;
use Response;

class AramaController extends Controller
{
    public function ara()
    {
        $aranan = request()->input('aranan');
        $egitimlerimiz= Egitimlerimiz::where('baslik_tr', 'like', "%$aranan%")->orwhere('aciklama_tr', 'like', "%$aranan%")->get();
        request()->flash();
        return view('frontend.egitimlerimiz.arama', compact('egitimlerimiz'));
    }

}
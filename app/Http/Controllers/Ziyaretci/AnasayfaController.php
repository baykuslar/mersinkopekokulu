<?php

namespace App\Http\Controllers\Ziyaretci;
use App\Http\Controllers\Controller;
use App\Modeller\Anasayfaresim;
use App\Modeller\Galeri;
use App\Modeller\Ozelkopekler;
use App\Modeller\Slider;
use App\Http\Requests;
use Auth;
use DB;
use Image;
use Validator;
use Response;

class AnasayfaController extends Controller
{
    //
    public function index()
    {

        \Visitor::log();
        $anasayfaresim= Anasayfaresim::orderBy('id','desc')->get();
        $sliderler= Slider::orderBy('id','desc')->get();
        $galeri= Galeri::orderBy('updated_at','desc')->get();
        $ozelkopekler = Ozelkopekler::orderBy('updated_at','ASC')->get();
        return view('frontend.index',
            [
                'anasayfaresim'=>$anasayfaresim,
                'sliderler'=>$sliderler,
                'galeri' => $galeri,
                'ozelkopekler' => $ozelkopekler,
            ]);
    }
}

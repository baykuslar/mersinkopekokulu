<?php

namespace App\Http\Controllers\Ziyaretci;
use App\Http\Controllers\Controller;
use App\Modeller\Hakkimizda;
use App\Http\Requests;
use Auth;
use DB;
use Image;
use Validator;
use Response;

class KurumsalController extends Controller
{
    //Hakkımızda
    public function hakkimizda()
    {
        \Visitor::log();
        $hakkimizda= Hakkimizda::first();
        return view('frontend.kurumsal.hakkimizda',
            [
                'hakkimizda' => $hakkimizda,
            ]);
    }

    //Ekibimiz
    public function ekibimiz()
    {
        \Visitor::log();
        $hakkimizda= Hakkimizda::first();
        return view('frontend.kurumsal.ekibimiz',
            [
                'hakkimizda' => $hakkimizda,
            ]);
    }

    //İnsan Kaynakları
    public function insanKaynaklari()
    {
        \Visitor::log();
        return view('frontend.kurumsal.insan-kaynaklari');
    }

}
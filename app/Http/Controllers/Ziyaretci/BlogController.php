<?php

namespace App\Http\Controllers\Ziyaretci;
use App\Http\Controllers\Controller;
use App\Modeller\Blog;
use App\Http\Requests;
use App\Modeller\Yorumlar;
use Auth;
use DB;
use Alert;
use Image;
use Validator;
use Response;

class BlogController extends Controller
{
    //Köpek Eğitimi
    public function index()
    {
        \Visitor::log();
        $kayitlar= Blog::orderBy('id','ASC')->paginate(6);
        return view('frontend.blog.index',
            [
                'kayitlar' => $kayitlar,
            ]);
    }

    // show item
    public function show($slug)
    {
        $bloglar = Blog::all();
        $blog = Blog::where('slug', $slug)->get();

        if( $blog->count() < 1 ) dd('Slug Hatalı');
        $blog = $blog[0];
        $yorumlar = Yorumlar::where('blog_id', $blog->id)->get();

        $kayitlar= Blog::find($blog->id);
        $kayitlar->hit++;
        $kayitlar->save();
        return view('frontend.blog.blog-detay',
            [
                'bloglar' => $bloglar,
                'yorumlar' => $yorumlar,
                'kayit' => $kayitlar,
                'blog' => $blog,
                'slug' => $slug,
                'description' => $blog->meta_description,
                'metakw' => $blog->meta_keywords
            ]);
    }

}

<?php
namespace App\Http\Controllers\Yonetici;
use App\Http\Controllers\Controller;
use App\Modeller\Ekibimiz;
use App\User;
use Image;
use Auth;
use Illuminate\Http\Request;
class AnasayfaController extends Controller
{
    public function index()
    {
        if(Auth::user()->kullanici_turu==5){
            $kayitlar = User::orderBy('id', 'DESC')->get();
        }else{
            $kayitlar = User::where('id', Auth::user()->id)->get();
        }
        $ekibimiz= Ekibimiz::all();
        return view('backend.index',
            [
                'ekibimiz' => $ekibimiz,
                'users' => $kayitlar, array('admin' => Auth::user())
            ]);
    }

    // create item
    public function create()
    {
       /* return view('musteriler.create');*/
    }

    // store item
    public function store(Request $request)
    {
        //
    }

    // show item
    public function show($id)
    {
        //
    }

    // update item
    public function update(Request $request, $id)
    {
        //
    }

    // delete item
    public function deleteMusteri(Request $req) {
       //
    }
}
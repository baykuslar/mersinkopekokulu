<?php

namespace App\Http\Controllers\Yonetici;
use App\Http\Controllers\Controller;
use App\Modeller\Kopekokulu;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Modeller\Ayarlar;
use App\Modeller\IslemGecmisi;
use Validator;
use Response;
use DB;
use Auth;
use Image;
use Illuminate\Support\Facades\Input;

class KopekokuluController extends Controller
{
    //
    public function index()
    {
        $ayarlar= Ayarlar::first();
        $kayitlar= Kopekokulu::orderBy('updated_at','desc')->get();
        return view('backend.kopek-okulu.index',
            [
                'ayarlar' => $ayarlar,
                'kayitlar' => $kayitlar,
            ]);
    }

    // store item
    public function store(Request $request)
    {
        //Gelen bilgileri al
        $gelenBilgiler = Input::all();
        //Fazlalık olan datalari sil (tabloda olmayan)
        $id=(int)$gelenBilgiler['gmid'];
        unset($gelenBilgiler['gmid']);
        unset($gelenBilgiler['_method']);
        unset($gelenBilgiler['_token']);

        $filename = null;
        if( !empty( $request->file('resim') ) ){
            $resim = $request->file('resim');
            $filename = time() . '.' . $resim->getClientOriginalExtension();
            $publicAvatarDizini = public_path('/frontend/images/kopek-okulu/');
            $resim->move( $publicAvatarDizini, $filename );
            Image::make( $publicAvatarDizini . $filename )->resize(700, 430)->save( $publicAvatarDizini . $filename );
        }
        //Kalan verileri düzenle
        if( $filename != null ) {
            $gelenBilgiler['resim'] = $filename;
        }

        //Yeni kayıt mı
        if( empty( $id ) ){
            $kayit = new Kopekokulu($gelenBilgiler);
            $kayit->save();
            $this->islemGecmisiKaydet( $kayit, 'Köpek Okulu sayfasına  kayıt eklendi.' );
            \Session::put('onayMesaji', 'Kayıt başarı ile gerçekleşti');
        }else{
            $kayit = Kopekokulu::find( $id );
            foreach( $gelenBilgiler as $k => $v ) $kayit->$k = $v;
            $kayit->save();
            //Gelen verilerin her birini kayda ata
            $this->islemGecmisiKaydet( $kayit, 'Köpek Okulu sayfasında  kayıt güncellendi.' );
            \Session::put('onayMesaji', 'Kayıt başarı ile gerçekleşti');
        }
        return redirect('/admin/kopek-okulu');
    }


    public function islemGecmisiKaydet( $kayit, $guncelleme ) {
        // Kayıt tut
        $islem = new IslemGecmisi(
            [
                'id' => $kayit->id,
                'kisi_id' => \Auth::user()->id,
                'adi_soyadi' => \Auth::user()->name,
                'islem_alani' => $kayit->id,
                'islem_yapilan_sayfa' => 'Köpek Okulu Sayfası',
                'yapilan_islem' => $guncelleme,
            ]
        );
        $islem->save();
    }

    // delete item
    public function deleteKopekokulu(Request $req) {
        $galeri = Kopekokulu::find($req->id);
        $file= $galeri->resim;
        $filename = public_path('/frontend/images/kopek-okulu/'.$file);
        @unlink($filename);
        $this->kayitSil($galeri);
        $galeri->delete();
        return response()->json();
    }
    public function kayitSil( $kayit) {
        if ( $kayit->save()) {
            // Kayıt tut
            $islem = new IslemGecmisi(
                [
                    'id' => $kayit->id,
                    'kisi_id' => \Auth::user()->id,
                    'adi_soyadi' => \Auth::user()->name,
                    'islem_alani' => $kayit->id,
                    'islem_yapilan_sayfa' => 'Köpek Okulu Sayfası',
                    'yapilan_islem' => 'Köpek Okulu sayfasında  kayıt silindi.',
                ]
            );
            $islem->save();
        } else {
            // Kayıt işleminde hata olursa
            return json_encode(array('success' => false));
        }
        return true;
    }
}

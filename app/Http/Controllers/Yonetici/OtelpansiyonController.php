<?php

namespace App\Http\Controllers\Yonetici;
use App\Http\Controllers\Controller;
use App\Modeller\Otelpansiyon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Modeller\IslemGecmisi;
use Validator;
use Response;
use DB;
use Auth;
use Image;
use Illuminate\Support\Facades\Input;

class OtelpansiyonController extends Controller
{
    //
    public function index()
    {
        $kayitlar= Otelpansiyon::all();
        return view('backend.otel-pansiyon.index',
            [
                'kayitlar' => $kayitlar,
            ]);
    }

    // store item
    public function store(Request $request)
    {
        //Gelen bilgileri al
        $gelenBilgiler = Input::all();
        //Fazlalık olan datalari sil (tabloda olmayan)
        $id=(int)$gelenBilgiler['gmid'];
        unset($gelenBilgiler['gmid']);
        unset($gelenBilgiler['_method']);
        unset($gelenBilgiler['_token']);

        $filename = null;
        if( !empty( $request->file('resim') ) ){
            $resim = $request->file('resim');
            $filename = time() . '.' . $resim->getClientOriginalExtension();
            $publicAvatarDizini = public_path('/frontend/images/otel-pansiyon/');
            $resim->move( $publicAvatarDizini, $filename );
            Image::make( $publicAvatarDizini . $filename )->resize(400, 400)->save( $publicAvatarDizini . $filename );
        }
        //Kalan verileri düzenle
        if( $filename != null ) {
            $gelenBilgiler['resim'] = $filename;
        }

        //Yeni kayıt mı
        if( empty( $id ) ){
            $kayit = new Otelpansiyon($gelenBilgiler);
            $kayit->save();
            $this->islemGecmisiKaydet( $kayit, 'Otel/Pansiyon sayfasına  kayıt eklendi.' );
            \Session::put('onayMesaji', 'Kayıt başarı ile gerçekleşti');
        }else{
            $kayit = Otelpansiyon::find( $id );
            foreach( $gelenBilgiler as $k => $v ) $kayit->$k = $v;
            $kayit->save();
            //Gelen verilerin her birini kayda ata
            $this->islemGecmisiKaydet( $kayit, 'Otel/Pansiyon sayfasında  kayıt güncellendi.' );
            \Session::put('onayMesaji', 'Kayıt başarı ile gerçekleşti');
        }
        return redirect('/admin/otel-pansiyon');
    }


    public function islemGecmisiKaydet( $kayit, $guncelleme ) {
        // Kayıt tut
        $islem = new IslemGecmisi(
            [
                'id' => $kayit->id,
                'kisi_id' => \Auth::user()->id,
                'adi_soyadi' => \Auth::user()->name,
                'islem_alani' => $kayit->id,
                'islem_yapilan_sayfa' => 'Otel/Pansiyon Sayfası',
                'yapilan_islem' => $guncelleme,
            ]
        );
        $islem->save();
    }

    public function show($id)
    {
        $kayitlar = Otelpansiyon::find($id);
        return view('otel-pansiyon.show', ['kayitlar' => $kayitlar]);
    }

    // delete item
    public function deleteOtelpansiyon(Request $req) {
        $galeri = Otelpansiyon::find($req->id);
        $file= $galeri->resim;
        $filename = public_path('/frontend/images/otel-pansiyon/'.$file);
        @unlink($filename);
        $this->kayitSil($galeri);
        $galeri->delete();
        return response()->json();
    }
    public function kayitSil( $kayit) {
        if ( $kayit->save()) {
            // Kayıt tut
            $islem = new IslemGecmisi(
                [
                    'id' => $kayit->id,
                    'kisi_id' => \Auth::user()->id,
                    'adi_soyadi' => \Auth::user()->name,
                    'islem_alani' => $kayit->id,
                    'islem_yapilan_sayfa' => 'Otel/Pansiyon Sayfası',
                    'yapilan_islem' => 'Otel/Pansiyon sayfasında  kayıt silindi.',
                ]
            );
            $islem->save();
        } else {
            // Kayıt işleminde hata olursa
            return json_encode(array('success' => false));
        }
        return true;
    }
}

<?php

namespace App\Http\Controllers\Yonetici;
use App\Http\Controllers\Controller;
use App\Modeller\Insankaynaklari;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Modeller\IslemGecmisi;
use Validator;
use Response;
use DB;
use Auth;
use Image;
use Illuminate\Support\Facades\Input;

class InsankaynaklariController extends Controller
{
    //
    public function index()
    {
        $basvurukayitsor		=	\DB::select("SELECT count(id) as sayi FROM tb_is_basvuru_formu ");
        $basvurutopla	        =	$basvurukayitsor[0]->sayi;

        $isbasvurukayitlar = Insankaynaklari::orderBy('updated_at','desc')->get();
        return view('backend.kurumsal.insan-kaynaklari.index',
            [
                'isbasvurukayitlar'  => $isbasvurukayitlar,
                'basvurutopla'      => $basvurutopla,
            ]);
    }

    public function isbasvurusu($id)
    {
        $basvurukayitsor		=	\DB::select("SELECT count(id) as sayi FROM tb_is_basvuru_formu ");
        $basvurutopla	        =	$basvurukayitsor[0]->sayi;

        $kisi_id = Insankaynaklari::find($id);
        $kayitlar = Insankaynaklari::orderBy('id')->get();
        return view('backend.kurumsal.insan-kaynaklari.isbasvuru-pdf',
            [
                'kisi_id'           => $kisi_id,
                'is_basvuru_formu'  => $kayitlar,
                'basvurutopla'      => $basvurutopla,
            ]);
    }

    // Aktif Pasif item
    public function basvurukapat(Request $request, $id)
    {
        //Gelen bigileri al
        $gelenBilgiler = Input::all();

        //Fazlalık olan datalari sil (tabloda olmayan)
        unset($gelenBilgiler['_token']);
        unset($gelenBilgiler['_method']);
        //İlgili kaydı bul
        $kayit = Insankaynaklari::find($id);
        $this->kayitKapatislemGecmisi($kayit);
        //Gelen verilerin her birini kayda ata
        foreach( $gelenBilgiler as $k => $v ) $kayit->$k = $v;
        //Kaydet
        $kayit->save();
        \Session::put('onayMesaji', 'Kayıt başarı ile gerçekleşti');
        return response()->json();
    }
    public function kayitKapatislemGecmisi( $kayit) {
        if ( $kayit->save()) {
            // Kayıt tut
            $islem = new IslemGecmisi(
                [
                    'id' => $kayit->id,
                    'kisi_id' => \Auth::user()->id,
                    'adi_soyadi' => \Auth::user()->name,
                    'islem_alani' => $kayit->adi_soyadi,
                    'islem_yapilan_sayfa' => 'İnsan Kaynakları  Sayfası',
                    'yapilan_islem' => 'İnsan Kaynaklar sayfasında  İş Başvurusu Kapatıldı.',
                ]
            );
            $islem->save();
        } else {
            // Kayıt işleminde hata olursa
            return json_encode(array('success' => false));
        }
        return true;
    }

    // delete item
    public function deleteIsbasvurusu(Request $req) {
        $user = Insankaynaklari::find($req->id);
        $file= $user->resim;
        $filename = public_path('/frontend/images/insan-kaynaklari/'.$file);
        @unlink($filename);
        $this->kayitSilislemGecmisi($user);
        $user->delete();
        return response()->json();
    }
    public function kayitSilislemGecmisi( $kayit) {
        if ( $kayit->save()) {
            // Kayıt tut
            $islem = new IslemGecmisi(
                [
                    'id' => $kayit->id,
                    'kisi_id' => \Auth::user()->id,
                    'adi_soyadi' => \Auth::user()->name,
                    'islem_alani' => $kayit->adi_soyadi,
                    'islem_yapilan_sayfa' => 'İnsan Kaynakları Sayfası',
                    'yapilan_islem' => 'İnsan Kaynaklar sayfasında  İş Başvurusu silindi.',
                ]
            );
            $islem->save();
        } else {
            // Kayıt işleminde hata olursa
            return json_encode(array('success' => false));
        }
        return true;
    }
}

<?php

namespace App\Http\Controllers\Yonetici;
use App\Http\Controllers\Controller;
use App\Modeller\Mahalle;
use App\Modeller\Sehir;
use App\Modeller\Ilce;
use Illuminate\Http\Request;
use App\Modeller\Ayarlar;
use App\Modeller\IslemGecmisi;
use App\Http\Requests;
use Auth;
use DB;
use Illuminate\Support\Facades\Input;
use Image;
use Validator;
use Response;

class AyarlarController extends Controller
{
    //
    public function index()
    {
        $sehir = Sehir::pluck("name","sehir_key")->all();
        $ayarlar= Ayarlar::first();
        return view('backend.ayarlar.index',
            [
                'ayarlar' => $ayarlar,
                'sehir'=>$sehir,
            ]);
    }

    public function getilceList(Request $request)
    {
        $ilce = Ilce::where("ilce_sehirkey",$request->ilce_sehirkey)->pluck("name","ilce_key");
        return response()->json($ilce);
    }
    public function getmahalleList(Request $request)
    {
        $mahalle = Mahalle::where("mahalle_ilcekey",$request->mahalle_ilcekey)->pluck("name","mahalle_key");
        return response()->json($mahalle);
    }

    // kayıt düzenleme kaydetme --
    public function update(Request $request, $id)
    {
        //Gelen bilgileri al
        $gelenBilgiler = Input::all();
        //Fazlalık olan datalari sil (tabloda olmayan)
        unset($gelenBilgiler['_method']);
        unset($gelenBilgiler['_token']);

        $filename = null;
        if( !empty( $request->file('resim') ) ){
            $resim = $request->file('resim');
            $filename = time() . '.' . $resim->getClientOriginalExtension();
            $publicAvatarDizini = public_path('/uploads/logo/');
            $resim->move( $publicAvatarDizini, $filename );
            Image::make( $publicAvatarDizini . $filename )->resize(200, 65)->save( $publicAvatarDizini . $filename );
        }
        //Kalan verileri düzenle
        if( $filename != null ) {
            $gelenBilgiler['resim'] = $filename;
        }
        $kayit = Ayarlar::find( $id );
        foreach( $gelenBilgiler as $k => $v ) $kayit->$k = $v;
        $kayit->save();
        //Gelen verilerin her birini kayda ata
        $this->islemGecmisiKaydet( $kayit, 'Ayarlar sayfasında  kayıt güncellendi.' );
        \Session::put('onayMesaji', 'Kayıt başarı ile gerçekleşti');

        return redirect('/admin/ayarlar');
    }

    public function islemGecmisiKaydet( $kayit, $guncelleme ) {
        // Kayıt tut
        $islem = new IslemGecmisi(
            [
                'id' => $kayit->id,
                'kisi_id' => \Auth::user()->id,
                'adi_soyadi' => \Auth::user()->name,
                'islem_alani' => $kayit->site_basligi,
                'islem_yapilan_sayfa' => 'Ayarlar Sayfası',
                'yapilan_islem' => $guncelleme,
            ]
        );
        $islem->save();
    }

}
<?php
namespace App\Http\Controllers\Yonetici;
use App\Http\Controllers\Controller;
use App\Modeller\Kopekler;
use Illuminate\Http\Request;
use App\Modeller\Ayarlar;
use App\Http\Requests;
use App\Modeller\IslemGecmisi;
use Auth;
use DB;
use File;
use Illuminate\Support\Facades\Input;
use Image;
use Validator;
use Response;

class KopekteminiController extends Controller
{
    //
    public function index()
    {
        $ayarlar= Ayarlar::first();
        $kopeklerimiz= Kopekler::orderBy('updated_at','desc')->get();
        return view('backend.kopek-temini.index',
            [
                'ayarlar' => $ayarlar,
                'kopeklerimiz' => $kopeklerimiz,
            ]);
    }

    // store item
    public function store(Request $request)
    {
        //Gelen bilgileri al
        $gelenBilgiler = Input::all();
        //Fazlalık olan datalari sil (tabloda olmayan)
        $id=(int)$gelenBilgiler['gmid'];
        unset($gelenBilgiler['gmid']);
        unset($gelenBilgiler['_method']);
        unset($gelenBilgiler['_token']);

        $filename = null;
        if( !empty( $request->file('resim') ) ){
            $resim = $request->file('resim');
            $filename = time() . '.' . $resim->getClientOriginalExtension();
            $publicAvatarDizini = public_path('/frontend/images/kopek-temini/');
            $resim->move( $publicAvatarDizini, $filename );
            Image::make( $publicAvatarDizini . $filename )->resize(570, 430)->save( $publicAvatarDizini . $filename );
        }
        $filename_resim_annesi = null;
        if( !empty( $request->file('resim_annesi') ) ){
            $resim_annesi = $request->file('resim_annesi');
            $filename_resim_annesi = time(). '1' . '.' . $resim_annesi->getClientOriginalExtension();
            $publicAvatarDizini = public_path('/frontend/images/kopek-temini/');
            $resim_annesi->move( $publicAvatarDizini, $filename_resim_annesi );
            Image::make( $publicAvatarDizini . $filename_resim_annesi )->resize(390, 215)->save( $publicAvatarDizini . $filename_resim_annesi );
        }
        $filename_resim_babasi = null;
        if( !empty( $request->file('resim_babasi') ) ){
            $resim_babasi = $request->file('resim_babasi');
            $filename_resim_babasi = time(). '2' . '.' . $resim_babasi->getClientOriginalExtension();
            $publicAvatarDizini = public_path('/frontend/images/kopek-temini/');
            $resim_babasi->move( $publicAvatarDizini, $filename_resim_babasi );
            Image::make( $publicAvatarDizini . $filename_resim_babasi )->resize(390, 215)->save( $publicAvatarDizini . $filename_resim_babasi );
        }
        //Kalan verileri düzenle
        if( $filename_resim_annesi != null ) {
            $gelenBilgiler['resim_annesi'] = $filename_resim_annesi;
        }
        if( $filename_resim_babasi != null ) {
            $gelenBilgiler['resim_babasi'] = $filename_resim_babasi;
        }
        if( $filename != null ) {
            $gelenBilgiler['resim'] = $filename;
        }


        //Yeni kayıt mı
        if( empty( $id ) ){
            $kayit = new Kopekler($gelenBilgiler);
            $kayit->save();
            $this->islemGecmisiKaydet( $kayit, 'Köpek Temini sayfasında  kayıt eklendi.' );
            \Session::put('onayMesaji', 'Kayıt başarı ile gerçekleşti');
        }else{
            $kayit = Kopekler::find( $id );
            foreach( $gelenBilgiler as $k => $v ) $kayit->$k = $v;
            $kayit->save();
            //Gelen verilerin her birini kayda ata
            $this->islemGecmisiKaydet( $kayit, 'Köpek Temini sayfasında  resim güncellendi.' );
            \Session::put('onayMesaji', 'Kayıt başarı ile gerçekleşti');
        }
        return redirect('/admin/backend/kopek-temini');
    }

    public function islemGecmisiKaydet( $kayit, $guncelleme ) {
            // Kayıt tut
            $islem = new IslemGecmisi(
                [
                    'id' => $kayit->id,
                    'kisi_id' => \Auth::user()->id,
                    'adi_soyadi' => \Auth::user()->name,
                    'islem_alani' => $kayit->adi,
                    'islem_yapilan_sayfa' => 'Köpek Temini Sayfası',
                    'yapilan_islem' => $guncelleme,
                ]
            );
        $islem->save();
    }
    // delete item
    public function deleteKopektemini(Request $req) {
        $kopeklerimiz = Kopekler::find($req->id);
        $file= $kopeklerimiz->resim;
        $filename = public_path('/frontend/images/kopek-temini/'.$file);
        @unlink($filename);

        $file= $kopeklerimiz->resim_annesi;
        $filename_resim_annesi = public_path('/frontend/images/kopek-temini/'.$file);
        @unlink($filename_resim_annesi);

        $file= $kopeklerimiz->resim_babasi;
        $filename_resim_babasi = public_path('/frontend/images/kopek-temini/'.$file);
        @unlink($filename_resim_babasi);
        $this->kayitSil($kopeklerimiz);
        $kopeklerimiz->delete();
        return response()->json();
    }
    public function kayitSil( $kayit) {
        if ( $kayit->save()) {
            // Kayıt tut
            $islem = new IslemGecmisi(
                [
                    'id' => $kayit->id,
                    'kisi_id' => \Auth::user()->id,
                    'adi_soyadi' => \Auth::user()->name,
                    'islem_alani' => $kayit->adi,
                    'islem_yapilan_sayfa' => 'Köpek Temini Sayfası',
                    'yapilan_islem' => 'Köpek Temini sayfasın da  kayıt silindi.',
                ]
            );
            $islem->save();
        } else {
            // Kayıt işleminde hata olursa
            return json_encode(array('success' => false));
        }
        return true;
    }
}
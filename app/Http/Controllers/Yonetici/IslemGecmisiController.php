<?php
namespace App\Http\Controllers\Yonetici;
use App\Http\Controllers\Controller;
use App\Modeller\IslemGecmisi;
use Image;
use Auth;
use Illuminate\Http\Request;
class IslemGecmisiController extends Controller
{
    //index
    public function index()
    {
        $islemGecmisikayitlar= IslemGecmisi::orderBy('id', 'ASC')->get();
        return view('backend.islem-gecmisi.index',
            [
                'islemGecmisikayitlar' => $islemGecmisikayitlar,
            ]);
    }


    function multipleDelete(Request $request)
    {
        $islemgecmisi_id_array = $request->input('id');
        $islemgecmisi = IslemGecmisi::whereIn('id', $islemgecmisi_id_array);
        $islemgecmisi->delete();
        return response()->json();
    }

// delete İşlem Geçmişi
    public function deleteislemGecmisi(Request $req) {
        $islemgecmisi = IslemGecmisi::find($req->id);
        $islemgecmisi->delete();
        \Session::put('onayMesaji', 'Kayıt başarı ile silindi!');
        return response()->json();
    }

}

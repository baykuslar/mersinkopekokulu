<?php

namespace App\Http\Controllers\Yonetici;
use App\Http\Controllers\Controller;
use App\Modeller\Rezervasyon;
use App\Modeller\Iletisim;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Modeller\IslemGecmisi;
use Validator;
use Response;
use DB;
use Auth;
use Image;
use Illuminate\Support\Facades\Input;

class IletisimController extends Controller
{
    //
    public function index()
    {
        $basvurukayitsor		=	\DB::select("SELECT count(id) as sayi FROM tb_iletisim ");
        $basvurutopla	        =	$basvurukayitsor[0]->sayi;
        $isbasvurukayitlar = Iletisim::orderBy('created_at','DESC')->get();
        $iletisimformvar = Iletisim::all();
        $iletisimform = 0;
        foreach ($iletisimformvar as $basvuru){
            if ($basvuru->aktif_pasif == 'on'){
                $iletisimform++;
            }
        }
        return view('backend.iletisim.index',
            [
                'isbasvurukayitlar'  => $isbasvurukayitlar,
                'basvurutopla'      => $basvurutopla,
                'iletisimform'  => $iletisimform,
            ]);
    }

    // Aktif Pasif item
    public function iletisimformkapat(Request $request, $id)
    {
        //Gelen bigileri al
        $gelenBilgiler = Input::all();

        //Fazlalık olan datalari sil (tabloda olmayan)
        unset($gelenBilgiler['_token']);
        unset($gelenBilgiler['_method']);
        //İlgili kaydı bul
        $kayit = Iletisim::find($id);
        $this->kayitKapatislemGecmisi($kayit);
        //Gelen verilerin her birini kayda ata
        foreach( $gelenBilgiler as $k => $v ) $kayit->$k = $v;
        //Kaydet
        $kayit->save();
        \Session::put('onayMesaji', 'Kayıt başarı ile gerçekleşti');
        return response()->json();
    }
    public function kayitKapatislemGecmisi( $kayit) {
        if ( $kayit->save()) {
            // Kayıt tut
            $islem = new IslemGecmisi(
                [
                    'id' => $kayit->id,
                    'kisi_id' => \Auth::user()->id,
                    'adi_soyadi' => \Auth::user()->name,
                    'islem_alani' => $kayit->adi_soyadi,
                    'islem_yapilan_sayfa' => 'İletişim Formu Sayfası',
                    'yapilan_islem' => 'İletişim Formu sayfasında  kayıt Kapatıldı.',
                ]
            );
            $islem->save();
        } else {
            // Kayıt işleminde hata olursa
            return json_encode(array('success' => false));
        }
        return true;
    }

    // delete item
    public function deleteFormmail(Request $req) {
        $user = Iletisim::find($req->id);
        $this->kayitSilislemGecmisi($user);
        $user->delete();
        return response()->json();
    }
    public function kayitSilislemGecmisi( $kayit) {
        if ( $kayit->save()) {
            // Kayıt tut
            $islem = new IslemGecmisi(
                [
                    'id' => $kayit->id,
                    'kisi_id' => \Auth::user()->id,
                    'adi_soyadi' => \Auth::user()->name,
                    'islem_alani' => $kayit->adi_soyadi,
                    'islem_yapilan_sayfa' => 'İletişim Formu Sayfası',
                    'yapilan_islem' => 'İletişim Formu sayfasında  kayıt silindi.',
                ]
            );
            $islem->save();
        } else {
            // Kayıt işleminde hata olursa
            return json_encode(array('success' => false));
        }
        return true;
    }

}

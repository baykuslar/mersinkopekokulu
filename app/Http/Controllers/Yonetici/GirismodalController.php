<?php

namespace App\Http\Controllers\Yonetici;
use App\Http\Controllers\Controller;
use App\Modeller\Girismodal;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Modeller\IslemGecmisi;
use Validator;
use Response;
use DB;
use Auth;
use Image;
use Illuminate\Support\Facades\Input;

class GirismodalController extends Controller
{
    //
    public function index()
    {
        $girismodal= Girismodal::first();
        return view('backend.giris-modal.index',
            [
                'girismodal' => $girismodal,
            ]);
    }


    // update item
    public function update(Request $request, $id)
    {
        $filename = null;
        if( !empty( $request->file('resim') ) ){
            $resim = $request->file('resim');
            $filename = time(). '1' . '.' . $resim->getClientOriginalExtension();
            $publicAvatarDizini = public_path('/uploads/giris-modal/');
            $resim->move( $publicAvatarDizini, $filename );
            Image::make( $publicAvatarDizini . $filename )->resize(448, auth())->save( $publicAvatarDizini . $filename );
        }
        //Gelen bigileri al
        $gelenBilgiler = Input::all();
        //Fazlalık olan datalari sil (tabloda olmayan)
        unset($gelenBilgiler['_token']);
        unset($gelenBilgiler['_method']);
        //Kalan verileri düzenle
        if( $filename != null ) {
            $gelenBilgiler['resim'] = $filename;
        }
        //İlgili kaydı bul
        $kayit = Girismodal::find($id);
        //Gelen verilerin her birini kayda ata
        foreach( $gelenBilgiler as $k => $v ) $kayit->$k = $v;
        $this->kayitGuncelle( $kayit);
        //Kaydet
        $kayit->save();
        \Session::put('onayMesaji', 'Kayıt başarı ile gerçekleşti');
        return redirect('/admin/giris-modal');
    }
    public function kayitGuncelle( $kayit) {
        if ( $kayit->save()) {
            // Kayıt tut
            $islem = new IslemGecmisi(
                [
                    'id' => $kayit->id,
                    'kisi_id' => \Auth::user()->id,
                    'adi_soyadi' => \Auth::user()->name,
                    'islem_alani' => $kayit->baslik,
                    'islem_yapilan_sayfa' => 'Giriş Madalı',
                    'yapilan_islem' => 'Giriş Madalı güncellendi.',
                ]
            );
            $islem->save();
        } else {
            // Kayıt işleminde hata olursa
            return json_encode(array('success' => false));
        }
        return true;
    }

    // Aktif Pasif item
    public function aktifpasif(Request $request, $id)
    {
        //Gelen bigileri al
        $gelenBilgiler = Input::all();

        //Fazlalık olan datalari sil (tabloda olmayan)
        unset($gelenBilgiler['_token']);
        unset($gelenBilgiler['_method']);
        //İlgili kaydı bul
        $kayit = Girismodal::find($id);
        //Gelen verilerin her birini kayda ata
        foreach( $gelenBilgiler as $k => $v ) $kayit->$k = $v;
        $this->KayitIslemGecmisi( $kayit);
        $kayit->save();
        \Session::put('onayMesaji', 'Kayıt başarı ile gerçekleşti');
        return redirect('/admin/giris-modal');
    }
    public function KayitIslemGecmisi( $kayit) {
        if ( $kayit->save()) {
            // Kayıt tut
            $islem = new IslemGecmisi(
                [
                    'id' => $kayit->id,
                    'kisi_id' => \Auth::user()->id,
                    'adi_soyadi' => \Auth::user()->name,
                    'islem_alani' => 'Modal '.$kayit->aktif_pasif.' durumuna getirildi.',
                    'islem_yapilan_sayfa' => 'Giriş Madalı',
                    'yapilan_islem' => 'Giriş Madal durum güncellendi.',
                ]
            );
            $islem->save();
        } else {
            // Kayıt işleminde hata olursa
            return json_encode(array('success' => false));
        }
        return true;
    }
}

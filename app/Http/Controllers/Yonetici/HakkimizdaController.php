<?php

namespace App\Http\Controllers\Yonetici;
use App\Http\Controllers\Controller;
use App\Modeller\Hakkimizda;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Modeller\IslemGecmisi;
use Validator;
use Response;
use DB;
use Auth;
use Image;
use Illuminate\Support\Facades\Input;

class HakkimizdaController extends Controller
{
    //
    public function index()
    {
        $hakkimizda= Hakkimizda::first();
        return view('backend.kurumsal.hakkimizda.index',
            [
                'hakkimizda' => $hakkimizda,
            ]);
    }

    // update item
    public function update(Request $request, $id)
    {
        $filename_1 = null;
        if( !empty( $request->file('resim_1') ) ){
            $resim_1 = $request->file('resim_1');
            $filename_1 = time(). '1' . '.' . $resim_1->getClientOriginalExtension();
            $publicAvatarDizini = public_path('/frontend/images/hakkimizda/');
            $resim_1->move( $publicAvatarDizini, $filename_1 );
            Image::make( $publicAvatarDizini . $filename_1 )->resize(270, 180)->save( $publicAvatarDizini . $filename_1 );
        }
        $filename_2 = null;
        if( !empty( $request->file('resim_2') ) ){
            $resim_2 = $request->file('resim_2');
            $filename_2 = time(). '2' . '.' . $resim_2->getClientOriginalExtension();
            $publicAvatarDizini = public_path('/frontend/images/hakkimizda/');
            $resim_2->move( $publicAvatarDizini, $filename_2 );
            Image::make( $publicAvatarDizini . $filename_2 )->resize(270, 180)->save( $publicAvatarDizini . $filename_2 );
        }
        $filename_3 = null;
        if( !empty( $request->file('resim_3') ) ){
            $resim_3 = $request->file('resim_3');
            $filename_3 = time(). '3' . '.' . $resim_3->getClientOriginalExtension();
            $publicAvatarDizini = public_path('/frontend/images/hakkimizda/');
            $resim_3->move( $publicAvatarDizini, $filename_3 );
            Image::make( $publicAvatarDizini . $filename_3 )->resize(270, 180)->save( $publicAvatarDizini . $filename_3 );
        }
        $filename_4 = null;
        if( !empty( $request->file('resim_4') ) ){
            $resim_4 = $request->file('resim_4');
            $filename_4 = time(). '4' . '.' . $resim_4->getClientOriginalExtension();
            $publicAvatarDizini = public_path('/frontend/images/hakkimizda/');
            $resim_4->move( $publicAvatarDizini, $filename_4 );
            Image::make( $publicAvatarDizini . $filename_4 )->resize(270, 180)->save( $publicAvatarDizini . $filename_4 );
        }
        $filename_5 = null;
        if( !empty( $request->file('resim_5') ) ){
            $resim_5 = $request->file('resim_5');
            $filename_5 = time(). '5' . '.' . $resim_5->getClientOriginalExtension();
            $publicAvatarDizini = public_path('/frontend/images/hakkimizda/');
            $resim_5->move( $publicAvatarDizini, $filename_5 );
            Image::make( $publicAvatarDizini . $filename_5 )->resize(270, 180)->save( $publicAvatarDizini . $filename_5 );
        }
        $filename_6 = null;
        if( !empty( $request->file('resim_6') ) ){
            $resim_6 = $request->file('resim_6');
            $filename_6 = time(). '6' . '.' . $resim_6->getClientOriginalExtension();
            $publicAvatarDizini = public_path('/frontend/images/hakkimizda/');
            $resim_6->move( $publicAvatarDizini, $filename_6 );
            Image::make( $publicAvatarDizini . $filename_6 )->resize(270, 180)->save( $publicAvatarDizini . $filename_6 );
        }
        $filename_7 = null;
        if( !empty( $request->file('resim_7') ) ){
            $resim_7 = $request->file('resim_7');
            $filename_7 = time(). '7' . '.' . $resim_7->getClientOriginalExtension();
            $publicAvatarDizini = public_path('/frontend/images/hakkimizda/');
            $resim_7->move( $publicAvatarDizini, $filename_7 );
            Image::make( $publicAvatarDizini . $filename_7 )->resize(270, 180)->save( $publicAvatarDizini . $filename_7 );
        }
        $filename_8 = null;
        if( !empty( $request->file('resim_8') ) ){
            $resim_8 = $request->file('resim_8');
            $filename_8 = time(). '8' . '.' . $resim_8->getClientOriginalExtension();
            $publicAvatarDizini = public_path('/frontend/images/hakkimizda/');
            $resim_8->move( $publicAvatarDizini, $filename_8 );
            Image::make( $publicAvatarDizini . $filename_8 )->resize(270, 180)->save( $publicAvatarDizini . $filename_8 );
        }
        $filename_9 = null;
        if( !empty( $request->file('resim_9') ) ){
            $resim_9 = $request->file('resim_9');
            $filename_9 = time(). '9' . '.' . $resim_9->getClientOriginalExtension();
            $publicAvatarDizini = public_path('/frontend/images/hakkimizda/');
            $resim_9->move( $publicAvatarDizini, $filename_9 );
            Image::make( $publicAvatarDizini . $filename_9 )->resize(580, 330)->save( $publicAvatarDizini . $filename_9 );
        }
        //Gelen bigileri al
        $gelenBilgiler = Input::all();
        //Fazlalık olan datalari sil (tabloda olmayan)
        unset($gelenBilgiler['_token']);
        unset($gelenBilgiler['_method']);
        //Kalan verileri düzenle
        if( $filename_1 != null ) {
            $gelenBilgiler['resim_1'] = $filename_1;
        }
        if( $filename_2 != null ) {
            $gelenBilgiler['resim_2'] = $filename_2;
        }
        if( $filename_3 != null ) {
            $gelenBilgiler['resim_3'] = $filename_3;
        }
        if( $filename_4 != null ) {
            $gelenBilgiler['resim_4'] = $filename_4;
        }
        if( $filename_5 != null ) {
            $gelenBilgiler['resim_5'] = $filename_5;
        }
        if( $filename_6 != null ) {
            $gelenBilgiler['resim_6'] = $filename_6;
        }
        if( $filename_7 != null ) {
            $gelenBilgiler['resim_7'] = $filename_7;
        }
        if( $filename_8 != null ) {
            $gelenBilgiler['resim_8'] = $filename_8;
        }
        if( $filename_9 != null ) {
            $gelenBilgiler['resim_9'] = $filename_9;
        }
        //İlgili kaydı bul
        $kayit = Hakkimizda::find($id);
        //Gelen verilerin her birini kayda ata
        foreach( $gelenBilgiler as $k => $v ) $kayit->$k = $v;
        $this->kayitGuncelle( $kayit);
        //Kaydet
        $kayit->save();
        \Session::put('onayMesaji', 'Kayıt başarı ile gerçekleşti');
        return redirect('/admin/hakkimizda');
    }
    public function kayitGuncelle( $kayit) {
        if ( $kayit->save()) {
            // Kayıt tut
            $islem = new IslemGecmisi(
                [
                    'id' => $kayit->id,
                    'kisi_id' => \Auth::user()->id,
//                    'islem_alani' => $kayit->baslik,
                    'adi_soyadi' => \Auth::user()->name,
                    'islem_yapilan_sayfa' => 'Hakkımızda Sayfası',
                    'yapilan_islem' => 'Hakkımızda sayfası  güncellendi.',
                ]
            );
            $islem->save();
        } else {
            // Kayıt işleminde hata olursa
            return json_encode(array('success' => false));
        }
        return true;
    }

}

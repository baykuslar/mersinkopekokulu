<?php

namespace App\Http\Controllers\Yonetici;
use App\Http\Controllers\Controller;
use App\Modeller\Referanslar;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Modeller\IslemGecmisi;
use Validator;
use Response;
use DB;
use Auth;
use Image;
use Illuminate\Support\Facades\Input;

class ReferanslarController extends Controller
{
    //
    public function index()
    {
        $referanskayitlari= Referanslar::orderBy('updated_at','desc')->get();
        return view('backend.referanslar.index',
            [
                'referanskayitlari' => $referanskayitlari,
            ]);
    }

    // store item
    public function store(Request $request)
    {
        //Gelen bilgileri al
        $gelenBilgiler = Input::all();
        //Fazlalık olan datalari sil (tabloda olmayan)
        $id=(int)$gelenBilgiler['gmid'];
        unset($gelenBilgiler['gmid']);
        unset($gelenBilgiler['_method']);
        unset($gelenBilgiler['_token']);
        $gelenBilgiler['slug'] = str_slug($gelenBilgiler['baslik_tr']);

        $filename = null;
        //Kalan verileri düzenle
        if( $filename != null ) {
            $gelenBilgiler['resim'] = $filename;
        }
        $kayit = empty( $id ) ? new Referanslar() : Referanslar::find($id);
        if( !empty( $request->file('resim') ) ){
            $resim = $request->file('resim');
            $yeniResim = time() . '.' . $resim->getClientOriginalExtension();
            $publicAvatarDizini = public_path('/uploads/referanslarimiz/');
            $resim->move( $publicAvatarDizini, $yeniResim );
            Image::make( $publicAvatarDizini . $yeniResim )->resize(700, 430)->save( $publicAvatarDizini . $yeniResim );
            if( !empty( $kayit->resim ) )  @unlink($publicAvatarDizini . $kayit->resim);

            $gelenBilgiler['resim'] = $yeniResim;
        }


        //Yeni kayıt mı
        if( empty( $id ) ){
            $kayit = new Referanslar($gelenBilgiler);
            $kayit->save();
            $this->islemGecmisiKaydet( $kayit, 'Referanslar sayfasına  kayıt eklendi.' );
            \Session::put('onayMesaji', 'Kayıt başarı ile gerçekleşti');
        }else{
            $kayit = Referanslar::find( $id );
            foreach( $gelenBilgiler as $k => $v ) $kayit->$k = $v;
            $kayit->save();
            //Gelen verilerin her birini kayda ata
            $this->islemGecmisiKaydet( $kayit, 'Referanslar sayfasında  kayıt güncellendi.' );
            \Session::put('onayMesaji', 'Kayıt başarı ile gerçekleşti');
        }
        return redirect('/admin/referanslar');
    }


    public function islemGecmisiKaydet( $kayit, $guncelleme ) {
        // Kayıt tut
        $islem = new IslemGecmisi(
            [
                'id' => $kayit->id,
                'kisi_id' => \Auth::user()->id,
                'adi_soyadi' => \Auth::user()->name,
                'islem_alani' => $kayit->id,
                'islem_yapilan_sayfa' => 'Referanslar Sayfası',
                'yapilan_islem' => $guncelleme,
            ]
        );
        $islem->save();
    }

    // delete item
    public function deleteReferans(Request $req) {
        $kayit = Referanslar::find($req->id);
        $file= $kayit->resim;
        $filename = public_path('/uploads/referanslarimiz/'.$file);
        @unlink($filename);
        $this->kayitSil($kayit);
        $kayit->delete();
        return response()->json();
    }
    public function kayitSil( $kayit) {
        if ( $kayit->save()) {
            // Kayıt tut
            $islem = new IslemGecmisi(
                [
                    'id' => $kayit->id,
                    'kisi_id' => \Auth::user()->id,
                    'adi_soyadi' => \Auth::user()->name,
                    'islem_alani' => $kayit->id,
                    'islem_yapilan_sayfa' => 'Referanslar Sayfası',
                    'yapilan_islem' => 'Referanslar sayfasında  resim silindi.',
                ]
            );
            $islem->save();
        } else {
            // Kayıt işleminde hata olursa
            return json_encode(array('success' => false));
        }
        return true;
    }
}

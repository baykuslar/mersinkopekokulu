<?php

namespace App\Http\Controllers\Yonetici;
use App\Http\Controllers\Controller;
use App\Modeller\Egitimlerimiz;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Modeller\IslemGecmisi;
use Validator;
use Response;
use MetaTag;
use DB;
use Auth;
use Image;
use Illuminate\Support\Facades\Input;

class EgitimlerimizController extends Controller
{
    //
    public function index()
    {
        $kayitlar= Egitimlerimiz::all();
               return view('backend.egitimlerimiz.index',
            [
                'kayitlar' => $kayitlar ,
            ]);
    }
    // store item
    public function store(Request $request)
    {


        //Gelen bilgileri al
        $gelenBilgiler = Input::all();

        $gelenBilgiler['meta_keywords'] = empty($gelenBilgiler['meta_keywords']) ? [] : $gelenBilgiler['meta_keywords'];
        $gelenBilgiler['meta_keywords'] = implode(',', $gelenBilgiler['meta_keywords']);


        //Fazlalık olan datalari sil (tabloda olmayan)
        $id=(int)$gelenBilgiler['gmid'];

        unset($gelenBilgiler['gmid']);
        unset($gelenBilgiler['_method']);
        unset($gelenBilgiler['_token']);



        $gelenBilgiler['slug'] = str_slug($gelenBilgiler['baslik']);
        $filename = null;
        if( !empty( $request->file('resim') ) ){
            $resim = $request->file('resim');
            $filename = time() . '.' . $resim->getClientOriginalExtension();
            $publicAvatarDizini = public_path('/uploads/egitimlerimiz/');
            $resim->move( $publicAvatarDizini, $filename );
            Image::make( $publicAvatarDizini . $filename )->resize(430, 700)->save( $publicAvatarDizini . $filename );
        }

        //Kalan verileri düzenle
        if( $filename != null ) {
            $gelenBilgiler['resim'] = $filename;
        }

        //Yeni kayıt mı
        if( empty( $id ) ){
            $kayit = new Egitimlerimiz($gelenBilgiler);
            $kayit->save();
            $this->islemGecmisiKaydet( $kayit, 'Eğitimlerimiz Sayfasına kayıt eklendi.' );
            \Session::put('onayMesaji', 'Kayıt başarı ile gerçekleşti');
        }else{
            $kayit = Egitimlerimiz::find( $id );
            foreach( $gelenBilgiler as $k => $v ) $kayit->$k = $v;
            $kayit->save();
            //Gelen verilerin her birini kayda ata
            $this->islemGecmisiKaydet( $kayit, 'Eğitimlerimiz Sayfasına kayıt güncellendi.' );
            \Session::put('onayMesaji', 'Kayıt başarı ile gerçekleşti');
        }
        return redirect('/admin/egitimlerimiz');
    }



    public function islemGecmisiKaydet( $kayit, $guncelleme ) {
        // Kayıt tut
        $islem = new IslemGecmisi(
            [
                'id' => $kayit->id,
                'kisi_id' => \Auth::user()->id,
                'adi_soyadi' => \Auth::user()->name,
                'islem_alani' => $kayit->id,
                'islem_yapilan_sayfa' => 'Eğitimlerimiz Sayfası',
                'yapilan_islem' => $guncelleme,
            ]
        );
        $islem->save();
    }

    // delete item
    public function deleteEgitim(Request $req) {
        $kayit = Egitimlerimiz::find($req->id);
        $this->kayitSil($kayit);
        $kayit->delete();
        \Session::put('onayMesaji', 'Kayıt başarı ile silindi!');
        return response()->json();
    }
    public function kayitSil( $kayit) {
        if ( $kayit->save()) {
            // Kayıt tut
            $islem = new IslemGecmisi(
                [
                    'id' => $kayit->id,
                    'kisi_id' => \Auth::user()->id,
                    'adi_soyadi' => \Auth::user()->name,
                    'islem_alani' => $kayit->adi,
                    'islem_yapilan_sayfa' => 'Eğitimlerimiz Sayfası',
                    'yapilan_islem' => 'Eğitimlerimiz sayfasın da  video silindi.',
                ]
            );
            $islem->save();
        } else {
            // Kayıt işleminde hata olursa
            return json_encode(array('success' => false));
        }
        return true;
    }
}

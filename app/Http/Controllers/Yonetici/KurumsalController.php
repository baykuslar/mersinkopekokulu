<?php

namespace App\Http\Controllers\Yonetici;
use App\Http\Controllers\Controller;
use App\Modeller\Ekibimiz;
use App\Modeller\IslemGecmisi;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Illuminate\Support\Facades\Input;
use Image;
use Validator;
use Response;

class KurumsalController extends Controller
{
    //
    public function index()
    {
        $ekibimiz= Ekibimiz::orderBy('updated_at','desc')->get();
        return view('backend.kurumsal.ekibimiz.index',
            [
                'ekibimiz' => $ekibimiz,
            ]);
    }

    // store item
    public function store(Request $request)
    {
        //Gelen bilgileri al
        $gelenBilgiler = Input::all();
        //Fazlalık olan datalari sil (tabloda olmayan)
        $id=(int)$gelenBilgiler['gmid'];
        unset($gelenBilgiler['gmid']);
        unset($gelenBilgiler['_method']);
        unset($gelenBilgiler['_token']);



        $filename = null;
        if( !empty( $request->file('profil_resim') ) ){
            $profil_resim = $request->file('profil_resim');
            $filename = time() . '.' . $profil_resim->getClientOriginalExtension();
            $publicAvatarDizini = public_path('/uploads/profil_resimleri/');
            $profil_resim->move( $publicAvatarDizini, $filename );
            Image::make( $publicAvatarDizini . $filename )->resize(300, 300)->save( $publicAvatarDizini . $filename );
        }
        //Kalan verileri düzenle
        if( $filename != null ) {
            $gelenBilgiler['profil_resim'] = $filename;
        }

        //Yeni kayıt mı
        if( empty( $id ) ){
            $kayit = new Ekibimiz($gelenBilgiler);
            $kayit->save();
            $this->islemGecmisiKaydet( $kayit, 'Ekip sayfasına yeni kişi eklendi.' );
            \Session::put('onayMesaji', 'Kayıt başarı ile gerçekleşti');
        }else{
            $kayit = Ekibimiz::find( $id );
            foreach( $gelenBilgiler as $k => $v ) $kayit->$k = $v;
            $kayit->save();
            //Gelen verilerin her birini kayda ata
            $this->islemGecmisiKaydet( $kayit, 'Ekip sayfasınd kişi güncellendi.' );
            \Session::put('onayMesaji', 'Kayıt başarı ile gerçekleşti');
        }
        return redirect('/admin/backend/kurumsal/ekibimiz');
    }


    public function islemGecmisiKaydet( $kayit, $guncelleme ) {
        // Kayıt tut
        $islem = new IslemGecmisi(
            [
                'id' => $kayit->id,
                'kisi_id' => \Auth::user()->id,
                'adi_soyadi' => \Auth::user()->name,
                'islem_alani' => $kayit->adi_soyadi,
                'islem_yapilan_sayfa' => 'Ekibimiz Sayfası',
                'yapilan_islem' => $guncelleme,
            ]
        );
        $islem->save();
    }
    // delete item
    public function deleteEkibimiz(Request $req) {
        $user = Ekibimiz::find($req->id);
        $file= $user->profil_resimleri;
        $filename = public_path('/uploads/profil_resimleri/'.$file);
        @unlink($filename);
        $this->kayitSil($user);
        $user->delete();
        return response()->json();
    }
    public function kayitSil( $kayit) {
        if ( $kayit->save()) {
            // Kayıt tut
            $islem = new IslemGecmisi(
                [
                    'id' => $kayit->id,
                    'kurum_id' => \Auth::user()->id,
                    'aciklamasi' => $kayit->adi_soyadi,
                    'adi_soyadi' => \Auth::user()->name,
                    'islem_yapilan_sayfa' => 'Ekibimiz Sayfası',
                    'yapilan_islem' => 'Ekip sayfasın kişi silindi.',
                ]
            );
            $islem->save();
        } else {
            // Kayıt işleminde hata olursa
            return json_encode(array('success' => false));
        }
        return true;
    }
}
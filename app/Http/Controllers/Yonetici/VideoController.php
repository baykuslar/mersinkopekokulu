<?php

namespace App\Http\Controllers\Yonetici;
use App\Http\Controllers\Controller;
use App\Modeller\Video;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Modeller\Ayarlar;
use App\Modeller\IslemGecmisi;
use Validator;
use Response;
use DB;
use Auth;
use Image;
use Illuminate\Support\Facades\Input;

class VideoController extends Controller
{
    //
    public function index()
    {
        $ayarlar= Ayarlar::first();
        $videokayitlari= video::orderBy('updated_at','desc')->get();
        return view('backend.video.index',
            [
                'ayarlar' => $ayarlar,
                'videokayitlari' => $videokayitlari,
            ]);
    }

    // store item
    public function store(Request $request)
    {
        //Gelen bilgileri al
        $gelenBilgiler = Input::all();
        //Fazlalık olan datalari sil (tabloda olmayan)
        $id=(int)$gelenBilgiler['gmid'];
        unset($gelenBilgiler['gmid']);
        unset($gelenBilgiler['_method']);
        unset($gelenBilgiler['_token']);

        //Yeni kayıt mı
        if( empty( $id ) ){
            $kayit = new Video($gelenBilgiler);
            $kayit->save();
            $this->islemGecmisiKaydet( $kayit, 'Videolar sayfasına  video eklendi.' );
            \Session::put('onayMesaji', 'Kayıt başarı ile gerçekleşti');
        }else{
            $kayit = Video::find( $id );
            foreach( $gelenBilgiler as $k => $v ) $kayit->$k = $v;
            $kayit->save();
            //Gelen verilerin her birini kayda ata
            $this->islemGecmisiKaydet( $kayit, 'Videolar sayfasında  video güncellendi.' );
            \Session::put('onayMesaji', 'Kayıt başarı ile gerçekleşti');
        }
        return redirect('/admin/video');
    }


    public function islemGecmisiKaydet( $kayit, $guncelleme ) {
        // Kayıt tut
        $islem = new IslemGecmisi(
            [
                'id' => $kayit->id,
                'kisi_id' => \Auth::user()->id,
                'adi_soyadi' => \Auth::user()->name,
                'islem_alani' => $kayit->adi,
                'islem_yapilan_sayfa' => 'Videolar Sayfası',
                'yapilan_islem' => $guncelleme,
            ]
        );
        $islem->save();
    }
    // delete item
    public function deleteVideo(Request $req) {
        $video = Video::find($req->id);
        $this->kayitSil($video);
        $video->delete();
        \Session::put('onayMesaji', 'Kayıt başarı ile silindi!');
        return response()->json();
    }
    public function kayitSil( $kayit) {
        if ( $kayit->save()) {
            // Kayıt tut
            $islem = new IslemGecmisi(
                [
                    'id' => $kayit->id,
                    'kisi_id' => \Auth::user()->id,
                    'adi_soyadi' => \Auth::user()->name,
                    'islem_alani' => $kayit->baslik,
                    'islem_yapilan_sayfa' => 'Videolar Sayfası',
                    'yapilan_islem' => 'Videolar sayfasın da  video silindi.',
                ]
            );
            $islem->save();
        } else {
            // Kayıt işleminde hata olursa
            return json_encode(array('success' => false));
        }
        return true;
    }
}
<?php
namespace App\Http\Controllers\Yonetici;
use App\Http\Controllers\Controller;
use App\Modeller\Galeri;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Modeller\Ayarlar;
use App\Modeller\IslemGecmisi;
use Validator;
use Response;
use DB;
use Auth;
use Image;
use Illuminate\Support\Facades\Input;

class GaleriController extends Controller
{
    //
    public function index()
    {
        $ayarlar= Ayarlar::first();
        $kayitlar= Galeri::orderBy('updated_at','desc')->get();
        return view('backend.galeri.index',
            [
                'ayarlar' => $ayarlar,
                'kayitlar' => $kayitlar,
            ]);
    }

    // store item
    public function store(Request $request)
    {
        //Gelen bilgileri al
        $gelenBilgiler = Input::all();
        //Fazlalık olan datalari sil (tabloda olmayan)
        $id=(int)$gelenBilgiler['gmid'];
        unset($gelenBilgiler['gmid']);
        unset($gelenBilgiler['_method']);
        unset($gelenBilgiler['_token']);

        $filename = null;
        if( !empty( $request->file('resim') ) ){
            $resim = $request->file('resim');
            $filename = time() . '.' . $resim->getClientOriginalExtension();
            $publicAvatarDizini = public_path('/frontend/images/galeri/');
            $resim->move( $publicAvatarDizini, $filename );
            Image::make( $publicAvatarDizini . $filename )->resize(640, 450)->save( $publicAvatarDizini . $filename );
        }
        //Kalan verileri düzenle
        if( $filename != null ) {
            $gelenBilgiler['resim'] = $filename;
        }

        //Yeni kayıt mı
        if( empty( $id ) ){
            $kayit = new Galeri($gelenBilgiler);
            $kayit->save();
            $this->islemGecmisiKaydet( $kayit, 'Galeri sayfasına  resim eklendi.' );
            \Session::put('onayMesaji', 'Kayıt başarı ile gerçekleşti');
        }else{
            $kayit = Galeri::find( $id );
            foreach( $gelenBilgiler as $k => $v ) $kayit->$k = $v;
            $kayit->save();
            //Gelen verilerin her birini kayda ata
            $this->islemGecmisiKaydet( $kayit, 'Galeri sayfasında  resim güncellendi.' );
            \Session::put('onayMesaji', 'Kayıt başarı ile gerçekleşti');
        }

        return redirect('/admin/galeri');
    }

    public function islemGecmisiKaydet( $kayit, $guncelleme ) {
        // Kayıt tut
        $islem = new IslemGecmisi(
            [
                'id' => $kayit->id,
                'kisi_id' => \Auth::user()->id,
                'adi_soyadi' => \Auth::user()->name,
                'islem_alani' => $kayit->id,
                'islem_yapilan_sayfa' => 'Galeri Sayfası',
                'yapilan_islem' => $guncelleme,
            ]
        );
        $islem->save();
    }

    // delete item
    public function deleteGaleri(Request $req) {
        $galeri = Galeri::find($req->id);
        $file= $galeri->resim;
        $filename = public_path('/frontend/images/galeri/'.$file);
        @unlink($filename);
        $this->kayitSil($galeri);
        $galeri->delete();
        return response()->json();
    }
    public function kayitSil( $kayit) {
        if ( $kayit->save()) {
            // Kayıt tut
            $islem = new IslemGecmisi(
                [
                    'id' => $kayit->id,
                    'kisi_id' => \Auth::user()->id,
                    'adi_soyadi' => \Auth::user()->name,
                    'islem_alani' => $kayit->adi,
                    'islem_yapilan_sayfa' => 'Galeri Sayfası',
                    'yapilan_islem' => 'Galeri sayfasında  resim silindi.',
                ]
            );
            $islem->save();
        } else {
            // Kayıt işleminde hata olursa
            return json_encode(array('success' => false));
        }
        return true;
    }
}

<?php

namespace App\Http\Controllers\Yonetici;
use App\Http\Controllers\Controller;
use App\Modeller\Slider;
use App\Modeller\Sssorular;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Modeller\IslemGecmisi;
use Validator;
use Response;
use DB;
use Auth;
use Image;
use Illuminate\Support\Facades\Input;

class SssorularController extends Controller
{
    //
    public function index()
    {
        $sssorular= Sssorular::all();
        return view('backend.sssorular.index',
            [
                'sssorular' => $sssorular,
            ]);
    }

    // store item
    public function store(Request $request)
    {
        //Gelen bilgileri al
        $gelenBilgiler = Input::all();
        //Fazlalık olan datalari sil (tabloda olmayan)
        $id=(int)$gelenBilgiler['gmid'];
        unset($gelenBilgiler['gmid']);
        unset($gelenBilgiler['_method']);
        unset($gelenBilgiler['_token']);

        //Yeni kayıt mı
        if( empty( $id ) ){
            $kayit = new Sssorular($gelenBilgiler);
            $kayit->save();
            $this->islemGecmisiKaydet( $kayit, 'SSS eklendi.' );
            \Session::put('onayMesaji', 'Kayıt başarı ile gerçekleşti');
        }else{
            $kayit = Sssorular::find( $id );
            foreach( $gelenBilgiler as $k => $v ) $kayit->$k = $v;
            $kayit->save();
            //Gelen verilerin her birini kayda ata
            $this->islemGecmisiKaydet( $kayit, 'SSS güncellendi.' );
            \Session::put('onayMesaji', 'Kayıt başarı ile gerçekleşti');
        }
        return redirect('/admin/sssorular');
    }


    public function islemGecmisiKaydet( $kayit, $guncelleme ) {
        // Kayıt tut
        $islem = new IslemGecmisi(
            [
                'id' => $kayit->id,
                'kisi_id' => \Auth::user()->id,
                'adi_soyadi' => \Auth::user()->name,
                'islem_alani' => $kayit->id,
                'islem_yapilan_sayfa' => 'Sssorular Sayfası',
                'yapilan_islem' => $guncelleme,
            ]
        );
        $islem->save();
    }

    // delete item
    public function deleteSoru(Request $req) {
        $galeri = Sssorular::find($req->id);
        $this->kayitSil($galeri);
        $galeri->delete();
        return response()->json();
    }
    public function kayitSil( $kayit) {
        if ( $kayit->save()) {
            // Kayıt tut
            $islem = new IslemGecmisi(
                [
                    'id' => $kayit->id,
                    'kisi_id' => \Auth::user()->id,
                    'adi_soyadi' => \Auth::user()->name,
                    'islem_alani' => $kayit->id,
                    'islem_yapilan_sayfa' => 'SSS Alanı',
                    'yapilan_islem' => 'SSS silindi.',
                ]
            );
            $islem->save();
        } else {
            // Kayıt işleminde hata olursa
            return json_encode(array('success' => false));
        }
        return true;
    }
}

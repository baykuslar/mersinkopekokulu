<?php
namespace App\Http\Controllers\Yonetici;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use App\Modeller\IslemGecmisi;
use Validator;
use Response;
use Auth;
use Image;
use Illuminate\Support\Facades\Input;


class YoneticiController extends Controller
{
    //
    public function index()
    {
        if(Auth::user()->kullanici_turu== '3'){
            $kayitlar = User::where('id', Auth::user()->id)->get();
        }  else{
            $kayitlar = User::where('kullanici_turu', '<=', Auth::user()->kullanici_turu)->orderBy('id', 'DESC')->get();
        }

        return view('backend.yoneticiler.index',
            [
                'users' => $kayitlar,
                ],
            array('admin' => Auth::user()) );
    }

    // create item
    public function create()
    {
     /*   return view('yoneticiler.create');*/
    }

    // store item
    public function store(Request $request)
    {
        //Gelen bilgileri al
        $gelenBilgiler = Input::all();
        //Fazlalık olan datalari sil (tabloda olmayan)
        $id=(int)$gelenBilgiler['gmid'];
        unset($gelenBilgiler['gmid']);
        unset($gelenBilgiler['_method']);
        unset($gelenBilgiler['_token']);
        unset($gelenBilgiler['password_confirmation']);
        $gelenBilgiler['password'] = bcrypt($gelenBilgiler['password']);

        $filename = null;
        if( !empty( $request->file('avatar') ) ){
            $avatar = $request->file('avatar');
            $filename = time() . '.' . $avatar->getClientOriginalExtension();
            $publicAvatarDizini = public_path('/uploads/avatars/');
            $avatar->move( $publicAvatarDizini, $filename );
            Image::make( $publicAvatarDizini . $filename )->resize(300, 300)->save( $publicAvatarDizini . $filename );
        }
        //Kalan verileri düzenle
        if( $filename != null ) {
            $gelenBilgiler['avatar'] = $filename;
        }


        //Yeni kayıt mı
        if( empty( $id ) ){
            $kayit = new User($gelenBilgiler);
            $kayit->save();
            $this->islemGecmisiKaydet( $kayit, 'Yöneticiler sayfasında  resim eklendi.' );
            \Session::put('onayMesaji', 'Kayıt başarı ile gerçekleşti');
        }else{
            $kayit = User::find( $id );
            foreach( $gelenBilgiler as $k => $v ) $kayit->$k = $v;
            $kayit->save();
            //Gelen verilerin her birini kayda ata
            $this->islemGecmisiKaydet( $kayit, 'Yöneticiler sayfasında  resim güncellendi' );
            \Session::put('onayMesaji', 'Kayıt başarı ile gerçekleşti');
        }

        return redirect('/admin/backend/yoneticiler');
    }

    public function islemGecmisiKaydet( $kayit, $guncelleme) {
        // Kayıt tut
            $islem = new IslemGecmisi(
                [
                    'id' => $kayit->id,
                    'kisi_id' => \Auth::user()->id,
                    'adi_soyadi' => \Auth::user()->name,
                    'islem_alani' => $kayit->name,
                    'islem_yapilan_sayfa' => 'Yöneticiler Sayfası',
                    'yapilan_islem' => $guncelleme,
                ]
            );
            $islem->save();
    }

    // show item
    public function show($id)
    {
        //
    }

    // delete item
    public function deleteYonetici(Request $req) {
        $user = User::find($req->id);
        $file= $user->avatar;
        $filename = public_path('/uploads/avatars/'.$file);
        @unlink($filename);
        $this->kayitSilislemGecmisi($user);
        $user->delete();
        return response()->json();
    }
    public function kayitSilislemGecmisi( $kayit) {
        if ( $kayit->save()) {
            // Kayıt tut
            $islem = new IslemGecmisi(
                [
                    'id' => $kayit->id,
                    'kisi_id' => \Auth::user()->id,
                    'adi_soyadi' => \Auth::user()->name,
                    'islem_alani' => $kayit->name,
                    'islem_yapilan_sayfa' => 'Yöneticiler Sayfası',
                    'yapilan_islem' => 'Yöneticiler sayfasında  resim silindi.',
                ]
            );
            $islem->save();
        } else {
            // Kayıt işleminde hata olursa
            return json_encode(array('success' => false));
        }
        return true;
    }
}

<?php

namespace App\Http\Controllers\Yonetici;
use App\Http\Controllers\Controller;
use App\Modeller\Ozelalan;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Modeller\IslemGecmisi;
use Validator;
use Response;
use DB;
use Auth;
use Image;
use Illuminate\Support\Facades\Input;

class OzelalanController extends Controller
{
    //
    public function index()
    {
        $ozelalan= Ozelalan::first();
        return view('backend.ozel-alan.index',
            [
                'ozelalan' => $ozelalan,
            ]);
    }


    // update item
    public function update(Request $request, $id)
    {
        $filename = null;
        if( !empty( $request->file('resim') ) ){
            $resim = $request->file('resim');
            $filename = time(). '1' . '.' . $resim->getClientOriginalExtension();
            $publicAvatarDizini = public_path('/frontend/images/ozel-alan/');
            $resim->move( $publicAvatarDizini, $filename );
            Image::make( $publicAvatarDizini . $filename )->resize(570, auth())->save( $publicAvatarDizini . $filename );
        }
        //Gelen bigileri al
        $gelenBilgiler = Input::all();
        //Fazlalık olan datalari sil (tabloda olmayan)
        unset($gelenBilgiler['_token']);
        unset($gelenBilgiler['_method']);
        //Kalan verileri düzenle
        if( $filename != null ) {
            $gelenBilgiler['resim'] = $filename;
        }
        //İlgili kaydı bul
        $kayit = Ozelalan::find($id);
        //Gelen verilerin her birini kayda ata
        foreach( $gelenBilgiler as $k => $v ) $kayit->$k = $v;
        $this->kayitGuncelle( $kayit);
        //Kaydet
        $kayit->save();
        \Session::put('onayMesaji', 'Kayıt başarı ile gerçekleşti');
        return redirect('/admin/ozel-alan');
    }
    public function kayitGuncelle( $kayit) {
        if ( $kayit->save()) {
            // Kayıt tut
            $islem = new IslemGecmisi(
                [
                    'id' => $kayit->id,
                    'kisi_id' => \Auth::user()->id,
                    'adi_soyadi' => \Auth::user()->name,
//                    'aciklamasi' => $kayit->adi,
                    'islem_yapilan_sayfa' => 'Özel Alan',
                    'yapilan_islem' => 'Özel alan güncellendi.',
                ]
            );
            $islem->save();
        } else {
            // Kayıt işleminde hata olursa
            return json_encode(array('success' => false));
        }
        return true;
    }

    // Aktif Pasif item
    public function aktifpasif(Request $request, $id)
    {
        //Gelen bigileri al
        $gelenBilgiler = Input::all();

        //Fazlalık olan datalari sil (tabloda olmayan)
        unset($gelenBilgiler['_token']);
        unset($gelenBilgiler['_method']);
        //İlgili kaydı bul
        $kayit = Ozelalan::find($id);
        //Gelen verilerin her birini kayda ata
        foreach( $gelenBilgiler as $k => $v ) $kayit->$k = $v;
        //Kaydet
        $kayit->save();
        return redirect('/admin/ozel-alan');
    }

}

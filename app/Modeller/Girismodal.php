<?php
namespace App\Modeller;

use Illuminate\Database\Eloquent\Model;

class Girismodal extends Model
{
    protected $table = 'tb_giris_modal';
    protected $fillable =
        [
            'id',
            'aktif_pasif',
            'resim',
            'baslik',
            'aciklamasi',
            'link',
        ];
}

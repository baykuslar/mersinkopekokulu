<?php
namespace App\Modeller;

use Illuminate\Database\Eloquent\Model;

class Webbilgileri extends Model
{
    protected $table = 'tb_web_bilgileri';
    protected $fillable = [
        'aktif_pasif',
        'kurum_id',
        'kullanici_turu',
        'aktif_pasif',
        'site_baslangic_tarihi',
        'site_teslim_tarihi',
        'site_yayin_tarihi',
        'site_sahibi',
        'firma_adi',
        'telefon',
        'email',
        'domain_kayitli_firma',
        'domain_bitis_tarihi',
        'domain_yenileme_ucreti',
        'hosting_firmasi',
        'hosting_bitis_tarihi',
        'hosting_yenileme_ucreti',
    ];

    public function yetkiadlariBilgileri() {
        return $this->belongsTo('\App\Modeller\Yetkiadlari', 'kullanici_turu', 'id');
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}

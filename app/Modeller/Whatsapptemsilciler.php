<?php
namespace App\Modeller;

use Illuminate\Database\Eloquent\Model;

class Whatsapptemsilciler extends Model
{
    protected $table = 'tb_whatsapp_temsilciler';
    protected $fillable =
    [
        //Genel Ayarlar
        'id',
        'durum',
        'resim',
        'adi_soyadi',
        'unvan_tr',
        'unvan_en',
        'email',
        'telefon',
        'whatsapp',
    ];
}

<?php
namespace App\Modeller;

use Illuminate\Database\Eloquent\Model;

class Sssorular extends Model
{
    protected $table = 'tb_sssorular';
    protected $fillable = [
        'resim',
        'baslik',
        'aciklamasi',
        'link',
        'created_at',
        'updated_at'
    ];
}

<?php

namespace App\Modeller;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Ozelkopekler extends Model
{
    use Notifiable;
    protected $table = 'tb_ozel_kopekler';
    protected $fillable = [
        'id',
        'resim',
        'baslik',
        'aciklamasi',
        'created_at',
        'updated_at'
    ];

}

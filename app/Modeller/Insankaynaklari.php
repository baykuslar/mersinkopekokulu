<?php

namespace App\Modeller;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Insankaynaklari extends Model
{
    use Notifiable;
    protected $table = 'tb_is_basvuru_formu';
    protected $fillable = [
        'resim',
        'basvuru_id',
        'aktif_pasif',
        'adisoyadi',
        'dogumtarihi',
        'cinsiyet',
        'gsm',
        'telefon',
        'email',
        'adres',
        'ib_askerlik_durumu',
        'ib_son_tecil_tarihi',
        'ib_ehliyet',
        'ib_ehliyet_turu',
        'ib_mezuniyet',
        'ib_okul',
        'ib_bolum',
        'ib_deneyimbir',
        'ib_deneyimiki',
        'ib_deneyimuc',
        'ib_maas',
        'ib_notlar',
        'created_at',
        'updated_at'
    ];

}

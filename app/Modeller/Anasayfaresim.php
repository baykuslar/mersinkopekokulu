<?php

namespace App\Modeller;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Anasayfaresim extends Model
{
    use Notifiable;
    protected $table = 'tb_anasayfa_resim';
    protected $fillable = [
        'id',
        'resim',
        'baslik_tr',
        'baslik_en',
        'created_at',
        'updated_at'
    ];

}

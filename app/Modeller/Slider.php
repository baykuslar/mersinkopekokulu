<?php

namespace App\Modeller;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Slider extends Model
{
    use Notifiable;
    protected $table = 'tb_slider';
    protected $fillable = [
        'id',
        'resim',
        'ozel_baslik',
        'baslik',
        'aciklamasi',
        'created_at',
        'updated_at'
    ];

}

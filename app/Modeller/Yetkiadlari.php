<?php
namespace App\Modeller;

use Illuminate\Database\Eloquent\Model;

class Yetkiadlari extends Model
{
    protected $table = 'tb_kullanici_yetki_adlari';
    protected $fillable = ['id'];
}

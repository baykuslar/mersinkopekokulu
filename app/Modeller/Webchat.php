<?php
namespace App\Modeller;

use Illuminate\Database\Eloquent\Model;

class Webchat extends Model
{
    protected $table = 'tb_web_chat';
    protected $fillable =
        [
            'id',
            'aktif_pasif',
        ];
}

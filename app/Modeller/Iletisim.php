<?php
namespace App\Modeller;

use Illuminate\Database\Eloquent\Model;

class Iletisim extends Model
{
    protected $table = 'tb_iletisim';
    protected $fillable =
        [
            //Genel Ayarlar
            'id',
            'adi_soyadi',
            'telefon',
            'email',
            'konu',
            'mesaj',
        ];
}

<?php

namespace App\Modeller;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Egitimlerimiz extends Model
{
    use Notifiable;
    protected $table = 'tb_egitimlerimiz';
    protected $fillable = [
        'id',
        'resim',
        'baslik_tr',
        'baslik_en',
        'slug',
        'aciklamas_tr',
        'aciklamas_en',
        'meta_description',
        'meta_keywords',
        'hit',
    ];
}

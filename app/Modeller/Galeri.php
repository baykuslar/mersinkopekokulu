<?php

namespace App\Modeller;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Galeri extends Model
{
    use Notifiable;
    protected $table = 'tb_galeri';
    protected $fillable = [
        'id',
        'kategori_id',
        'resim',
        'baslik',
        'created_at',
        'updated_at'
    ];

}

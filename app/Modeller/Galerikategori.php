<?php

namespace App\Modeller;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Galerikategori extends Model
{
    use Notifiable;
    protected $table = 'tb_galeri_kategori';
    protected $fillable = [
        'id',
        'adi',
        'baslik',
        'created_at',
        'updated_at'
    ];
    public function galeri() {
        return $this->belongsTo('\App\Modeller\Galeri', 'id', 'kategori_id');
    }
}

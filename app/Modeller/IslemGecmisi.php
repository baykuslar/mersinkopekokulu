<?php

namespace App\Modeller;

use Illuminate\Database\Eloquent\Model;


class IslemGecmisi extends Model
{

    protected $table = 'tb_islem_gecmisi';
    protected $fillable = [
        'kisi_id',
        'kullanici_turu',
        'islem_alani',
        'yapilan_islem',
        'adi_soyadi',
        'islem_yapilan_sayfa',
        'created_at'
    ];
}

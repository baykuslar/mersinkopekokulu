<?php

namespace App\Modeller;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Video extends Model
{
    use Notifiable;
    protected $table = 'tb_video';
    protected $fillable = [
        'id',
        'sayfa_aciklamasi',
        'adi',
        'video_aciklamasi',
        'url_adresi',
        'created_at',
        'updated_at'
    ];

}

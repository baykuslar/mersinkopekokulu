<?php

namespace App\Modeller;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Blog extends Model
{
    use Notifiable;
    protected $table = 'tb_blog';
    protected $fillable = [
        'id',
        'resim',
        'baslik',
        'slug',
        'aciklamasi',
        'meta_description',
        'meta_keywords',
        'hit',
    ];

}

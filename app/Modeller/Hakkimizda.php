<?php
namespace App\Modeller;

use Illuminate\Database\Eloquent\Model;

class Hakkimizda extends Model
{
    protected $table = 'tb_hakkimizda';
    protected $fillable =
    [
        //Genel Ayarlar
        'id',
        'resim_1',
        'resim_2',
        'resim_3',
        'resim_4',
        'resim_5',
        'resim_6',
        'resim_7',
        'resim_8',
        'resim_9',
        'hakkimizda',
        'baslik_1',
        'baslik_2',
        'baslik_3',
        'baslik_4',
        'baslik_5',
        'baslik_6',
        'baslik_7',
        'baslik_8',
        'baslik_9',
        'ozel_baslik',
        'ozel_yazi',
    ];
}

<?php
namespace App\Modeller;

use Illuminate\Database\Eloquent\Model;

class Ekibimiz extends Model
{
    protected $table = 'tb_ekibimiz';
    protected $fillable =
    [
        //Genel Ayarlar
        'id',
        'profil_resim',
        'adi_soyadi',
        'unvan',
        'email',
        'telefon',
        'gsm',
        'hakkinda',
        //Sosyal Medya
        'facebook',
        'instagram',
        'twitter',
        'whatsapp',
    ];
    public function ulke() {
        return $this->belongsTo('App\Modeller\Ulkeler', 'country');
    }
    public function il() {
        return $this->belongsTo('App\Modeller\Iller', 'state');
    }
    public function ilce() {
        return $this->belongsTo('App\Modeller\Ilceler', 'city');
    }
}

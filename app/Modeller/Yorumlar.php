<?php

namespace App\Modeller;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Yorumlar extends Model
{
    use Notifiable;
    protected $table = 'tb_yorumlar';
    protected $fillable = [
        'blog_id',
        'adi_soyadi',
        'yorum',
        'email',
    ];

}

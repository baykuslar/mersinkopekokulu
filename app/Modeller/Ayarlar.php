<?php
namespace App\Modeller;

use Illuminate\Database\Eloquent\Model;

class Ayarlar extends Model
{
    protected $table = 'tb_ayarlar';
    protected $fillable =
    [
        //Genel Ayarlar
        'id',
        'site_basligi',
        'site_adresi',
        'calisma_saatleri_baslangic',
        'calisma_saatleri_bitis',
        'cumartesi',
        'pazar',
        'sehir',
        'ilce',
        'mahalle',
        //İletişim Bilgileri
        'email',
        'email_2',
        'telefon',
        'telefon_2',
        'faks',
        'gsm',
        'adres',
        //Sosyal Medya
        'facebook',
        'instagram',
        'twitter',
        'youtube',
    ];

    public function sehirBilgisi() {
        return $this->belongsTo('App\Modeller\Sehir', 'sehir');
    }
    public function ilceBilgisi() {
        return $this->belongsTo('App\Modeller\Ilce', 'ilce');
    }
    public function mahalleBilgisi() {
        return $this->belongsTo('App\Modeller\Mahalle', 'mahalle');
    }
}

<?php

namespace App\Modeller;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Referanslar extends Model
{
    use Notifiable;
    protected $table = 'tb_referanslar';
    protected $fillable = [
        'id',
        'resim',
        'baslik_tr',
        'baslik_en',
        'aciklama_tr',
        'aciklama_en',
        'created_at',
        'updated_at'
    ];

}

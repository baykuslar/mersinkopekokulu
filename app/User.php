<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'users';
    protected $fillable = [
        'aktif_pasif', 'avatar', 'username', 'password', 'name', 'email', 'phone', 'mobile', 'kullanici_turu', 'kurum_id'
    ];

    public function yetkiadlariBilgileri() {
        return $this->belongsTo('\App\Modeller\Yetkiadlari', 'kullanici_turu', 'id');
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}

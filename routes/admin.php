<?php
Route::group(['middleware' => 'auth'], function () {
    // Anasayfa Router
    Route::get('/index', 'AnasayfaController@index')->name('index');

    Route::group(['middleware' => ['yonetici']], function () {

        // Ürünler İşlemleri Routes
        Route::resource('urunler','UrunlerController');
        Route::post('urunler/changeStatus', array('as' => 'changeStatus', 'uses' => 'UrunlerController@changeStatus'));

        // Köpek Temini İşlemleri Routes
        Route::resource('/backend/kopek-temini', 'KopekteminiController');
        Route::post ('/deleteKopektemini', 'KopekteminiController@deleteKopektemini');

        // Galeri İşlemleri Routes
        Route::resource('/galeri', 'GaleriController');
        Route::post ('/deleteGaleri/{id}', 'GaleriController@deleteGaleri');

        // Kopek Oteli İşlemleri Routes
        Route::resource('/kopekoteli', 'KopekoteliController');
        Route::post ('/deleteKopekoteli/{id}', 'KopekoteliController@deleteKopekoteli');

        // Otel - Pansiyon İşlemleri Routes
        Route::resource('/otel-pansiyon', 'OtelpansiyonController');

        // Kopek Egitim İşlemleri Routes
        Route::resource('/kopekegitimi', 'KopekegitimiController');
        Route::post ('/deleteKopekegitimi/{id}', 'KopekegitimiController@Kopekegitimi');

        // Referanslar İşlemleri Routes
        Route::resource('/referanslar', 'ReferanslarController');
        Route::post ('/deleteReferans/{id}', 'ReferanslarController@deleteReferans');

        // Köpek Okulu İşlemleri Routes
        Route::resource('/kopek-okulu', 'KopekokuluController');
        Route::post ('/deleteKopekokulu/{id}', 'KopekokuluController@deleteKopekokulu');

        // Galeri Video İşlemleri Routes
        Route::resource('/video', 'VideoController');
        Route::post ('/deleteVideo', 'VideoController@deleteVideo');

        // Kurumsal/Ekibimiz İşlemleri Routes
        Route::resource('/backend/kurumsal/ekibimiz', 'KurumsalController');
        Route::post ('/deleteEkibimiz', 'KurumsalController@deleteEkibimiz');

        // Kurumsal/Hakkimizda İşlemleri Routes
        Route::resource('/hakkimizda', 'HakkimizdaController');

        // Kurumsal/İnsanKaynakları İşlemleri Routes
        Route::resource('/insan-kaynaklari', 'InsankaynaklariController');
        Route::post('/insan-kaynaklari/{id}/edit', 'InsankaynaklariController@basvurukapat');
        Route::get('/insan-kaynaklari/{id}/isbasvuru-pdf', 'InsankaynaklariController@isbasvurusu');
        Route::post ('/deleteIsbasvurusu', 'InsankaynaklariController@deleteIsbasvurusu');

        // Yönetici İşlemleri Routes
        Route::resource('/backend/yoneticiler', 'YoneticiController');
        Route::post ('/deleteYonetici', 'YoneticiController@deleteYonetici');

        // Blog Routes
        Route::resource('/blog', 'BlogController');
        Route::post ('/deleteBlog/{id}', 'BlogController@deleteBlog');
        // Ayarlar Routes
        Route::resource('/ayarlar', 'AyarlarController');

        // Giriş Modalı İşlemleri Routes
        Route::resource('/giris-modal', 'GirismodalController');
        Route::post('/giris-modal/{id}/edit', 'GirismodalController@aktifpasif');


// Ülke-il-ilçe Routes
        Route::get('api/dependent-dropdown','AyarlarController@index');
        Route::get('/api/get-ilce-list','AyarlarController@getilceList');
        Route::get('/api/get-mahalle-list','AyarlarController@getmahalleList');
        /*Route::get('/api/get-sokak-list','AnasayfaController@getsokakList');*/

        // İletişim Formu İşlemleri Routes
        Route::resource('/iletisim', 'IletisimController');
        Route::post('/iletisim/{id}/edit', 'IletisimController@iletisimformkapat');
        Route::post ('/deleteFormmail', 'IletisimController@deleteFormmail');

        // SSS İşlemleri Routes
        Route::resource('/sssorular', 'SssorularController');
        Route::post ('/deleteSoru/{id}', 'SssorularController@deleteSoru');

        // Özel Alan İşlemleri Routes
        Route::resource('/ozel-alan', 'OzelalanController');
        Route::post('/ozel-alan/{id}/edit', 'OzelalanController@aktifpasif');

        // Ürünler İşlemleri Routes
        Route::resource('/egitimlerimiz', 'EgitimlerimizController');
        Route::post ('/deleteEgitim', 'EgitimlerimizController@deleteEgitim');
    });

    // İşlem Geçmişi İşlemleri Routes
    Route::resource('/islem-gecmisi', 'IslemGecmisiController');
    Route::post ('/deleteislemGecmisi', 'IslemGecmisiController@deleteislemGecmisi');
    Route::get('multipleDelete', 'IslemGecmisiController@multipleDelete')->name('multipleDelete');

    Route::get('/backend/errors/dosya-yukleme-hatasi', function () {
        return view('/backend/errors/dosya-yukleme-hatasi');
    });

});

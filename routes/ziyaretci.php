<?php

Route::get('/admin', 'ZiyaretciController@index')->middleware('guest');
//Route::get('/admin', 'ZiyaretciController@oturumAc');
Route::post('/admin', 'ZiyaretciController@oturumDogrula');

Route::get('/password/email', 'ZiyaretciController@emailHatirlatmaFormu');

Route::post('/iletisim-formu', function () {
    $gelenBilgiler = \Illuminate\Support\Facades\Input::all();
    \Mail::send('template.mailler.iletisim_formu', $gelenBilgiler, function ($mesaj) {
        $mesaj->to(env('MAIL_ALICI'))->subject('İletişim Formu');
    });

    return response('', 204);
});

Route::get('/dil', 'ZiyaretciController@dilDegistir');
Route::post('/arama_sonuc', 'AramaController@ara')->name('arama_sonuc');

Route::get('/', 'AnasayfaController@index');

Route::get('/hakkimizda', 'KurumsalController@hakkimizda');
/*Route::get('/ekibimiz', 'KurumsalController@ekibimiz');*/
Route::get('/insan-kaynaklari', 'KurumsalController@insanKaynaklari');
Route::resource('/insan-kaynaklari', 'InsankaynaklariController');

Route::get('/referanslar', 'ReferanslarController@referanslar');
Route::get('/referans/{slug}', 'ReferanslarController@show');

Route::get('/resimgalerisi', 'GaleriController@resimgalerisi');
Route::get('/videogalerisi', 'GaleriController@videogalerisi');

Route::get('/sssorular', 'SssorularController@index');

Route::resource('/blog', 'BlogController');
Route::get('/blog/{slug}', 'BlogController@show');
Route::post('/yorum-gonder', 'YorumlarController@yorumGonder');

Route::resource('/iletisim', 'IletisimController');
Route::post('/iletisim-gonder', 'IletisimController@iletisimGonder');

Route::get('/egitimlerimiz', 'EgitimlerimizController@index');
Route::get('/mersin-kopek/{slug}', 'EgitimlerimizController@mersinEgitim');

Route::post ('/arama', 'AnasayfaController@arama');
